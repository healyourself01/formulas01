﻿var language={

"1. 2D SHAPES" : "1. Figury płaskie",
"2. 3D SHAPES" : "2. Figury 3D",
"TRIANGLE" : "Trójkąt",
"RIGHT TRIANGLE" : "Trójkąt prostokątny",
"SQUARE" : "Kwadrat",
"RECTANGLE" : "Prostokąt",
"PARALLELOGRAM" : "Równoległobok",
"LOZENGE" : "Romb",
"TRAPEZOID" : "Trapez",
"CONVEX QUADRILATERAL" : "Czworokąt Wypukły",
"CIRCLE" : "Koło",
"SEGMENT OF CIRCLE" : "Odcinek koła",
"SECTOR OF CIRCLE" : "Wycinek koła",
"REGULAR POLYGON OF N SIDES" : "Wielokąt foremny o N bokach",
"REGULAR POLYGON" : "Wielokąt Foremny",
"REGULAR POLYGON" : "Wielokąt Foremny",
"HEXAGON" : "Sześciokąt",
"SPHERE" : "Kula",
"CYLINDER" : "Walec",
"CYLINDER" : "Walec",
"CONE" : "Stożek",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "Stożek ścięty kołowy",
"PYRAMID" : "Ostrosłup",
"SQUARE PYRAMID" : "Ostrosłup kwadratowy",
"CUBOID" : "Prostopadłościan",
"CUBOID" : "Prostopadłościan",
"TRIANGULAR PRISM" : "Pryzmat Trójkątny",
"A: Area" : "A: Pole",
"A: Area, P: Perimeter" : "A: Pole, P: Obwód",
"b: Arc length" : "b: Długość łuku",
"V: Volume, A: Surface Area" : "V: Objętość, A: Powierzchnia",
"A: Lateral surface area" : "A: Powierzchnia boczna",
"A: Lateral surface area" : "A: Powierzchnia boczna",
"A: Base Area" : "A: Pole podstawy",
"A: Surface Area" : "A: Pole powierzchni",

"1. OPERATIONS ON EXPRESSIONS" : "1. Operacje na wyrażeniach",
"POLYNOMIAL" : "Wielomian",
"FRACTIONS" : "Frakcje",
"IDENTITY" : "Tożsamość",
"EXPONENTIATION" : "Potęgowanie",
"ROOTS" : "Pierwiastki",
"2. RATE FORMULAS" : "2. Proporcje",
"6. COMPLEX NUMBERS" : "6. Liczby zespolone",
"COMPLEX PLANE" : "Płaszczyzna zespolona",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. Ciąg \nCiąg arytmetyczny",
"4. PROGRESSION" : "4. Ciąg", 
"ARITHMETIC PROGRESSION" : "Ciąg arytmetyczny", 
"GEOMETRIC PROGRESSION" : "Ciąg geometryczny", 
"SUMMATIONS" : "Sumowanie",
"5. LOGARITHM" : "5. Logarytm", 
"DECIMAL LOGARITHM" : "Logarytm dziesiętny",
"NATURAL LOGARITHM" : "Logarytm naturalny",
"d: common difference" : "d: różnica ciągu",
"q: common ratio" : "q: iloraz ciągu",
"3. INEQUALITIES" : "3. Nierówności",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "Funkcje trygonometryczne w trójkącie prostokątnym",
"TRIGONOMETRIC TABLE" : "Tablica wartości funkcji trygonometrycznych",
"CO-RATIOS" : "Współczynniki co-",
"BASIC FORMULAS" : "Podstawowe wzory",
"MULTIPLE ANGLE FORMULAS" : "Funkcje wielokrotności kątów",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "Potęgi w postaci sumy",
"ADDITION FORMULAS" : "Formuły dodawania",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "Suma funkcji trygonometrycznych",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "Iloczyn w postaci sumy",
"HALF ANGLE FORMULAS" : "Funkcje kąta połówkowego",
"ANGLES OF A PLANE TRIANGLE" : "Kąty w płaszczyźnie trójkąta",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "Związki pomiędzy funkcjami trygonometrycznymi",
"α, β, γ are 3 angles of a triangle" : "α, β, γ to kąty trójkąta",


"1. ALGEBRAIC EQUATIONS" : "1. Równania algebraiczne",
"LINEAR EQUATION" : "Równanie liniowe",
"SYSTEM OF TWO LINEAR EQUATIONS" : "Układ dwóch równań liniowych",
"QUADRATIC EQUATION" : "Równanie kwadratowe",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. Wykładnik i logarytm \nRównanie wykładnicze",
"2. EXPONENT AND LOGARITHM" : "2. Wykładnik i logarytm",
"LOGARITHMIC EQUATION" : "Równanie logarytmiczne",
"3. TRIGONOMETRIC EQUATION" : "3. równanie trygonometryczne",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. Nierówności \nNierówności liniowe",
"4. INEQUATIONS" : "4. Nierówności",
"QUADRATIC INEQUATION" : "Nierówność Kwadratowa",
"EXPONENTIAL INEQUATION" : "Nierówność Wykładnicza",
"LOGARITHMIC INEQUATION" : "Nierówność Logarytmiczna",
"TRIGONOMETRIC INEQUATIONS" : "Nierówności Trygonometryczne",
"LINEAR EQUATION" : "Równanie liniowe",
"EXPONENTIAL EQUATION" : "Równanie Wykładnicze",
"LINEAR INEQUATION" : "Nierówność Liniowa",
"CUBIC EQUATION" : "Równanie Sześcienne",
"POINTS" : "Punkty",
"- Distance between two points A and B" : "- Odległość między punktami A i B",
"- Distance between point A and origin:" : "- Odległość między punktem A i początkiem układu",
"TRIANGLE" : "Trójkąt",
"- Area of triangle with vertices at A, B, C" : "- Pole trójkąta o wierzchołkach A, B, C",
"- Area of a triangle with a vertice at origin" : "- Pole trójkąta z wierzchołkiem w początku układu",
"EQUATION OF LINE" : "Równanie linii",
"- Joining two points A, B" : "- Łączenie dwóch punktów A, B",
"- Passing point A and parallel with line y=ax+b" : "Przechodząca przez punkt A i równoległa do y=ax+b",
"- Passing point A and perpendicular with line y=ax+b" : "- Przechodząca przez punkt A i prostopadła do y=ax+b",
"EQUATION OF CIRCLE" : "Równanie okręgu",
"- Circle with radius r and center at (a, b)" : "- Okrąg o promieniu r i środku w punkcie (a, b)",
"- Circle with center at origin" : "- Koło ze środkiem w początku układu",
"ELLIPSE" : "Elipsa",
"HYPERBOLA" : "Hiperbola",
"PARABOLA" : "Parabola",

"Limit \n" : "Limit \n",
"Derivative \n" : "Pochodna \n",
"C: constant" : "C: stała",
"Differentiation \n" : "Zróżnicowanie \n",
"C: constant; u, v, w: functions of x" : "C: stała ; u, v, w: funkcje x",
"LIMIT" : "Limit",
"DERIVATIVE" : "Pochodna",
"DIFFERENTIATION" : "Zróżnicowanie \n",

"1. INDEFINITE INTEGRALS" : "1. Całki nieoznaczone",
"C: arbitrary constant, k: constant" : "C: dowolna stała, k: stała",
"INTEGRALS BY PARTIAL FRACTIONS" : "całkowanie ułamików prostych",
"INTEGRALS INVOLVING ROOTS" : "Całki z udziałem pierwiastków",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "Całki z udziałem funkcji trygonometrycznych",
"2. DEFINITE INTEGRALS" : "2. Całki oznaczone",
"APPLICATIONS" : "Zastosowania",
"- Surface area created by y=f(x)" : "- Powierzchnia utworzona przez y=f(x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- Objętość bryły stworzonej przez y=f(x) obracanej wokół osi:",

"Median" : "Mediana",
"Angle bisector" : "Dwusieczna Kąta",
"SPHERICAL CAP" : "Czasza kuli",
"SPHERICAL SEGMENT" : "Odcinek kuli",
"SPHERICAL SECTOR" : "Wycinek kuli",
"TORUS" : "Torus",
"FORMULAS WITH t=tan(x/2)" : "Wzory z t = tan(x/2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "Boki i kąty trójkąta",
"Law of sines, cosines and tangents" : "Prawo sinusów, cosinusów i tangensów",

"1. PLANE ANALYTIC GEOMETRY" : "1. Geometria analityczna płaska",
"2. SOLID ANALYTIC GEOMETRY" : "2. Stereometria",
"LINE" : "Linia",
"- Direction Cosines of Line Joining Points A and B" : "- Kierunek cosinusów linii łączączej punkty A i B",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "Równanie linii łączącej dwa punkty A, B",
"- In standard form" : "- w postaci standardowej",
"- In parametric form" : "- w postaci parametrycznej",
"PLANE" : "Płaszczyzna",
"- General equation of a plane" : "- Ogólne równanie płaszczyzny",
"- Equation of plane passing through point A, B, C" : "- Równanie płaszczyzny przechodzącej przez punkt A, B, C",
"- Equation of plane in intercept form" : "- równanie płaszczyzny w postaci odcinkowej",
"a,b,c are the intercept on the x,y,z axes, respectively" : "Punkty A, B, C leża na osiach x,y,z i są to kolejno",
"- Normal Form for Equation of Plane" : "- Postać normalna dla równania płaszczyzny",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p: prostopadła odległość od O do płaszczyzny P ; α, β, γ: kąty pomiędzy OP i dodatnimi osiami x,y,z",
"- Distance from point M to a plane" : "- Odległość od punktu M do płaszczyzny",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Równanie środka kuli w punkcie M i promieniu R w układzie współrzędnych",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "Równanie elipsoidy o środku M i pół-osiach a, b, c,",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Eliptyczny walec z osią z jako oś walca",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "Eliptyczny stożek z osią z jako oś stożka",
"HYPERBOLOID OF ONE SHEET" : "Hiperboloida jednopowłokowa",
"HYPERBOLOID OF TWO SHEETS" : "Hiperboloida dwupowłokowa",
"ELLIPTIC PARABOLOID" : "Paraboloida eliptyczna",
"HYPERBOLIC PARABOLOID" : "Paraboloida hiperboliczna",

"TRANSFORMATIONS" : "Transformacje",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. Specjalne całki nieoznaczone",
"Integrals involving ax+b" : "Całki z udziałem ax+b",
"Integrals involving ax+b and px+q" : "Całki z udziałem ax+b i px+q",
"Integrals involving x²+a²" : "Całki z udziałem x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "Całki z udziałem x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a²" : "Całki z udziałem x²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "Całki z udziałem ax²+bx+c",
"Integrals involving xⁿ+aⁿ" : "Całki z udziałem xⁿ+aⁿ",
"Integrals involving sin(ax)" : "Całki z udziałem sin(ax)",
"Integrals involving e^(ax)" : "Całki z udziałem e^(ax)",
"Integrals involving ln(x)" : "Całki z udziałem ln(x)",

"TRANSPOSE OF A MATRIX" : "Transpozycja macierzy",
"ADDITION AND SUBTRACTION OF MATRICES" : "Dodawanie i odejmowanie macierzy",
"MULTIPLICATION OF MATRICES" : "Mnożenie macierzy",
"DETERMINANT OF MATRIX" : "Wyznacznik macierzy",
"INVERSE OF MATRIX" : "Odwracanie macierzy",
"EQUATION IN MATRIX FORM" : "Równanie w postaci macierzowej",
"PROPERTIES OF MATRIX CALCULATIONS" : "Właściwości obliczeń macierzowych",
"LENGTH" : "Długość",
"AREA" : "Obszar",
"VOLUME" : "Objętość",
"MASS" : "Masa",
"SPEED" : "Prędkość",
"TIME" : "Czas",
"TEMPERATURE" : "Temperatura",
"DENSITY" : "Gęstość",
"FORCE" : "Siła",
"ENERGY" : "Energia",
"POWER" : "Moc",
"PRESSURE" : "Ciśnienie",
"Do you know this?" : "Znasz to?",
"What is special about these calculations?" : "Co jest szczególnego w tych obliczeniach ?",
"Can you find the interesting trait of these calculations?" : "Możesz znaleźć ciekawą cechę dotyczącą tych obliczeń?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. Wykres innych funkcji",
"Constant" : "Stały",
"Absolute" : "Bezwzględny",
"Square Root" : "Pierwiastek Kwadratowy",
"Parabolic" : "Paraboliczny",
"Cubic" : "Sześcienny",
"Reciprocal" : "Wzajemny",
"Sec" : "Sec",
"Cosec" : "Cosec",
"6. FUNCTION TRANSFORMATIONS" : "6. Przemiany funkcji",
"Horizontal Shifting" : "Przesunięcie poziome",
"Vertical Shifting" : "Przesunięcie w pionie",
"Reflection" : "Odbicie",
"Stretching" : "Rozciąganie",
"F, F1: Focus points (foci)" : "F, F1: Punkty ogniskowe (ogniska)",
"AF=p: Parameter of Parabola" : "AF=p: Parametr paraboli",

"Probability & Statistics" : "Statystyka",
"1. SETS" : "1. Zbiory",
"SET" : "Zbiór",
"SUBSET" : "Podzbiór ",
"INTERSECTION" : "Przecięcie Zbiorów",
"UNION" : "Union",
"SYMMETRIC DIFFERENCE" : "Różnica symetryczna ",
"RELATIVE COMPLEMENT OF A IN B" : "Względna komplementarna A w B ",
"ABSOLUTE COMPLEMENT" : "Bezwzględne dopełnienie",
"OPERATIONS ON SETS" : "Operacje na Zbiorach ",
"2. COMBINATIONS AND PERMUTATIONS" : "2. Kombinacje i Permutacje ",
"COMBINATIONS" : "Kombinacje ",
"PERMUTATIONS" : "Permutacje ",
"3. PROBABILITY" : "3. Prawdopodobieństwo ",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) Jeżeli A i B są wzajemnie wykluczające, (2) Jeżeli A i B są niezależne ",
"4. STATISTICS" : "4. Statystyki ",
"MEAN" : "Średnia",
"MEDIAN" : "Mediana",
"MODE" : "Tryb ",
"Numerical value that occurs the most number of times" : "Wartość liczbowa, która występuje najwięcej razy ",
"EXAMPLE" : "Przykład ",
"GEOMETRIC MEAN" : "Średnia geometryczna ",
"HARMONIC MEAN" : "Średnia harmoniczna ",
"VARIANCE" : "Zróżnicowanie",
"STANDARD DEVIATION" : "Odchylenie standardowe ",
"MEAN DEVIATION" : "Średnie odchylenie ",
"ROOT MEAN SQUARE" : "Średni pierwiastek kwadratowy ",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "Ukkład normalny (układ Gaussa) ",
"f: density function, F: Distribution function, μ: mean" : "f: funkcja gęstości, K: Funkcja Układu, μ: średnie ",
"EXPONENTIAL DISTRIBUTION" : "Układ wykładniczy ",
"POISSON DISTRIBUTION" : "Układ Poissona ",
"UNIFORM DISTRIBUTION" : "Układ równomierny ",
"Transforms" : "Transformaty",
"1. FOURIER SERIES AND TRANSFORMS" : "1. Szereg Fouriera i transformaty ",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f (x): funkcja okresowa, okres 2L; a_n, B_N: współczynniki Fouriera; C_N: kompleks współczynników Fouriera ",
"REAL FORM OF FOURIER SERIES" : "Prawdziwa postać szeregu Fouriera ",
"COMPLEX FORM" : "Forma złożona ",
"PARSEVAL’S THEOREM" : "Twierdzenie Parsevala ",
"FOURIER TRANSFORM" : "Transformacja Fouriera ",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f (x) funkcja x, F (s) z transformatą Fouriera funkcji f (x) ",
"CONVOLUTION" : "Splot ",
"CORRELATION" : "Korelacja ",
"f*: complex conjugate of f" : "f *: f sprzężona ",
"FOURIER SYMMETRY RELATIONSHIPS" : "Relacje symetrii Fouriera ",
"FOURIER TRANSFORM PAIRS" : "Pary z transformacją Fouriera ",
"2. LAPLACE TRANSFORMS" : "2. Transformaty Laplace'a ",
"DEFINITION" : "Definicja ",
"CONVOLUTION" : "Splot ",
"INVERSE" : "Odwrotna ",
"a: constant" : "a: stała ",
"DERIVATIVE" : "Pochodna",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "Zmiana (przesunięcie Częstotliwości) ",
"TRANSLATION (TIME SHIFTING)" : "Tłumaczenie (zmiana Czasu) ",
"LAPLACE TRANSFORM PAIRS" : "Pary transformatów Laplace",

"Math Tricks" : "Sztuczki Matematyczne ",
"1. ADDITION" : "1. Dodawanie ",
"2. SUBTRACTION" : "2. Odejmowanie ",
"3. MULTIPLICATION" : "3. mnożenie ",
"4. DIVISION" : "4. Dzielenie ",
"5. SQUARING" : "5. Kwadrat",
"6. EXPONENTIATION" : "6. Potęgowanie",
"7. ROOTS" : "7. Pierwiastki",
"8. SUMMATIONS" : "8. Sumowanie",
"6. SUMMATIONS" : "6. Sumowanie",

"UNITS CONVERTER" : "Konwerter Jednostek ",
"ANGLE" : "Kąt",
"COMPUTER DATA" : "Dane Komputera ",
"FUEL CONSUMPTION" : "Zużycie Paliwa ",
"Click here to download Physics Formulas app" : "Kliknij tutaj, aby pobrać aplikację Wzorów z Fizyki ",
"All Maths formulas for your work and study" : "Wszystkie wzory Matematyczne dla Twojej pracy i do nauki ",
"EDIT" : "Edytuj ",
"ADD" : "Dodaj ",
"TITLE OF THE PICTURE" : "Tytuł obrazu ",
"GAME" : "Gra ",
"START" : "Start ",
"LEVEL" : "Poziom ",
"RETRY" : "Ponów Próbę ",
"SCORE" : "Wynik ",
"TIME" : "Czas",
"BEST" : "Najlepszy ",
"GAME OVER" : "Koniec Gry ",
"YOUR SCORE IS: " : "Twój wynik to",

"Metre (m)" : "Metr  (m)",
"Kilometre (km)" : "Kilometr  (km)",
"Centimetre (cm)" : "Centymetr  (cm)",
"Millimetre (mm)" : "Milimetr  (mm)",
"Micrometre | Mircon (μm)" : "Mikrometr | Mircon  (μm)",
"Nanometre (nm)" : "Nanometr (nm)",
"Inch (in)" : "Cal  (in)",
"Foot (ft)" : "Stopa  (ft)",
"Yard (yd)" : "Jard  (yd)",
"Mile (mi)" : "Mila  (mi)",
"Light Year" : "Rok Świetlny ",
"Nautical Mile (NM)" : "Mila Morska (NM)",
"Angstrom (Å)" : "Angstrem (Å)",

"Square Mile (mi²)" : "Mila Kwadratowa  (mi²)",
"Square Yard (yd²)" : "Jard Kwadratowy  (yd²)",
"Square Foot (ft²)" : "Stopa Kwadratowa  (ft²)",
"Square Inch (in²)" : "Cal Kwadratowy  (in²)",
"Square Kilometre (km²)" : "Kilometr Kwadratowy  (km²)",
"Hectare (ha)" : "Hektar (ha)",
"Acre" : "Akr ",
"Square Metre (m²)" : "Metr Kwadratowy  (m²)",
"Square Centimetre (cm²)" : "Centymetr Kwadratowy  (cm²)",
"Square Millimetre (mm²)" : "Milimetr Kwadratowy  (mm²)",

"Litre (l)" : "Litr  (l)",
"Cubic Metre (m³)" : "Metr Sześcienny  (m³)",
"Cubic Inch (in³)" : "Cal Sześcienny  (in³)",
"Cubic Foot (ft³)" : "Stopa Sześcienna  (ft³)",
"Cubic Yard (yd³)" : "Jard Sześcienny  (yd³)",
"Gallon (US) (gal)" : "Galon (US)  (gal)",
"Gallon (UK) (gal)" : "Galon (UK)  (gal)",

"Gram (g)" : "Gram  (g)",
"Kilogram (kg)" : "Kilogram  (kg)",
"Tonne (t)" : "Tona  (t)",
"Milligram (mg)" : "Miligram  (mg)",
"Microgram (μg)" : "Mikrogram  (μg)",
"Ounce (oz)" : "Uncja  (oz)",
"Pound (lb)" : "Funt  (lb)",
"Carat" : "Karat ",
"Slug" : "Slug ",
"Ounce (Troy) (ozt)" : "Uncja (Troy)  (ozt)",

"Year" : "Rok ",
"Month" : "Miesiąc ",
"Week" : "Tydzień ",
"Day" : "Dzień ",
"Hour (h)" : "Godzina  (h)",
"Minute (min)" : "Minuta  (min)",
"Second (s)" : "Sekund  (s)",
"Millisecond (ms)" : "Milisekunda  (ms)",
"Microsecond (μs)" : "Mikrosekunda  (μs)",
"Decade" : "Dekada ",
"Century" : "Wiek ",
"Millennium" : "Millennium ",

"Kilometre per Hour (km/h)" : "Kilometrów na Godzinę  (km/h)",
"Miles per Hour (mi/h)" : "Mil na Godzinę  (mi/h)",
"Metre per Second (m/s)" : "Metrów na Sekundę  (m/s)",
"Foot per Second (ft/s)" : "Stóp na Sekundę  (ft/s)",
"Knot" : "Knot ",
"Speed of Light (c)" : "Prędkość światła  (c)",
"Minutes per Mile (min/mi)" : "Minut na Milę  (min/mi)",
"Minutes per Kilometre (min/km)" : "Minut na Kilometr  (min/km)",

"Celsius (°C)" : "Celsjusza  (°C)",
"Fahrenheit (°F)" : "Fahrenheita (°F)",
"Kelvin (K)" : "Kelvina (K)",
"Rankine (°R)" : "Rankina  (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "Kilogram / ​​Metr Sześcienny  (kg/m³)",
"Kilogram/Litre (kg/l)" : "Kilogram / ​​Litr  (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "Slug / Stopa Sześcienna  (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "Funt / Stopa Sześcienna  (lbm/ft³)",

"Newton (N)" : "Newton  (N)",
"Kilonewton (kN)" : "Kilonewton  (kN)",
"Atomic unit of force" : "Jednostka atomowa siły",
"Gram Force (gf)" : "Siła Gram  (gf)",
"Pound Force (lbf)" : "Siła Funta  (lbf)",
"Poundal (pdl)" : "Poundal  (pdl)",
"Dyne (dyn)" : "Dyn  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "Kilogram-Siła | Kilopond  (kgf)",

"Watt (W)" : "Wat (W)",
"Kilowatt (kW)" : "Kilowat (kW)",
"Megawatt (MW)" : "Megawat (MW)",
"Milliwatt (mW)" : "Miliwat  (mW)",
"Microwatt (µW)" : "Mikrowat  (µW)",
"Horsepower (hp)" : "Konie Mechaniczne (hp)",
"Calorie per Second (cal/s)" : "Kalorii na Sekundę  (cal/s)",

"Pascal (Pa)" : "Pascal  (Pa)",
"Kilopascal (kPa)" : "Kilopaskal  (kPa)",
"Megapascal (MPa)" : "Megapaskal  (MPa)",
"Bar (bar)" : "Bar  (bar)",
"Millibar (mbar)" : "Milibar  (mbar)",
"Atmosphere (atm)" : "Atmosfera (atm) ",
"Pound Force per Square Inch (psi)" : "Funt Siły na Cal Kwadratowy  (psi)",
"Dyne per Square Centimetre" : "Dyn na Centymetr Kwadratowy ",

"Degree (°)" : "Stopień  (°)",
"Radian (rad)" : "Radian  (rad)",
"π Radian (π rad)" : "π Radian  (π rad)",
"Second" : "Sekund ",
"Minute (')" : "Minuta  (')",

"Byte (B)" : "Bajt  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "Kilobajt | Kibibajt  (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "Megabajt | Mebibajt  (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Gigabajt | Gibibajt  (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "Terabajt | Tebibajt  (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "Petabajt | Pebibajt  (PB | Pi)",
"Bit (b)" : "Bit (b)",
"Zebibyte (Zi)" : "Zebibajt  (Zi)",

"Litres per 100 Kilometres (l/100km)" : "Litrów na 100 Kilometrów  (l/100km)",
"Kilometres per Litre (km/l)" : "Kilometry za Litr  (km/l)",
"Miles per Gallon (US) (mpg)" : "Mil na Galon (US)  (mpg)",
"Miles per Gallon (UK) (mpg)" : "Mil na Galon (UK) (mpg)",

"Title of new tool" : "Tytuł nowego narzędzia",
"Name of the variable" : "Nazwa zmiennej",
"Add more variable" : "Dodaj zmienną",
"Formula of the tool" : "Wzór narzędzia",
"Name of the result " : "Imię wyniku",
"Add more result" : "Dodaj wyniki",

"EULER'S FORMULA" : "Wzór Eulera",

};