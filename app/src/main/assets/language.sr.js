﻿var language={
"1. 2D SHAPES" : "1. 2Д Облици",
"2. 3D SHAPES" : "2. 3Д Облици",
"TRIANGLE" : "Троугао",
"RIGHT TRIANGLE" : "Правоугли троугао",
"SQUARE" : "Квадрат",
"RECTANGLE" : "Правоугаоник",
"PARALLELOGRAM" : "Паралелограм",
"LOZENGE" : "Ромб",
"TRAPEZOID" : "Трапез",
"CONVEX QUADRILATERAL" : "Конвексни четвороугао",
"CIRCLE" : "Круг",
"SEGMENT OF CIRCLE" : "Сегмент круга",
"SECTOR OF CIRCLE" : "Сектор круга",
"REGULAR POLYGON OF N SIDES" : "Регуларни полигон са Н страна",
"REGULAR POLYGON" : "Регуларни полигон",
"REGULAR POLYGON" : "Регуларни полигон",
"HEXAGON" : "Шестоугаоник",
"SPHERE" : "Сфера",
"CYLINDER" : "Цилиндар",
"CYLINDER" : "Цилиндар",
"CONE" : "Купа",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "Фрустум праве кружне купе",
"PYRAMID" : "Пирамида",
"SQUARE PYRAMID" : "Квадратна пирамида",
"CUBOID" : "Кубоид",
"CUBOID" : "Кубоид",
"TRIANGULAR PRISM" : "Тространа призма",
"A: Area" : "A: Површина",
"A: Area, P: Perimeter" : "A: Површина, P: Обим",
"b: Arc length" : "b: Дужина лука",
"V: Volume, A: Surface Area" : "V: Запремина, А: Површинска Област",
"A: Lateral surface area" : "А: Бочна површинска област",
"A: Lateral surface area" : "А: Бочна површинска област",
"A: Base Area" : "А: Базна област",
"A: Surface Area" : "А: Површинска област",

"1. OPERATIONS ON EXPRESSIONS" : "1. Операције на изразима",
"POLYNOMIAL" : "Полином",
"FRACTIONS" : "Разломци",
"IDENTITY" : "Идентитет",
"EXPONENTIATION" : "Степеновање",
"ROOTS" : "Корење",
"2. RATE FORMULAS" : "2. Стопа Формуле",
"6. COMPLEX NUMBERS" : "6. Комплексни бројеви",
"COMPLEX PLANE" : "Комплексна раван",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. Прогресија \nАритметичка прогресија",
"4. PROGRESSION" : "4. Прогресија", 
"ARITHMETIC PROGRESSION" : "Аритметичка прогресија", 
"GEOMETRIC PROGRESSION" : "Геометричка прогресија", 
"SUMMATIONS" : "Сумирања",
"5. LOGARITHM" : "5. Логаритам", 
"DECIMAL LOGARITHM" : "Децимални логаритам",
"NATURAL LOGARITHM" : "Природни логаритам",
"d: common difference" : "d: заједничка разлика",
"q: common ratio" : "q: количник",
"3. INEQUALITIES" : "3. Неједнакости",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "Тригонометријске функције за прави троугао",
"TRIGONOMETRIC TABLE" : "Тригонометријски сто",
"CO-RATIOS" : "ко-односи",
"BASIC FORMULAS" : "Основне формуле",
"MULTIPLE ANGLE FORMULAS" : "Многоугаоне формуле",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "Овлашћења тригонометријских функција",
"ADDITION FORMULAS" : "Додане формуле",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "Збир тригонометријских функција",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "Производ тригонометријских функција",
"HALF ANGLE FORMULAS" : "Полуугаоне формуле",
"ANGLES OF A PLANE TRIANGLE" : "Углови простог троугла",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "Односи међу тригонометријским функцијама",
"α, β, γ are 3 angles of a triangle" : "α, β, γ су 3 углова троугла",


"1. ALGEBRAIC EQUATIONS" : "1. Алгебарске једначине",
"LINEAR EQUATION" : "Линеарна једначина",
"SYSTEM OF TWO LINEAR EQUATIONS" : "Систем две линеарне једначине",
"QUADRATIC EQUATION" : "Квадратна једначина",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. експоненти и логаритми \nЕкспоненцијалне једначина",
"2. EXPONENT AND LOGARITHM" : "2. експонент и логаритам",
"LOGARITHMIC EQUATION" : "Логаритамска једначина",
"3. TRIGONOMETRIC EQUATION" : "3. Тригонометријска једначина",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. неједнакости \nЛинеарна неједнакост",
"4. INEQUATIONS" : "4. Неједнакости",
"QUADRATIC INEQUATION" : "Квадратне неједнакост",
"EXPONENTIAL INEQUATION" : "Експоненцијална неједнакост",
"LOGARITHMIC INEQUATION" : "Логаритамска неједнакост",
"TRIGONOMETRIC INEQUATIONS" : "Тригонометријске неједнакости",
"LINEAR EQUATION" : "Линеарна једначина",
"EXPONENTIAL EQUATION" : "Експоненцијална једначина",
"LINEAR INEQUATION" : "Линеарна неједнакост",
"CUBIC EQUATION" : "Кубна једначина",
"POINTS" : "Бодови",
"- Distance between two points A and B" : "- Растојање између две тачке А и B",
"- Distance between point A and origin:" : "- Растојање између тачке А и порекла",
"TRIANGLE" : "Троугао",
"- Area of triangle with vertices at A, B, C" : "- Површина троугла са теменима у А, B, C",
"- Area of a triangle with a vertice at origin" : "- Површина троугла са теменом на пореклу",
"EQUATION OF LINE" : "Једначина линије",
"- Joining two points A, B" : "- Спајање две тачке А, B",
"- Passing point A and parallel with line y=ax+b" : "- Пролазна тачка А и паралела са линијом у=аx+b",
"- Passing point A and perpendicular with line y=ax+b" : "- Доношење тачке А и под правим углом са линије y=ax+b",
"EQUATION OF CIRCLE" : "Једначина круга",
"- Circle with radius r and center at (a, b)" : "- Круг полупречника р и центром у (a, b)",
"- Circle with center at origin" : "- Круг са центром у пореклу",
"ELLIPSE" : "Елипса",
"HYPERBOLA" : "Хипербола",
"PARABOLA" : "Парабола",

"Limit \n" : "Ограничење \n",
"Derivative \n" : "Извод \n",
"C: constant" : "C: константа",
"Differentiation \n" : "Диференцијација \n",
"C: constant; u, v, w: functions of x" : "C: константа; u, v, w: функција x",
"LIMIT" : "Граница",
"DERIVATIVE" : "Извод",
"DIFFERENTIATION" : "Диференцијација",

"1. INDEFINITE INTEGRALS" : "1. Неодређени интеграли",
"C: arbitrary constant, k: constant" : "C: произвољна константа, k: константа",
"INTEGRALS BY PARTIAL FRACTIONS" : "Интеграли са делимичним фракцијама",
"INTEGRALS INVOLVING ROOTS" : "Интеграли који укључују корене",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "Интеграли који укључују тригонометријске функције",
"2. DEFINITE INTEGRALS" : "2. Одређени интеграли",
"APPLICATIONS" : "Апликације",
"- Surface area created by y=f(x)" : "- Површинска област створена од y=f(x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- Обим солидан створен од y=f(x) ротира око осе:",

"Median" : "Медијан",
"Angle bisector" : "Угао симетрале",
"SPHERICAL CAP" : "Сферна Капа",
"SPHERICAL SEGMENT" : "Сферни сегмент",
"SPHERICAL SECTOR" : "Сферни сектор",
"TORUS" : "Торус",
"FORMULAS WITH t=tan(x/2)" : "Формуле са t=tan(x/2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "Стране и углови правоуглог троугла",
"Law of sines, cosines and tangents" : "Закон о синусу, косинусу и тангенсу",

"1. PLANE ANALYTIC GEOMETRY" : "1. Проста Аналитичка геометрија",
"2. SOLID ANALYTIC GEOMETRY" : "2. Солидна Аналитичка геометрија",
"LINE" : "Линија",
"- Direction Cosines of Line Joining Points A and B" : "-Правац косинус линије која спаја тачке А и B",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "Једначина линије која спаја две тачке А, B",
"- In standard form" : "- у стандардном облику",
"- In parametric form" : "- у параметарском облику",
"PLANE" : "РАВАН",
"- General equation of a plane" : "- Генерална Једначина равни",
"- Equation of plane passing through point A, B, C" : "- Једначина равни пролази кроз тачку А, B, C",
"- Equation of plane in intercept form" : "- Једначина равни у облику пресретнутих форми",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a,b,c је пресретање на x,y,z осе, односно",
"- Normal Form for Equation of Plane" : "- Нормална форма За Једначине равни",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p: нормала удаљености од О до равни на P; α, β, γ: углови између OP и позитивних x,y,z оси",
"- Distance from point M to a plane" : "- Удаљеност од тачке М на раван",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Једначина сфере центра на М и полупречника R у правоугаоне координате",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "Једначина елипсоида са центром М и полу-осама a, b, c",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Елипсасти цилиндар са осом као z оса",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "Елипсасти конус са осом као z оса",
"HYPERBOLOID OF ONE SHEET" : "Хиперболоид једног листа",
"HYPERBOLOID OF TWO SHEETS" : "Хиперболоид од два листа",
"ELLIPTIC PARABOLOID" : "Елипсасти параболоид",
"HYPERBOLIC PARABOLOID" : "Хиперболички параболоид",

"TRANSFORMATIONS" : "Трансформације",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. Посебне неодређени интеграли",
"Integrals involving ax+b" : "Интеграли укључују ax+b",
"Integrals involving ax+b and px+q" : "Интеграли укључују ax+b и px+q",
"Integrals involving x²+a²" : "Интеграли укључују x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "Интеграли укључују x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a" : "Интеграли укључују x²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "Интеграли укључујуa x²+bx+c",
"Integrals involving xⁿ+aⁿ" : "Интеграли укључују xⁿ+aⁿ",
"Integrals involving sin(ax)" : "Интеграли укључују sin(ax)",
"Integrals involving e^(ax)" : "Интеграли укључују e^(ax)",
"Integrals involving ln(x)" : "Интеграли укључују ln(x)",

"TRANSPOSE OF A MATRIX" : "Транспонуј матрице",
"ADDITION AND SUBTRACTION OF MATRICES" : "Сабирање и одузимање матрица",
"MULTIPLICATION OF MATRICES" : "Множење матрица",
"DETERMINANT OF MATRIX" : "Детерминанта матрице",
"INVERSE OF MATRIX" : "Инверзија матрице",
"EQUATION IN MATRIX FORM" : "Једначина у облику матрице",
"PROPERTIES OF MATRIX CALCULATIONS" : "Својства прорачуна матрице",
"LENGTH" : "Дужина",
"AREA" : "Област",
"VOLUME" : "Запремина",
"MASS" : "Маса",
"SPEED" : "Брзина",
"TIME" : "Време",
"TEMPERATURE" : "Температура",
"DENSITY" : "Густина",
"FORCE" : "Сила",
"ENERGY" : "Енергија",
"POWER" : "Снага",
"PRESSURE" : "Притисак",
"Do you know this?" : "Да ли знате ово?",
"What is special about these calculations?" : "Шта је посебно у вези ових прорачуна?",
"Can you find the interesting trait of these calculations?" : "Да ли можете да пронађете занимљиву особину ових прорачуна?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. Графикон других функција",
"Constant" : "Константа",
"Absolute" : "Апсолутност",
"Square Root" : "Kvadratni koren",
"Parabolic" : "Параболска",
"Cubic" : "Кубик",
"Reciprocal" : "Реципрочан",
"Sec" : "Секант",
"Cosec" : "Косекант",
"6. FUNCTION TRANSFORMATIONS" : "6. ФУНКЦИЈА ТРАНСФОРМАЦИЈЕ",
"Horizontal Shifting" : "Хоризонтално мењање",
"Vertical Shifting" : "Вертикално мењање",
"Reflection" : "Одраз",
"Stretching" : "Истезање",
"F, F1: Focus points (foci)" : "F, F1: Тачке фокуса (foci)",
"AF=p: Parameter of Parabola" : "AF=p: Параметар параболе",

"Probability & Statistics" : "Вероватноћа и статистика",
"1. SETS" : "1. Сетови",
"SET" : "Сет",
"SUBSET" : "Podskup",
"INTERSECTION" : "Раскрсница",
"UNION" : "Сједињење",
"SYMMETRIC DIFFERENCE" : "Симетрична разлика",
"RELATIVE COMPLEMENT OF A IN B" : "Релативни комплемент А и B",
"ABSOLUTE COMPLEMENT" : "Апсолутна допуна",
"OPERATIONS ON SETS" : "Операције над скуповима",
"2. COMBINATIONS AND PERMUTATIONS" : "2. комбинације и пермутације",
"COMBINATIONS" : "Комбинације",
"PERMUTATIONS" : "Пермутације",
"3. PROBABILITY" : "3. Вероватноћа",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) Ако се А и B се међусобно искључују, (2) Ако су А и B су независни",
"4. STATISTICS" : "4. Статистика",
"MEAN" : "Просек",
"MEDIAN" : "Медијан",
"MODE" : "Мод",
"Numerical value that occurs the most number of times" : "Нумеричка вредност која се јавља највећи број пута",
"EXAMPLE" : "Пример",
"GEOMETRIC MEAN" : "Геометријска средина",
"HARMONIC MEAN" : "Хармонијска средина",
"VARIANCE" : "Разлика",
"STANDARD DEVIATION" : "Standardna devijacija",
"MEAN DEVIATION" : "Средње одступање",
"ROOT MEAN SQUARE" : "Корен средњег квадрата",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "Нормална дистрибуција (Гаусова расподела)",
"f: density function, F: Distribution function, μ: mean" : "f: Функција густине, F: Дистрибуција функција, μ: меан",
"EXPONENTIAL DISTRIBUTION" : "Експоненцијална дистрибуција",
"POISSON DISTRIBUTION" : "Поисонова расподела",
"UNIFORM DISTRIBUTION" : "Униформна расподела",
"Transforms" : "Трансформисања",
"1. FOURIER SERIES AND TRANSFORMS" : "1. Фуријеов ред и преображавања",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): периодична функција, рок2L; a_n, b_n: Фуријеви коефицијенти; c_n: комплекс Фуријевог коефицијента",
"REAL FORM OF FOURIER SERIES" : "Стварни облик Фуријер серије",
"COMPLEX FORM" : "Комплексна форма",
"PARSEVAL’S THEOREM" : "Парсевалова теорема",
"FOURIER TRANSFORM" : "Фуријев трансформ",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): функција од x, F(s): Фуријеова трансформација f(x)",
"CONVOLUTION" : "Сазив",
"CORRELATION" : "Корелација",
"f*: complex conjugate of f" : "f*: комплекс Коњугација од f",
"FOURIER SYMMETRY RELATIONSHIPS" : "Фуријер односи симетрије",
"FOURIER TRANSFORM PAIRS" : "Фуријер трансформ парова",
"2. LAPLACE TRANSFORMS" : "2. Лаплас трансформације",
"DEFINITION" : "Дефиниција",
"CONVOLUTION" : "Сазив",
"INVERSE" : "Инверзан",
"a: constant" : "a: константа",
"DERIVATIVE" : "Извод",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "Замена (Фреквенција померања)",
"TRANSLATION (TIME SHIFTING)" : "Превод (време пребацивања)",
"LAPLACE TRANSFORM PAIRS" : "Лапласова трансформација парова",

"Math Tricks" : "Математички трикови",
"1. ADDITION" : "1. Додатак",
"2. SUBTRACTION" : "2. Одузимање",
"3. MULTIPLICATION" : "3. Множење",
"4. DIVISION" : "4. Подела",
"5. SQUARING" : "5. Квадратирање",
"6. EXPONENTIATION" : "6. Степеновање",
"7. ROOTS" : "7. Корење",
"8. SUMMATIONS" : "8. Сумирања",
"6. SUMMATIONS" : "6. Сумирања",

"UNITS CONVERTER" : "Конвертер јединица",
"ANGLE" : "Угао",
"COMPUTER DATA" : "Рачунарски податци",
"FUEL CONSUMPTION" : "Потрошња горива",
"Click here to download Physics Formulas app" : "Кликните овде да преузмете апликацију за формуле из физике",
"All Maths formulas for your work and study" : "Све Математичке формуле за ваш рад и студије",
"EDIT" : "Уреди",
"ADD" : "Додај нову ставку",
"TITLE OF THE PICTURE" : "Назив слике",
"GAME" : "Игра",
"START" : "Почетак",
"LEVEL" : "Ниво",
"RETRY" : "Покушати поново",
"SCORE" : "Резултат",
"TIME" : "Време",
"BEST" : "Најбољи reзултат",
"GAME OVER" : "Kraj igre",
"YOUR SCORE IS: " : "Ваш резултат је:",
"Title of new tool" : "Наслов новог алата",
"Name of the variable" : "Име променљиве",
"Add more variable" : "Додајте још променљивих",
"Formula of the tool" : "Формула алата",
"Name of the result " : "Име резултата",
"Add more result" : "Додајте још резултата",

"Save favorite successful" : "Омиљено успешно сачувано",
"Save favorite failed" : "Омињено неуспешно сачувано",
"Save tool successful" : "Алат сачуван успешно",
"Save tool failed" : "Алат сачуван неуспешно",
"Free version is limited with only 3 variables. For more, please use Full version" : "Бесплатна верзија је ограничена са само 3 варијабли. За више, молимо вас да користите пуну верзију",
"Are you sure deleting this?" : "Да ли сте сигурни за брисање овог?",
"Go to Full version" : "Иди на пуну верзију",
"Delete invalid" : "Брисање неважеће",
"EULER'S FORMULA" : "Ојлерова формула",



"Metre (m)" : "Метар (m)",
"Kilometre (km)" : "Километар (km)",
"Centimetre (cm)" : "Центиметар (cm)",
"Millimetre (mm)" : "Милиметар (mm)",
"Micrometre | Mircon (μm)" : "Микрометар | Микрон (μm)",
"Nanometre (nm)" : "Нанометар (nm)",
"Inch (in)" : "Инч (in)",
"Foot (ft)" : "Стопа (ft)",
"Yard (yd)" : "Јард (yd)",
"Mile (mi)" : "Миља (mi)",
"Light Year" : "Svetlosna godina",
"Nautical Mile (NM)" : "Наутичка миља (NM)",
"Angstrom (Å)" : "Ангстром (Å)",

"Square Mile (mi²)" : "Квадратна миља (mi²)",
"Square Yard (yd²)" : "Квадратни јард (yd²)",
"Square Foot (ft²)" : "Квадратна стопа (ft²)",
"Square Inch (in²)" : "Квадратни инч (in²)",
"Square Kilometre (km²)" : "Квадратни километар (km²)",
"Hectare (ha)" : "Хектар (ha)",
"Acre" : "Ар",
"Square Metre (m²)" : "Квадратни метар (m²)",
"Square Centimetre (cm²)" : "Квадратни центиметар (cm²)",
"Square Millimetre (mm²)" : "Квадратни милиметар (mm²)",

"Litre (l)" : "Литар (l)",
"Cubic Metre (m³)" : "Кубни метар (m³)",
"Cubic Inch (in³)" : "Кубни инч (in³)",
"Cubic Foot (ft³)" : "Кубна стопа (ft³)",
"Cubic Yard (yd³)" : "Кубни јард (yd³)",
"Gallon (US) (gal)" : "Галон (САД) (gal)",
"Gallon (UK) (gal)" : "Галон (Велика Британија) (gal)",

"Gram (g)" : "Грам (g)",
"Kilogram (kg)" : "Килограм (kg)",
"Tonne (t)" : "Тона (t)",
"Milligram (mg)" : "Милиграм (mg)",
"Microgram (μg)" : "Микрограм (μg)",
"Ounce (oz)" : "Унца (oz)",
"Pound (lb)" : "Фунта (lb)",
"Carat" : "Карат",
"Slug" : "Слуг",
"Ounce (Troy) (ozt)" : "Унца (Troy) (ozt)",

"Year" : "Година",
"Month" : "Месец",
"Week" : "Недеља",
"Day" : "Дан",
"Hour (h)" : "Сат (h)",
"Minute (min)" : "Минута (min)",
"Second (s)" : "Друго (s)",
"Millisecond (ms)" : "Милисекунда (ms)",
"Microsecond (μs)" : "Микросекунда (μs)",
"Decade" : "Декада",
"Century" : "Столеће",
"Millennium" : "Миленијум",

"Kilometre per Hour (km/h)" : "Километара по сату (km/h)",
"Miles per Hour (mi/h)" : "Миља на сат (mi/h)",
"Metre per Second (m/s)" : "Метара у секунди (m/s)",
"Foot per Second (ft/s)" : "Стопа у секунди (ft/s)",
"Knot" : "Чвор",
"Speed of Light (c)" : "Брзина светлости (c)",
"Minutes per Mile (min/mi)" : "Минута по миљи (min/mi)",
"Minutes per Kilometre (min/km)" : "Минута по километру (min/km)",

"Celsius (°C)" : "Целзијус (°C)",
"Fahrenheit (°F)" : "Фаренхајт (°F)",
"Kelvin (K)" : "Келвин (K)",
"Rankine (°R)" : "Ранкин (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "Килограм / кубни Метар (kg/m³)",
"Kilogram/Litre (kg/l)" : "Килограм / литар (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "Слуг / кубна стопа (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "Фунта / кубна стопа (lbm/ft³)",

"Newton (N)" : "Њутн (N)",
"Kilonewton (kN)" : "Килоњутн (kN)",
"Atomic unit of force" : "Атомска јединица силе",
"Gram Force (gf)" : "Грам снаге (gf)",
"Pound Force (lbf)" : "Фунта снаге (lbf)",
"Poundal (pdl)" : "Поундал (pdl)",
"Dyne (dyn)" : "Дин (dyn)",
"Kilogram-force | Kilopond (kgf)" : "Килограм-силе | Килопонд (kgf)",

"Watt (W)" : "Ват (W)",
"Kilowatt (kW)" : "Киловат (kW)",
"Megawatt (MW)" : "Мегават (MW)",
"Milliwatt (mW)" : "Миливат (mW)",
"Microwatt (µW)" : "Микроват (µW)",
"Horsepower (hp)" : "Коњска снага (hp)",
"Calorie per Second (cal/s)" : "Калорија у секунди (cal/s)",

"Pascal (Pa)" : "Паскал (Pa)",
"Kilopascal (kPa)" : "Килопаскал (kPa)",
"Megapascal (MPa)" : "Мегапаскал (MPa)",
"Bar (bar)" : "Бар (bar)",
"Millibar (mbar)" : "Милибар (mbar)",
"Atmosphere (atm)" : "Атмосфера (АТМ)",
"Pound Force per Square Inch (psi)" : "Фунта снаге по квадратном инчу (psi)",
"Dyne per Square Centimetre" : "Дин по квадратном Центиметру",

"Degree (°)" : "Степен (°)",
"Radian (rad)" : "Радијан (rad)",
"π Radian (π rad)" : "π Радијан (π rad)",
"Second" : "Друго",
"Minute (')" : "Минута (')",

"Byte (B)" : "Бајт (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "Килобајт | Кибибајт (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "Мегабајт | Мебибајт (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Гигабајт | Гибибајт (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "Терабајт | Тебибајт (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "Петабајт | Пебибајт (PB | Pi)",
"Bit (b)" : "Бит (b)",
"Zebibyte (Zi)" : "Зебибајт (Zi)",

"Litres per 100 Kilometres (l/100km)" : "Литара на 100 километара (l/100km)",
"Kilometres per Litre (km/l)" : "Километара по литри (km/l)",
"Miles per Gallon (US) (mpg)" : "Миља по галону (САД) (mpg)",
"Miles per Gallon (UK) (mpg)" : "Миља по галону (Велика Британија) (mpg)",


};