﻿var language={

	"1. 2D SHAPES" : "1. 2D former",
"2. 3D SHAPES" : "2. 3D former",
"TRIANGLE" : "Triangel",
"RIGHT TRIANGLE" : "Rät triangel",
"SQUARE" : "Kvadrat",
"RECTANGLE" : "Rektangel",
"PARALLELOGRAM" : "Parallellogram",
"LOZENGE" : "Romb",
"TRAPEZOID" : "Parallelltrapets",
"CONVEX QUADRILATERAL" : "Konvex fyrhörning",
"CIRCLE" : "Cirkel",
"SEGMENT OF CIRCLE" : "Cirkelsegment",
"SECTOR OF CIRCLE" : "Cirkelsektor",
"REGULAR POLYGON OF N SIDES" : "Regelbunden polygon med N sidor",
"REGULAR POLYGON" : "Regelbunden polygon",
"REGULAR POLYGON" : "Regelbunden polygon",
"HEXAGON" : "Hexagon",
"SPHERE" : "Klot",
"CYLINDER" : "Cylinder",
"CYLINDER" : "Cylinder",
"CONE" : "Kon",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "Avstympat parti i rät cirkulär kon",
"PYRAMID" : "Pyramid",
"SQUARE PYRAMID" : "Fyrkantig pyramid",
"CUBOID" : "Rätblock",
"CUBOID" : "Rätblock",
"TRIANGULAR PRISM" : "Tresidigt prisma",
"A: Area" : "A: Area",
"A: Area, P: Perimeter" : "A: Area, P: Perimeter",
"b: Arc length" : "b: Båglängd",
"V: Volume, A: Surface Area" : "V: Volym, A: Yta",
"A: Lateral surface area" : "A: Mantelyta",
"A: Lateral surface area" : "A: Mantelyta",
"A: Base Area" : "A: Basyta",
"A: Surface Area" : "A: Yta",

"1. OPERATIONS ON EXPRESSIONS" : "1. Operationer på uttryck",
"POLYNOMIAL" : "Polynom",
"FRACTIONS" : "Bråk",
"IDENTITY" : "Identitet",
"EXPONENTIATION" : "Potenser",
"ROOTS" : "Rötter",
"2. RATE FORMULAS" : "2. Skalor",
"6. COMPLEX NUMBERS" : "6. Komplexa tal",
"COMPLEX PLANE" : "Komplexa planet",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. Följd \nAritmetisk följd",
"4. PROGRESSION" : "4. Följd", 
"ARITHMETIC PROGRESSION" : "Aritmetisk följd", 
"GEOMETRIC PROGRESSION" : "Geometrisk följd", 
"SUMMATIONS" : "Summering",
"5. LOGARITHM" : "5. Logaritm", 
"DECIMAL LOGARITHM" : "Tiologaritmen",
"NATURAL LOGARITHM" : "Naturliga logaritmen",
"d: common difference" : "d: gemensam skillnad",
"q: common ratio" : "q: gemensam förhållande",
"3. INEQUALITIES" : "3. Ojämlikhet",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "Trigonometriska funktioner för en rätvinklig triangel",
"TRIGONOMETRIC TABLE" : "Trigonometrisk tabell",
"CO-RATIOS" : "Likförhållanden",
"BASIC FORMULAS" : "Grundläggande formler",
"MULTIPLE ANGLE FORMULAS" : "Multipla vinkel formler",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "Potenser av trigonometriska funktioner",
"ADDITION FORMULAS" : "Additionformler",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "Summan av trigonometriska funktioner",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "Produkten av trigonometriska funktioner",
"HALF ANGLE FORMULAS" : "Halvvinkel formler",
"ANGLES OF A PLANE TRIANGLE" : "Vinklar i en plan triangel",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "Samband mellan trigonometriska funktioner",
"α, β, γ are 3 angles of a triangle" : "α, β, γ är de tre vinklarna i en triangel",


"1. ALGEBRAIC EQUATIONS" : "1. algebraiska ekvationer",
"LINEAR EQUATION" : "Linjär ekvation",
"SYSTEM OF TWO LINEAR EQUATIONS" : "Linjärekvationssystem",
"QUADRATIC EQUATION" : "Kvadratisk ekvation",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. Potenser och logaritmer \nExponentiell ekvation",
"2. EXPONENT AND LOGARITHM" : "2. Potenser och logaritmer",
"LOGARITHMIC EQUATION" : "Logaritmisk ekvation",
"3. TRIGONOMETRIC EQUATION" : "3. Trigonometrisk ekvation",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. Olikheter \nLinjär olikhet",
"4. INEQUATIONS" : "4. Ejlikamedtecken", 
"QUADRATIC INEQUATION" : "Kvadratisk Ejlikamedtecken",
"EXPONENTIAL INEQUATION" : "Exponentiell Ejlikamedtecken",
"LOGARITHMIC INEQUATION" : "Logaritmisk Ejlikamedtecken",
"TRIGONOMETRIC INEQUATIONS" : "Trigonometriska Ejlikamedtecken",
"LINEAR EQUATION" : "Linjär ekvation",
"EXPONENTIAL EQUATION" : "Exponentiell ekvation",
"LINEAR INEQUATION" : "Linjär Ejlikamedtecken",
"CUBIC EQUATION" : "Tredjegradsekvation",
"POINTS" : "Poäng",
"- Distance between two points A and B" : "- Avståndet mellan två punkter A och B",
"- Distance between point A and origin:" : "- Avståndet mellan punkt A och origo:",
"TRIANGLE" : "Triangel",
"- Area of triangle with vertices at A, B, C" : "- Arean på en triangel med hörn på A, B, C",
"- Area of a triangle with a vertice at origin" : "- Arean på en triangel med ett hörn i origo",
"EQUATION OF LINE" : "Linjär ekvation",
"- Joining two points A, B" : "Linjär ekvation",
"- Passing point A and parallel with line y=ax+b" : "- Går genom punkt A och parallellt med linjen y=ax+b",
"- Passing point A and perpendicular with line y=ax+b" : "- Går genom punkt A och vinkelrätt mot linjen y=ax+b",
"EQUATION OF CIRCLE" : "Ekvationen för en cirkel",
"- Circle with radius r and center at (a, b)" : "- Cirkel med radien r och mittpunkt (a, b)",
"- Circle with center at origin" : "- Cirkel med mittpunkt vid origo",
"ELLIPSE" : "Ellips",
"HYPERBOLA" : "Hyperbel",
"PARABOLA" : "Parabel",

"Limit \n" : "Gräns \n",
"Derivative \n" : "Derivata \n",
"C: constant" : "C: konstant",
"Differentiation \n" : "Differentiering \n",
"C: constant; u, v, w: functions of x" : "C: konstant; u, v, w: funktioner x",
"LIMIT" : "Gräns",
"DERIVATIVE" : "Derivata",
"DIFFERENTIATION" : "Differentiering",

"1. INDEFINITE INTEGRALS" : "1. Obestämda integraler",
"C: arbitrary constant, k: constant" : "C: godtycklig konstant, k: konstant",
"INTEGRALS BY PARTIAL FRACTIONS" : "Integration genom partialbråksuppdelning",
"INTEGRALS INVOLVING ROOTS" : "Integraler med rotfunktioner",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "Integraler med trigonometriska funktioner",
"2. DEFINITE INTEGRALS" : "2. Bestämda integraler",
"APPLICATIONS" : "Tillämpningar",
"- Surface area created by y=f(x)" : "- Ytan som definieras av y=f(x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- Volymen av formen som definieras av y=f(x) roteras runt axeln:",

"Median" : "Median",
"Angle bisector" : "Bisektris",
"SPHERICAL CAP" : "Klotformig hatt",
"SPHERICAL SEGMENT" : "Klotsegment",
"SPHERICAL SECTOR" : "Klotsektor",
"TORUS" : "Torus",
"FORMULAS WITH t=tan(x/2)" : "Formler med t = tan(x/2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "Sidor och vinklar i en plan triangel",
"Law of sines, cosines and tangents" : "Sinussatsen, cosinussatsen och tangenssatsen",

"1. PLANE ANALYTIC GEOMETRY" : "1. Plan analytisk geometri",
"2. SOLID ANALYTIC GEOMETRY" : "2. 3D analytisk geometri",
"LINE" : "Linje",
"- Direction Cosines of Line Joining Points A and B" : "- Riktning cosinus för linjen som förbinder punkterna A och B",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "Ekvationen för linjen som förbinder punkterna A, B",
"- In standard form" : "- I standardform",
"- In parametric form" : "- I parametrisk form",
"PLANE" : "PLAN",
"- General equation of a plane" : "- Allmän ekvation för ett plan",
"- Equation of plane passing through point A, B, C" : "- Ekvationen för det plan som går genom punkten A, B, C",
"- Equation of plane in intercept form" : "- Ekvationen för planet i nollställeformen",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a, b, c är respektive nollställena för x-, y-, z-axlarna",
"- Normal Form for Equation of Plane" : "- Normal form för ekvation som definierar ett plan",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p: vinkelräta avståndet från O till planet vid P; α, β, γ: vinklar mellan OP och positiva x, y, z-axlarna",
"- Distance from point M to a plane" : "- Avståndet från punkt M till ett plan",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Ekvation för ett klots mittpunkt M och radie R i rektangulära koordinater",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "Ekvationen för ellipsoiden med mittpunkt M och axlarna a, b, c",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Elliptisk cylinder med axeln som z-axeln",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "Elliptisk kon med axeln som z-axeln",
"HYPERBOLOID OF ONE SHEET" : "Hyperboloid av ett ark",
"HYPERBOLOID OF TWO SHEETS" : "Hyperboloid av två ark",
"ELLIPTIC PARABOLOID" : "Elliptisk paraboloid",
"HYPERBOLIC PARABOLOID" : "Hyperbolisk paraboloid",

"TRANSFORMATIONS" : "Transformationer",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. Särskilda obestämda integraler",
"Integrals involving ax+b" : "Integraler med ax+b",
"Integrals involving ax+b and px+q" : "Integraler med ax+b och px+q",
"Integrals involving x²+a²" : "Integraler med x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "Integraler med x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a²" : "Integraler medx²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "Integraler med ax²+bx+c",
"Integrals involving xⁿ+aⁿ" : "Integraler med xⁿ+aⁿ",
"Integrals involving sin(ax)" : "Integraler med sin(ax)",
"Integrals involving e^(ax)" : "Integraler med e^(ax)",
"Integrals involving ln(x)" : "Integraler med ln(x)",

"TRANSPOSE OF A MATRIX" : "Transponeringen av en matris",
"ADDITION AND SUBTRACTION OF MATRICES" : "Addition och subtraktion av matriser",
"MULTIPLICATION OF MATRICES" : "Multiplikation av matriser",
"DETERMINANT OF MATRIX" : "Determinanten för en matris",
"INVERSE OF MATRIX" : "Inversen av matrisen",
"EQUATION IN MATRIX FORM" : "Ekvationen i matrisform",
"PROPERTIES OF MATRIX CALCULATIONS" : "Egenskaper hos matrisberäkningar",
"LENGTH" : "Längd",
"AREA" : "Area",
"VOLUME" : "Volym",
"MASS" : "Massa",
"SPEED" : "Hastighet",
"TIME" : "Tid",
"TEMPERATURE" : "Temperatur",
"DENSITY" : "Densitet",
"FORCE" : "Kraft",
"ENERGY" : "Energi",
"POWER" : "Effekt",
"PRESSURE" : "Tryck",
"Do you know this?" : "Visste du det här?",
"What is special about these calculations?" : "Vad är speciellt med dessa beräkningar?",
"Can you find the interesting trait of these calculations?" : "Kan du hitta intressanta draget bland dessa beräkningar?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. KURVOR FÖR ÖVRIGA FUNKTIONER",
"Constant" : "Konstant",
"Absolute" : "Absoluta",
"Square Root" : "Kvadratroten",
"Parabolic" : "Parabolisk",
"Cubic" : "Kubisk",
"Reciprocal" : "Reciprok",
"Sec" : "Sec",
"Cosec" : "Cosec",
"6. FUNCTION TRANSFORMATIONS" : "6. Funktionstransformationer",
"Horizontal Shifting" : "Horisontell förskjutning",
"Vertical Shifting" : "Vertikal förskjutning",
"Reflection" : "Reflektion",
"Stretching" : "Sträckning",
"F, F1: Focus points (foci)" : "F, F1: Brännpunkter",
"AF=p: Parameter of Parabola" : "AF=p: Parameterna för en Parabel",

"Probability & Statistics" : "Sannolikhet och Statistik",
"1. SETS" : "1. Mängder",
"SET" : "Set",
"SUBSET" : "Delmängd",
"INTERSECTION" : "Korsning",
"UNION" : "Union",
"SYMMETRIC DIFFERENCE" : "Symmetrisk differens",
"RELATIVE COMPLEMENT OF A IN B" : "Relativ komplement till A i B",
"ABSOLUTE COMPLEMENT" : "Absolut komplement",
"OPERATIONS ON SETS" : "Operationer på mängder",
"2. COMBINATIONS AND PERMUTATIONS" : "2. Kombinationer och permutationer",
"COMBINATIONS" : "Kombinationer",
"PERMUTATIONS" : "Permutationer",
"3. PROBABILITY" : "3. Sannolikhet",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) Om A och B är ömsesidigt uteslutande, (2) Om A och B är oberoende",
"4. STATISTICS" : "4. Statistik",
"MEAN" : "Medelvärde",
"MEDIAN" : "Median",
"MODE" : "Typvärde",
"Numerical value that occurs the most number of times" : "Numeriska värdet som inträffar flest antal gånger",
"EXAMPLE" : "Exempel",
"GEOMETRIC MEAN" : "Geometriskt medelvärde",
"HARMONIC MEAN" : "Harmoniska medelvärdet",
"VARIANCE" : "Varians",
"STANDARD DEVIATION" : "Standardavvikelse",
"MEAN DEVIATION" : "Genomsnittlig avvikelse",
"ROOT MEAN SQUARE" : "Kvadratiska medelvärdet",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "Normalfördelning (normalfördelning)",
"f: density function, F: Distribution function, μ: mean" : "f: täthetsfunktionen, F: Distribution funktion, μ: medelvärde",
"EXPONENTIAL DISTRIBUTION" : "Exponentialfördelning",
"POISSON DISTRIBUTION" : "Poissonfördelning",
"UNIFORM DISTRIBUTION" : "Likformig fördelning",
"Transforms" : "Transformer",
"1. FOURIER SERIES AND TRANSFORMS" : "1. Fourierserier och transformationer",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): periodisk funktion, perioden 2L; a_n, b_n: Fourierkoefficienterna; c_n: komplex Fourierkoefficient",
"REAL FORM OF FOURIER SERIES" : "Reella formen för Fourierserier",
"COMPLEX FORM" : "Komplex form",
"PARSEVAL’S THEOREM" : "Parsevals sats",
"FOURIER TRANSFORM" : "Fouriertransformen",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): funktion av x, F(s): Fouriertransformen av f(x)",
"CONVOLUTION" : "Faltning",
"CORRELATION" : "Korrelation",
"f*: complex conjugate of f" : "f*: komplexkonjugatet av f",
"FOURIER SYMMETRY RELATIONSHIPS" : "Fourier-symmetri relationer",
"FOURIER TRANSFORM PAIRS" : "Fourier-transparen",
"2. LAPLACE TRANSFORMS" : "2. Laplace transformationer",
"DEFINITION" : "Definition",
"CONVOLUTION" : "Faltning",
"INVERSE" : "Invers",
"a: constant" : "a: konstant",
"DERIVATIVE" : "Derivata",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "Substitution (Frekvensväxling)",
"TRANSLATION (TIME SHIFTING)" : "Förflyttning (Tidsväxling)",
"LAPLACE TRANSFORM PAIRS" : "Laplacetransformpar",

"Math Tricks" : "Matte Tricks",
"1. ADDITION" : "1. Addition",
"2. SUBTRACTION" : "2. Subtraktion",
"3. MULTIPLICATION" : "3. Multiplikation",
"4. DIVISION" : "4. Division",
"5. SQUARING" : "5. Kvadrering",
"6. EXPONENTIATION" : "6. Potenser",
"7. ROOTS" : "7. Rötter",
"8. SUMMATIONS" : "8. Summering",
"6. SUMMATIONS" : "6. Summering",

"UNITS CONVERTER" : "ENHETSOMVANDLARE",
"ANGLE" : "Vinkel",
"COMPUTER DATA" : "Datordata",
"FUEL CONSUMPTION" : "Bränsleförbrukning",
"Click here to download Physics Formulas app" : "Klicka här för att ladda ner fysikformelappen",
"All Maths formulas for your work and study" : "Alla matteformler till arbetet och studierna",
"EDIT" : "Redigera",
"ADD" : "Lägg till",
"TITLE OF THE PICTURE" : "Namnet på bilden",
"GAME" : "Spel",
"START" : "Start",
"LEVEL" : "Nivå",
"RETRY" : "Återförsök",
"SCORE" : "Poäng",
"TIME" : "Tid",
"BEST" : "Bäst",
"GAME OVER" : "Spel Slut",
"YOUR SCORE IS: " : "Dina poäng:",
"Title of new tool" : "Namn på nya verktyget",
"Name of the variable" : "Namn på variabeln",
"Add more variable" : "Lägg till fler variabler",
"Formula of the tool" : "Verktygsformeln",
"Name of the result " : "Namn på resultatet",
"Add more result" : "Lägg till fler resultat",

"Save favorite successful" : "Spara favorit lyckades",
"Save favorite failed" : "Spara favorit misslyckades",
"Save tool successful" : "Spara verktyget lyckades",
"Save tool failed" : "Spara verktyget misslyckades",
"Free version is limited with only 3 variables. For more, please use Full version" : "Gratisversionen är begränsad till endast 3 variabler. För mer information vänligen använd Fullständiga versionen.",
"Are you sure deleting this?" : "Är du säker du vill radera detta?",
"Go to Full version" : "Gå till Fullständiga versionen",
"Delete invalid" : "Radera ogiltig",
"EULER'S FORMULA" : "Eulers formel",


"Metre (m)" : "Meter (m)",
"Kilometre (km)" : "Kilometer (km)",
"Centimetre (cm)" : "Centimeter (cm)",
"Millimetre (mm)" : "Millimeter (mm)",
"Micrometre | Mircon (μm)" : "Mikrometer (μm)",
"Nanometre (nm)" : "Nanometer (nm)",
"Inch (in)" : "Tum (in)",
"Foot (ft)" : "Fot (ft)",
"Yard (yd)" : "Yard (yd)",
"Mile (mi)" : "Britisk mile (mi)",
"Light Year" : "Ljusår",
"Nautical Mile (NM)" : "Sjömil (Nautisk mil) (NM)",
"Angstrom (Å)" : "Ångström (Å)",

"Square Mile (mi²)" : "Kvadrat britisk mile (mi²)",
"Square Yard (yd²)" : "Kvadratyard (yd²)",
"Square Foot (ft²)" : "Kvadratfot (ft²)",
"Square Inch (in²)" : "Kvadrattum (in²)",
"Square Kilometre (km²)" : "Kvadratkilometer (km²)",
"Hectare (ha)" : "Hektar (ha)",
"Acre" : "Britisk acre",
"Square Metre (m²)" : "Kvadratmeter (m²)",
"Square Centimetre (cm²)" : "Kvadratcentimeter (cm²)",
"Square Millimetre (mm²)" : "Kvadratmilimeter (mm²)",

"Litre (l)" : "Liter (l)",
"Cubic Metre (m³)" : "Kubikmeter (m³)",
"Cubic Inch (in³)" : "Kubiktum (in³)",
"Cubic Foot (ft³)" : "Kubikfot (ft³)",
"Cubic Yard (yd³)" : "Kubikmeter (yd³)",
"Gallon (US) (gal)" : "Gallon (US) (gal)",
"Gallon (UK) (gal)" : "Gallon (UK) (gal)",

"Gram (g)" : "Gram (g)",
"Kilogram (kg)" : "Kilogram (kg)",
"Tonne (t)" : "Ton (t)",
"Milligram (mg)" : "Milligram (mg)",
"Microgram (μg)" : "Mikrogram (μg)",
"Ounce (oz)" : "Ounce (US) (oz)",
"Pound (lb)" : "Pund (lb)",
"Carat" : "Karat",
"Slug" : "Slug",
"Ounce (Troy) (ozt)" : "Ounce (Troy) (ozt)",

"Year" : "År",
"Month" : "Månad",
"Week" : "Vecka",
"Day" : "Dag",
"Hour (h)" : "Timme (h)",
"Minute (min)" : "Minut (min)",
"Second (s)" : "Sekund (s)",
"Millisecond (ms)" : "Millisekund (ms)",
"Microsecond (μs)" : "Mikrosekund (μs)",
"Decade" : "Årtionde",
"Century" : "Århundrade",
"Millennium" : "Millennium",

"Kilometre per Hour (km/h)" : "Kilometer per timme (km/h)",
"Miles per Hour (mi/h)" : "Miles per timme (mi/h)",
"Metre per Second (m/s)" : "Meter per sekund (m/s)",
"Foot per Second (ft/s)" : "Fot per sekund (ft/s)",
"Knot" : "Knop",
"Speed of Light (c)" : "Ljusets hastighet (c)",
"Minutes per Mile (min/mi)" : "Minuter per Mile (min/mi)",
"Minutes per Kilometre (min/km)" : "Minuter per Kilometer (min/km)",

"Celsius (°C)" : "Celsius (°C)",
"Fahrenheit (°F)" : "Fahrenheit (°F)",
"Kelvin (K)" : "Kelvin (K)",
"Rankine (°R)" : "Rankine (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "Kilogram/Kubikmeter (kg/m³)",
"Kilogram/Litre (kg/l)" : "Kilogram/Liter (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "Slug/Kubikfot (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "Pund/Kubikfot (lbm/ft³)",

"Newton (N)" : "Newton (N)",
"Kilonewton (kN)" : "Kilonewton (kN)",
"Atomic unit of force" : "Atomisk kraftenhet",
"Gram Force (gf)" : "Gram kraft (gf)",
"Pound Force (lbf)" : "Pund kraft (lbf)",
"Poundal (pdl)" : "Poundal (pdl)",
"Dyne (dyn)" : "Dyne (dyn)",
"Kilogram-force | Kilopond (kgf)" : "Kilopond | Kilopond (kgf)",

"Watt (W)" : "Watt (W)",
"Kilowatt (kW)" : "Kilowatt (kW)",
"Megawatt (MW)" : "Megawatt (MW)",
"Milliwatt (mW)" : "Milliwatt (mW)",
"Microwatt (µW)" : "Mikrowatt (µW)",
"Horsepower (hp)" : "Hästkraft (hp)",
"Calorie per Second (cal/s)" : "Kalorier per sekund (cal/s)",

"Pascal (Pa)" : "Pascal (Pa)",
"Kilopascal (kPa)" : "Kilopascal (kPa)",
"Megapascal (MPa)" : "Megapascal (MPa)",
"Bar (bar)" : "Bar (bar)",
"Millibar (mbar)" : "Millibar (mbar)",
"Atmosphere (atm)" : "Atmosfär (atm)",
"Pound Force per Square Inch (psi)" : "Pund Kraft per kvadrattum (psi)",
"Dyne per Square Centimetre" : "Dyn per kvadratcentimeter",

"Degree (°)" : "Grad (°)",
"Radian (rad)" : "Radian (rad)",
"π Radian (π rad)" : "π Radian (π rad)",
"Second" : "Sekund",
"Minute (')" : "Minut (')",

"Byte (B)" : "Bitgrupp (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "Kilobyte | Kibibyte (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "Megabyte | Mebibyte (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Gigabyte | Gibibyte (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "Terabyte | Tebibyte (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "Petabyte | Pebibyte (PB | Pi)",
"Bit (b)" : "Bit (b)",
"Zebibyte (Zi)" : "Zebibyte (Zi)",

"Litres per 100 Kilometres (l/100km)" : "Liter per 100 kilometer (l/100km)",
"Kilometres per Litre (km/l)" : "Kilometer per liter (km/l)",
"Miles per Gallon (US) (mpg)" : "Miles per gallon (US) (mpg)",
"Miles per Gallon (UK) (mpg)" : "Miles per gallon (UK) (mpg)",




};