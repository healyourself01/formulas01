﻿var language={

"1. 2D SHAPES" : "1 。 2Dの図形",
"2. 3D SHAPES" : "2 。 3Dの図形",
"TRIANGLE" : "三角形",
"RIGHT TRIANGLE" : "直角三角形",
"SQUARE" : "正方形",
"RECTANGLE" : "長方形",
"PARALLELOGRAM" : "平行四辺形",
"LOZENGE" : "ひし形",
"TRAPEZOID" : "台形",
"CONVEX QUADRILATERAL" : "凸四辺形",
"CIRCLE" : "円形",
"SEGMENT OF CIRCLE" : "円形のセグメント",
"SECTOR OF CIRCLE" : "扇形",
"REGULAR POLYGON OF N SIDES" : "N個の辺の正多角形",
"REGULAR POLYGON" : "正多角形",
"REGULAR POLYGON" : "正多角形",
"HEXAGON" : "六角形",
"SPHERE" : "球",
"CYLINDER" : "円柱",
"CYLINDER" : "円柱",
"CONE" : "円錐",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "円錐台",
"PYRAMID" : "角錐",
"SQUARE PYRAMID" : "四角錐",
"CUBOID" : "直方体",
"CUBOID" : "直方体",
"TRIANGULAR PRISM" : "三角柱",
"A: Area" : "A：面積",
"A: Area, P: Perimeter" : "A：面積、 P ：周囲",
"b: Arc length" : "b：アーク長",
"V: Volume, A: Surface Area" : "V：体積、A ：表面積",
"A: Lateral surface area" : "A：横表面積",
"A: Lateral surface area" : "A：横表面積",
"A: Base Area" : "A：ベース面積",
"A: Surface Area" : "A：表面積",

"1. OPERATIONS ON EXPRESSIONS" : "1 。式の操作",
"POLYNOMIAL" : "多項式",
"FRACTIONS" : "分数",
"IDENTITY" : "恒等式",
"EXPONENTIATION" : "冪乗",
"ROOTS" : "冪根",
"2. RATE FORMULAS" : "2 。速度公式",
"6. COMPLEX NUMBERS" : "6 。複素数",
"COMPLEX PLANE" : "複素平面",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4 。進行\n等差数列",
"4. PROGRESSION" : "4 。進行", 
"ARITHMETIC PROGRESSION" : "等差数列", 
"GEOMETRIC PROGRESSION" : "等比数列", 
"SUMMATIONS" : "総和",
"5. LOGARITHM" : "5 。対数", 
"DECIMAL LOGARITHM" : "常用対数",
"NATURAL LOGARITHM" : "自然対数",
"d: common difference" : "d：公差",
"q: common ratio" : "q ：公比",
"3. INEQUALITIES" : "3 。不平等",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "直角三角形のための三角関数",
"TRIGONOMETRIC TABLE" : "三角関数テーブル",
"CO-RATIOS" : "共同比",
"BASIC FORMULAS" : "基本式",
"MULTIPLE ANGLE FORMULAS" : "複数の角度式",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "三角関数の乗",
"ADDITION FORMULAS" : "加法公式",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "三角関数の合計",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "三角関数の積",
"HALF ANGLE FORMULAS" : "半角式",
"ANGLES OF A PLANE TRIANGLE" : "三角形の角度",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "三角関数の関係",
"α, β, γ are 3 angles of a triangle" : "α 、 βは、 γは、三角形の3角である",


"1. ALGEBRAIC EQUATIONS" : "1 。代数方程式",
"LINEAR EQUATION" : "線形方程式",
"SYSTEM OF TWO LINEAR EQUATIONS" : "2連立一次方程式",
"QUADRATIC EQUATION" : "二次方程式",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2 。指数と対数\n指数方程式",
"2. EXPONENT AND LOGARITHM" : "2 。指数と対数",
"LOGARITHMIC EQUATION" : "対数方程式",
"3. TRIGONOMETRIC EQUATION" : "3 。三角方程式",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4 。不平等\n線形不等式", 
"4. INEQUATIONS" : "4 。不等式",
"QUADRATIC INEQUATION" : "二次不等式",
"EXPONENTIAL INEQUATION" : "指数関数的不等式",
"LOGARITHMIC INEQUATION" : "対数不等式",
"TRIGONOMETRIC INEQUATIONS" : "三角不等式",
"LINEAR EQUATION" : "線形方程式",
"EXPONENTIAL EQUATION" : "指数方程式",
"LINEAR INEQUATION" : "一次不等式",
"CUBIC EQUATION" : "三次方程式",
"POINTS" : "点",
"- Distance between two points A and B" : "- 二つの点AとBの間の距離",
"- Distance between point A and origin:" : "- 点Aと原点の間の距離：",
"TRIANGLE" : "三角形",
"- Area of triangle with vertices at A, B, C" : "- での頂点を持つ三角形の面積A、B、 C",
"- Area of a triangle with a vertice at origin" : "- 原点に頂点とする三角形の面積",
"EQUATION OF LINE" : "直線の方程式",
"- Joining two points A, B" : "- 2点を結ぶA、B",
"- Passing point A and parallel with line y=ax+b" : "- ラインy=ax+bと点Aと並列を渡す",
"- Passing point A and perpendicular with line y=ax+b" : "- ラインy=ax+bと点Aと垂直を渡す",
"EQUATION OF CIRCLE" : "円形の方程式",
"- Circle with radius r and center at (a, b)" : "- 半径rと（a,b)を中心とするサークル",
"- Circle with center at origin" : "- 原点を中心とするサークル",
"ELLIPSE" : "楕円",
"HYPERBOLA" : "双曲線",
"PARABOLA" : "放物線",

"Limit \n" : "を制限 \n",
"Derivative \n" : "導関数\n",
"C: constant" : "C：定数",
"Differentiation \n" : "微分\n",
"C: constant; u, v, w: functions of x" : "C：定数、 U、V、W ： xの関数",
"LIMIT" : "を制限",
"DERIVATIVE" : "微分法",
"DIFFERENTIATION" : "分化",

"1. INDEFINITE INTEGRALS" : "1 。不定積分",
"C: arbitrary constant, k: constant" : "C：任意の定数、k ：定数",
"INTEGRALS BY PARTIAL FRACTIONS" : "部分分数によって積分",
"INTEGRALS INVOLVING ROOTS" : "根を含む積分",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "三角関数を含む積分",
"2. DEFINITE INTEGRALS" : "2 。定積分",
"APPLICATIONS" : "アプリケーション",
"- Surface area created by y=f(x)" : "- y=f(x)よりの表面積を計算せよ",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- 軸の周りに回転するy=f(x)により固体の体積を計算せよ",

"Median" : "中線",
"Angle bisector" : "二等分線",
"SPHERICAL CAP" : "球状キャップ",
"SPHERICAL SEGMENT" : "球状​​セグメント",
"SPHERICAL SECTOR" : "球状​​セクター",
"TORUS" : "トーラス",
"FORMULAS WITH t=tan(x/2)" : "t=tan(x/2)と数式",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "側面と平面の三角形の角度",
"Law of sines, cosines and tangents" : "正弦、余弦および接線の法則",

"1. PLANE ANALYTIC GEOMETRY" : "1. 平面解析的幾何学",
"2. SOLID ANALYTIC GEOMETRY" : "2. 固体解析幾何学 ",
"LINE" : "ライン",
"- Direction Cosines of Line Joining Points A and B" : "- ラインの方向余弦は、ポイントAとBの結合",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "直線の式は、A、B二点を結ぶ",
"- In standard form" : "- 標準的な形式で",
"- In parametric form" : "- パラメトリック形式で",
"PLANE" : "平面",
"- General equation of a plane" : "- 平面の一般式",
"- Equation of plane passing through point A, B, C" : "- 平面の方程式は A、B、C.三点を通る",
"- Equation of plane in intercept form" : "- 平面の切片方程式",
"a,b,c are the intercept on the x,y,z axes, respectively" : "のx、y、z軸切片のa、b、cをそれぞれ、",
"- Normal Form for Equation of Plane" : "- ノーマルタイプの平面方程式",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "pは原点から平面までの距離であり、法線ベクトルのcosα、cosβ、cosγ平面方向余弦",
"- Distance from point M to a plane" : "- 平面からの距離に点M",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Mと半径rのグローブのデカルト方程式",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "楕円面",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "楕円柱面",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "錐面",
"HYPERBOLOID OF ONE SHEET" : "一葉双曲面",
"HYPERBOLOID OF TWO SHEETS" : "二葉双曲面",
"ELLIPTIC PARABOLOID" : "楕円放物面",
"HYPERBOLIC PARABOLOID" : "双曲放物面",

"TRANSFORMATIONS" : "変換",
"3. SPECIAL INDEFINITE INTEGRALS" : "3 。特別な不定積分",
"Integrals involving ax+b" : "ax+ bを含む積分",
"Integrals involving ax+b and px+q" : "ax+ bとpx+qを含む積分",
"Integrals involving x²+a²" : "x²+a²を含む積分",
"Integrals involving x²-a², x²&gt;a²" : "x²-a²で、x²&gt;a²を含む積分",
"Integrals involving x²-a², x²&lt;a²" : "x²-a²で、x²&lt;a²を含む積分",
"Integrals involving ax²+bx+c" : "ax²+bx+ c が関与する積分",
"Integrals involving xⁿ+aⁿ" : "xⁿ +aⁿ個を含む積分",
"Integrals involving sin(ax)" : "sin(ax)を含む積分",
"Integrals involving e^(ax)" : "e^(ax)を含む積分",
"Integrals involving ln(x)" : "ln(x)を含む積分",

"TRANSPOSE OF A MATRIX" : "転置行列",
"ADDITION AND SUBTRACTION OF MATRICES" : "行列の加算と減算",
"MULTIPLICATION OF MATRICES" : "行列の掛け算",
"DETERMINANT OF MATRIX" : "行列式",
"INVERSE OF MATRIX" : "行列の逆行列",
"EQUATION IN MATRIX FORM" : "行列形式で方程式",
"PROPERTIES OF MATRIX CALCULATIONS" : "行列計算の性質",
"LENGTH" : "長さ",
"AREA" : "面積",
"VOLUME" : "体積",
"MASS" : "重量",
"SPEED" : "速度",
"TIME" : "時間",
"TEMPERATURE" : "温度",
"DENSITY" : "密度",
"FORCE" : "力",
"ENERGY" : "エネルギー",
"POWER" : "パワー",
"PRESSURE" : "圧力",
"Do you know this?" : "あなたはこれを知っていますか？",
"What is special about these calculations?" : "これらの計算は何か特別なことがありませんか？",
"Can you find the interesting trait of these calculations?" : "これらの計算の特徴を見つけませんか。",
"5. GRAPH OF OTHER FUNCTIONS" : "5. 関数 のグラフ",
"Constant" : "定数",
"Absolute" : "絶対値",
"Square Root" : "平方根 ",
"Parabolic" : "自乗",
"Cubic" : "立方数",
"Reciprocal" : "逆数",
"Sec" : "Sec",
"Cosec" : "Cosec",
"6. FUNCTION TRANSFORMATIONS" : "6. 変換",
"Horizontal Shifting" : "水平移動",
"Vertical Shifting" : "垂直移動",
"Reflection" : "鏡映",
"Stretching" : "ストレッチング",
"F, F1: Focus points (foci)" : "F, F1: 焦点",
"AF=p: Parameter of Parabola" : "AF=p: 放物線のパラメータ",

"Probability & Statistics" : "確率と統計学",
"1. SETS" : "1. 集合",
"SET" : "集合",
"SUBSET" : "部分集合",
"INTERSECTION" : "交差",
"UNION" : "和集合",
"SYMMETRIC DIFFERENCE" : "対称差",
"RELATIVE COMPLEMENT OF A IN B" : "差集合",
"ABSOLUTE COMPLEMENT" : "絶対的補数",
"OPERATIONS ON SETS" : "集合の演算",
"2. COMBINATIONS AND PERMUTATIONS" : "2. 順列と組み合わせ",
"COMBINATIONS" : "組合せ",
"PERMUTATIONS" : "置換",
"3. PROBABILITY" : "3. 確率",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "（1）A及びBが互いに排他的である場合、（2）A及びBは独立している場合",
"4. STATISTICS" : "4. 統計学",
"MEAN" : "平均 ",
"MEDIAN" : "中線",
"MODE" : "最頻値",
"Numerical value that occurs the most number of times" : "値が最も頻繁に発生します ",
"EXAMPLE" : "例 ",
"GEOMETRIC MEAN" : "幾何平均 ",
"HARMONIC MEAN" : "調和平均 ",
"VARIANCE" : "分散 ",
"STANDARD DEVIATION" : "標準偏差 ",
"MEAN DEVIATION" : "平均偏差 ",
"ROOT MEAN SQUARE" : "二乗平均平方根",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "正規分布（ガウス分布） ",
"f: density function, F: Distribution function, μ: mean" : "F：密度関数、F：配布機能、μ：平均値",
"EXPONENTIAL DISTRIBUTION" : "指数分布 ",
"POISSON DISTRIBUTION" : "ポアソン分布 ",
"UNIFORM DISTRIBUTION" : "一様分布 ",
"Transforms" : "関数変換",
"1. FOURIER SERIES AND TRANSFORMS" : "1。フーリエ変換 と級数",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x)： 周期関数、周期2L; a_n, b_n：フーリエ係数; c_n：複素フーリエ係数 ",
"REAL FORM OF FOURIER SERIES" : "フーリエ級数の本来の型",
"COMPLEX FORM" : "複合形 ",
"PARSEVAL’S THEOREM" : "パーセバルの定理 ",
"FOURIER TRANSFORM" : "フーリエ変換 ",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x)：xの関数、F(s)：f(x)のフーリエ変換 ",
"CONVOLUTION" : "畳み込み",
"CORRELATION" : "相関",
"f*: complex conjugate of f" : "f*：fの共役複素数",
"FOURIER SYMMETRY RELATIONSHIPS" : "フーリエ対称性との関係 ",
"FOURIER TRANSFORM PAIRS" : "フーリエ変換のペアを変換 ",
"2. LAPLACE TRANSFORMS" : "2。ラプラス変換 ",
"DEFINITION" : "定義 ",
"CONVOLUTION" : "畳み込み",
"INVERSE" : "逆ラプラス変換",
"a: constant" : "a：定数 ",
"DERIVATIVE" : "微分法",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "代入(周波シフティング）",
"TRANSLATION (TIME SHIFTING)" : "タイム·ドメイン変換 ",
"LAPLACE TRANSFORM PAIRS" : "ラプラス変換の対 ",

"Math Tricks" : "暗算",
"1. ADDITION" : "1. 加法",
"2. SUBTRACTION" : "2. 減法",
"3. MULTIPLICATION" : "3. 乗法",
"4. DIVISION" : "4. 除法",
"5. SQUARING" : "5. 自乗",
"6. EXPONENTIATION" : "6. 冪乗",
"7. ROOTS" : "7. 冪根",
"8. SUMMATIONS" : "8. 総和",
"6. SUMMATIONS" : "6. 総和",

"UNITS CONVERTER" : "単位コンバータ ",
"ANGLE" : "角度",
"COMPUTER DATA" : "コンピュータのデータ ",
"FUEL CONSUMPTION" : "燃料消費量 ",
"Click here to download Physics Formulas app" : "物理学の数式アプリをダウンロードするには、ここをクリックしてください ",
"All Maths formulas for your work and study" : "あなたの仕事と学習のためのすべての数学式",
"EDIT" : "編集 ",
"ADD" : "加える ",
"TITLE OF THE PICTURE" : "絵のタイトル ",
"GAME" : "ゲーム ",
"START" : "スタート ",
"LEVEL" : "レベル ",
"RETRY" : "再試行 ",
"SCORE" : "スコア ",
"TIME" : "時間",
"BEST" : "最高 ",
"GAME OVER" : "ゲームオーバー ",
"YOUR SCORE IS: " : "あなたのスコアは、 :",


"Metre (m)" : "メートル  (m)",
"Kilometre (km)" : "キロメートル  (km)",
"Centimetre (cm)" : "センチメートル (cm)",
"Millimetre (mm)" : "ミリメートル (mm)",
"Micrometre | Mircon (μm)" : "マイクロメートル| ミクロン (μm)",
"Nanometre (nm)" : "ナノメートル (nm)",
"Inch (in)" : "インチ  (in)",
"Foot (ft)" : "フット  (ft)",
"Yard (yd)" : "ヤード (yd)",
"Mile (mi)" : "マイル  (mi)",
"Light Year" : "光年 ",
"Nautical Mile (NM)" : "海里  (NM)",
"Angstrom (Å)" : "オングストローム  (Å)",

"Square Mile (mi²)" : "スクエアマイル  (mi²)",
"Square Yard (yd²)" : "平方ヤード  (yd²)",
"Square Foot (ft²)" : "平方フット  (ft²)",
"Square Inch (in²)" : "平方インチ  (in²)",
"Square Kilometre (km²)" : "スクエアキロメートル  (km²)",
"Hectare (ha)" : "ヘクタール  (ha)",
"Acre" : "アクレ州 ",
"Square Metre (m²)" : "スクエアメートル  (m²)",
"Square Centimetre (cm²)" : "スクエアセンチメートル (cm²)",
"Square Millimetre (mm²)" : "スクエアミリメートル (mm²)",

"Litre (l)" : "リットル  (l)",
"Cubic Metre (m³)" : "キュービックメートル  (m³)",
"Cubic Inch (in³)" : "立方インチ  (in³)",
"Cubic Foot (ft³)" : "立方フート  (ft³)",
"Cubic Yard (yd³)" : "キュービック·ヤード  (yd³)",
"Gallon (US) (gal)" : "ガロン（米国）  (gal)",
"Gallon (UK) (gal)" : "ガロン（英国）  (gal)",

"Gram (g)" : "グラム  (g)",
"Kilogram (kg)" : "キログラム  (kg)",
"Tonne (t)" : "メートルトン  (t)",
"Milligram (mg)" : "ミリグラム  (mg)",
"Microgram (μg)" : "マイクログラム  (μg)",
"Ounce (oz)" : "オンス  (oz)",
"Pound (lb)" : "パウンド  (lb)",
"Carat" : "カラット ",
"Slug" : "スラッグ ",
"Ounce (Troy) (ozt)" : "オンス（トロイ）  (ozt)",

"Year" : "年 ",
"Month" : "月 ",
"Week" : "週 ",
"Day" : "日",
"Hour (h)" : "時間 (h)",
"Minute (min)" : "分  (min)",
"Second (s)" : "秒  (s)",
"Millisecond (ms)" : "ミリ秒  (ms)",
"Microsecond (μs)" : "マイクロ秒  (μs)",
"Decade" : "10年",
"Century" : "世紀",
"Millennium" : "千年間 ",

"Kilometre per Hour (km/h)" : "時間あたりのキロメートル  (km/h)",
"Miles per Hour (mi/h)" : "マイル/時間  (mi/h)",
"Metre per Second (m/s)" : "毎秒メートル  (m/s)",
"Foot per Second (ft/s)" : "秒あたりのフット  (ft/s)",
"Knot" : "ノット",
"Speed of Light (c)" : "光の速度  (c)",
"Minutes per Mile (min/mi)" : "マイルあたりの議事録  (min/mi)",
"Minutes per Kilometre (min/km)" : "キロメートルあたりの議事録  (min/km)",

"Celsius (°C)" : "セ氏  (°C)",
"Fahrenheit (°F)" : "ファーレンハイト  (°F)",
"Kelvin (K)" : "ケルビン  (K)",
"Rankine (°R)" : "ランキン  (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "キログラム/立方メートル  (kg/m³)",
"Kilogram/Litre (kg/l)" : "キログラム/リットル  (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "スラッグ/立方フィート  (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "ポンド/立方フィート  (lbm/ft³)",

"Newton (N)" : "ニュートン  (N)",
"Kilonewton (kN)" : "キロニュートン  (kN)",
"Atomic unit of force" : "原子間力ユニット ",
"Gram Force (gf)" : "グラムフォース  (gf)",
"Pound Force (lbf)" : "ポンドフォース  (lbf)",
"Poundal (pdl)" : "パウンダル (pdl)",
"Dyne (dyn)" : "ダイン  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "キログラムフォース|キロポンド  (kgf)",

"Watt (W)" : "ワット  (W)",
"Kilowatt (kW)" : "キロワット  (kW)",
"Megawatt (MW)" : "メガワット  (MW)",
"Milliwatt (mW)" : "ミリワット  (mW)",
"Microwatt (µW)" : "マイクロワット  (µW)",
"Horsepower (hp)" : "馬力  (hp)",
"Calorie per Second (cal/s)" : "秒あたりのカロリー  (cal/s)",

"Pascal (Pa)" : "パスカル  (Pa)",
"Kilopascal (kPa)" : "キロパスカル  (kPa)",
"Megapascal (MPa)" : "メガパスカル  (MPa)",
"Bar (bar)" : "バー  (bar)",
"Millibar (mbar)" : "ミリバール  (mbar)",
"Atmosphere (atm)" : "雰囲気 ",
"Pound Force per Square Inch (psi)" : "平方インチあたりポンドフォース  (psi)",
"Dyne per Square Centimetre" : "スクエアセンチメートルあたりダイン",

"Degree (°)" : "度 (°)",
"Radian (rad)" : "ラジアン  (rad)",
"π Radian (π rad)" : "πラジアン  (π rad)",
"Second" : "秒 ",
"Minute (')" : "分  (')",

"Byte (B)" : "バイト  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "キロバイト| KIBIBYTE  (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "メガバイト|メビバイト  (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "ギガバイト|ギビバイト  (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "テラバイト|テビバイト  (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "ペタバイト|ペビバイト  (PB | Pi)",
"Bit (b)" : "ビット  (b)",
"Zebibyte (Zi)" : "ゼビバイト|Zebibyte  (Zi)",

"Litres per 100 Kilometres (l/100km)" : "100キロメートルあたりのリットル  (l/100km)",
"Kilometres per Litre (km/l)" : "リットルあたりのキロメートル  (km/l)",
"Miles per Gallon (US) (mpg)" : "ガロンあたりのマイル（米国）  (mpg)",
"Miles per Gallon (UK) (mpg)" : "ガロンあたりのマイル（英国） (mpg)",

"Title of new tool" : "新しいツールのタイトル",
"Name of the variable" : "変数の名前",
"Add more variable" : "変数を追加します。",
"Formula of the tool" : "ツールの式",
"Name of the result " : "結果の名前",
"Add more result" : "結果を追加",

"EULER'S FORMULA" : "オイラーの公式",

};