﻿var language={

"1. 2D SHAPES" : "1. 2 डी आकृतियाँ",
"2. 3D SHAPES" : "2. 3 डी आकृतियाँ",
"TRIANGLE" : "त्रिभुज",
"RIGHT TRIANGLE" : "समकोण त्रिभुज",
"SQUARE" : "वर्गाकार",
"RECTANGLE" : "आयत",
"PARALLELOGRAM" : "समानांतर चतुर्भुज",
"LOZENGE" : "विषमकोण",
"TRAPEZOID" : "समलंब चर्तुभुज",
"CONVEX QUADRILATERAL" : "उत्तल चतुर्भुज",
"CIRCLE" : "वृत्त",
"SEGMENT OF CIRCLE" : "वृत्तखण्ड",
"SECTOR OF CIRCLE" : "सर्कल के सेक्टर",
"REGULAR POLYGON OF N SIDES" : "एन पक्षों के नियमित बहुभुज",
"REGULAR POLYGON" : "समबहुभुज",
"REGULAR POLYGON" : "समबहुभुज",
"HEXAGON" : "षट्भुज",
"SPHERE" : "गोला",
"CYLINDER" : "बेलन",
"CYLINDER" : "बेलन",
"CONE" : "शंकु",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "राइट परिपत्र शंकु के छिन्नक",
"PYRAMID" : "पिरैमिड",
"SQUARE PYRAMID" : "वर्ग पिरामिड",
"CUBOID" : "घनाभ",
"CUBOID" : "घनाभ",
"TRIANGULAR PRISM" : "त्रिकोणीय चश्मे",
"A: Area" : "A: क्षेत्र",
"A: Area, P: Perimeter" : "A: क्षेत्र, P: परिमाप",
"b: Arc length" : "b: आर्क लंबाई",
"V: Volume, A: Surface Area" : "V: आयतन, A: भूतल क्षेत्र",
"A: Lateral surface area" : "A: पार्श्व सतह क्षेत्र",
"A: Lateral surface area" : "A: पार्श्व सतह क्षेत्र",
"A: Base Area" : "A: बेस एरिया",
"A: Surface Area" : "A: भूतल क्षेत्र",

"1. OPERATIONS ON EXPRESSIONS" : "1. अभिव्यक्ति पर कार्रवाई",
"POLYNOMIAL" : "बहुपद",
"FRACTIONS" : "भाग",
"IDENTITY" : "सर्वसमिका",
"EXPONENTIATION" : "घातांक",
"ROOTS" : "जड़ें",
"2. RATE FORMULAS" : "2. दर सूत्र",
"6. COMPLEX NUMBERS" : "6. समिश्र संख्या",
"COMPLEX PLANE" : "परिसर विमान",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. प्रगति \nअंकगणित प्रगति",
"4. PROGRESSION" : "4. प्रगति", 
"ARITHMETIC PROGRESSION" : "अंकगणित प्रगति", 
"GEOMETRIC PROGRESSION" : "ज्यामितीय प्रगति", 
"SUMMATIONS" : "जोड़ ",
"5. LOGARITHM" : "5. लघुगणक", 
"DECIMAL LOGARITHM" : "दशमलव लघुगणक",
"NATURAL LOGARITHM" : "प्राकृतिक लघुगणक",
"d: common difference" : "d: आम फर्क",
"q: common ratio" : "q: आम अनुपात",
"3. INEQUALITIES" : "3. असमता",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "एक सही त्रिकोण के लिए त्रिकोणमितीय कार्य",
"TRIGONOMETRIC TABLE" : "त्रिकोणमितीय तालिका",
"CO-RATIOS" : "सह अनुपात",
"BASIC FORMULAS" : "मूलभूत फार्मूले",
"MULTIPLE ANGLE FORMULAS" : "एकाधिक कोण सूत्र",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "त्रिकोणमितीय कार्यों की शक्तियां",
"ADDITION FORMULAS" : "अतिरिक्त सूत्र",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "त्रिकोणमितीय कार्यों का योग",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "त्रिकोणमितीय कार्यों के उत्पाद",
"HALF ANGLE FORMULAS" : "आधा कोण सूत्र",
"ANGLES OF A PLANE TRIANGLE" : "एक विमान त्रिकोण के कोण",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "त्रिकोणमितीय कार्यों के बीच रिश्ते",
"α, β, γ are 3 angles of a triangle" : "α, β, γ एक त्रिकोण के 3 कोण हैं",


"1. ALGEBRAIC EQUATIONS" : "1. बीजीय समीकरण",
"LINEAR EQUATION" : "रेखीय समीकरण",
"SYSTEM OF TWO LINEAR EQUATIONS" : "दो रेखीय समीकरण के सिस्टम",
"QUADRATIC EQUATION" : "वर्ग समीकरण",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. प्रतिपादक और लघुगणक \n घातीय समीकरण",
"2. EXPONENT AND LOGARITHM" : "2. प्रतिपादक और लघुगणक",
"LOGARITHMIC EQUATION" : "लघुगणक समीकरण",
"3. TRIGONOMETRIC EQUATION" : "3. त्रिकोणमितीय समीकरण",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. असमानता \n रैखिक असमानता",
"4. INEQUATIONS" : "4. असमता",
"QUADRATIC INEQUATION" : "द्विघात असमानता",
"EXPONENTIAL INEQUATION" : "घातीय असमानता",
"LOGARITHMIC INEQUATION" : "लघुगणक असमानता",
"TRIGONOMETRIC INEQUATIONS" : "त्रिकोणमितीय असमानतायें",
"LINEAR EQUATION" : "रेखीय समीकरण",
"EXPONENTIAL EQUATION" : "चरघातांकी फलन",
"LINEAR INEQUATION" : "रैखिक असमानता",
"CUBIC EQUATION" : "घन समीकरण",
"POINTS" : "बिन्दु",
"- Distance between two points A and B" : "- दो अंक के बीच दूरी A और B",
"- Distance between point A and origin:" : "- बिंदु A और मूल के बीच दूरी:",
"TRIANGLE" : "त्रिभुज",
"- Area of triangle with vertices at A, B, C" : "- A, B, C पर कोने के साथ त्रिकोण का क्षेत्रफल",
"- Area of a triangle with a vertice at origin" : "- मूल में एक कोने साथ एक त्रिकोण का क्षेत्रफल",
"EQUATION OF LINE" : "रेखा के समीकरण",
"- Joining two points A, B" : "- दो बिंदुओं का मिलान A और B",
"- Passing point A and parallel with line y=ax+b" : "- बिंदु A से गुज़रती और लाइन y=ax+b के सामानांतर ",
"- Passing point A and perpendicular with line y=ax+b" : "- बिंदु A से गुज़रती और लाइन y=ax+b के अधोलंब",
"EQUATION OF CIRCLE" : "वृत्त के समीकरण",
"- Circle with radius r and center at (a, b)" : "- त्रिज्या r और (a,b) पर केंद्र के साथ वृत्त",
"- Circle with center at origin" : "- मूल में केंद्र के साथ वृत्त",
"ELLIPSE" : "दीर्घवृत्त",
"HYPERBOLA" : "अति परवलय",
"PARABOLA" : "परवलय",

"Limit \n" : "सीमा \n",
"Derivative \n" : "व्युत्पन्न \n",
"C: constant" : "C: स्थिर",
"Differentiation \n" : "अवकल \n",
"C: constant; u, v, w: functions of x" : "C: निरंतर; u, v, w: x के कार्य",
"LIMIT" : "सीमा",
"DERIVATIVE" : "व्युत्पन्न",
"DIFFERENTIATION" : "अवकल ",

"1. INDEFINITE INTEGRALS" : "1 .अनिश्चित समाकल",
"C: arbitrary constant, k: constant" : "C: मनमाने ढंग से निरंतर, k: स्थिर",
"INTEGRALS BY PARTIAL FRACTIONS" : "आंशिक भागों से समाकल",
"INTEGRALS INVOLVING ROOTS" : "जड़ों से जुड़े समाकल",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "त्रिकोणमितीय कार्यों को शामिल करते समाकल",
"2. DEFINITE INTEGRALS" : "2. निश्चित समाकल",
"APPLICATIONS" : "अनुप्रयोग",
"- Surface area created by y=f(x)" : "- y=f(x) के द्वारा बनाया गया भूतल क्षेत्र",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- y=f(x) को धुरी के चारों तरफ घुमाने से बने ठोस का आयतन ",

"Median" : "मंझला",
"Angle bisector" : "कोण द्विभाजक",
"SPHERICAL CAP" : "गोलाकार कैप",
"SPHERICAL SEGMENT" : "गोलाकार खंड",
"SPHERICAL SECTOR" : "गोलाकार क्षेत्र",
"TORUS" : "टॉरस ",
"FORMULAS WITH t=tan(x/2)" : "t=tan(x/2) के साथ सूत्र",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "पक्ष और एक विमान त्रिकोण के कोण",
"Law of sines, cosines and tangents" : "Sines, cosines और tangents के कानून",

"1. PLANE ANALYTIC GEOMETRY" : "1. विमान विश्लेषणात्मक ज्यामिति",
"2. SOLID ANALYTIC GEOMETRY" : "2. ठोस विश्लेषणात्मक ज्यामिति",
"LINE" : "सरल रेखा",
"- Direction Cosines of Line Joining Points A and B" : "- A और B अंक जोड़ने वाली रेखा के दिशा cosines ",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "A और B अंक जोड़ने वाली रेखा का समीकरण",
"- In standard form" : "- मानक रूप में",
"- In parametric form" : "- पैरामीट्रिक रूप में",
"PLANE" : "समतल",
"- General equation of a plane" : "- एक समतल के जनरल समीकरण",
"- Equation of plane passing through point A, B, C" : "- A, B, C से गुजरने वाले समतल के समीकरण ",
"- Equation of plane in intercept form" : "- अवरोधन रूप में समतल के समीकरण",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a, b, c x,y,z अक्ष के क्रमशः अवरोधन हैं। ",
"- Normal Form for Equation of Plane" : "- समतल के समीकरण के लिए सामान्य रूप",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p: O से समतल p कि सीढ़ी दूरी; α, β, γ: OP और सकारात्मक x, y, z अक्ष के बीच कोण",
"- Distance from point M to a plane" : "- बिंदु M से समतल की दूरी ",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "आयताकार निर्देशांक में M और त्रिज्या R पर क्षेत्र के केंद्र के समीकरण",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "केंद्र M और अर्द्ध अक्ष a, b, c के साथ दीर्घवृत्ताभ के समीकरण",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "z अक्ष के रूप में अक्ष के साथ अण्डाकार सिलेंडर",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "z अक्ष के रूप में अक्ष के साथ अण्डाकार शंकु",
"HYPERBOLOID OF ONE SHEET" : "एक चादर की अति परवलय",
"HYPERBOLOID OF TWO SHEETS" : "दो चादरें के अति परवलय",
"ELLIPTIC PARABOLOID" : "अण्डाकार ठोस अनुवृत्त",
"HYPERBOLIC PARABOLOID" : "अतिशयोक्तिपूर्ण ठोस अनुवृत्त",

"TRANSFORMATIONS" : "परिवर्तन",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. विशेष अनिश्चितकालीन समाकलन",
"Integrals involving ax+b" : "ax+b को शामिल करते समाकल",
"Integrals involving ax+b and px+q" : "ax+b और px+q को शामिल करते समाकल",
"Integrals involving x²+a²" : "x²+a² को शामिल करते समाकल",
"Integrals involving x²-a², x²&gt;a²" : "x²-a², x²&gt;a² को शामिल करते समाकल",
"Integrals involving x²-a², x²&lt;a²" : "x²-a², x²&lt;a² को शामिल करते समाकल",
"Integrals involving ax²+bx+c" : "ax²+bx+c को शामिल करते समाकल",
"Integrals involving xⁿ+aⁿ" : "xⁿ+aⁿ को शामिल करते समाकल",
"Integrals involving sin(ax)" : "sin(ax) को शामिल करते समाकल",
"Integrals involving e^(ax)" : "e^(ax) से जुड़े समाकल",
"Integrals involving ln(x)" : "ln(x) को शामिल समाकल",

"TRANSPOSE OF A MATRIX" : "एक मैट्रिक्स का स्थानांतरण ",
"ADDITION AND SUBTRACTION OF MATRICES" : "मैट्रिक्स का जुड़ाव और घटाव",
"MULTIPLICATION OF MATRICES" : "मैट्रिक्स के गुणन",
"DETERMINANT OF MATRIX" : "मैट्रिक्स के आव्यूह",
"INVERSE OF MATRIX" : "मैट्रिक्स का प्रतिलोम",
"EQUATION IN MATRIX FORM" : "मैट्रिक्स रूप में समीकरण",
"PROPERTIES OF MATRIX CALCULATIONS" : "मैट्रिक्स गणना के गुण",
"LENGTH" : "लम्बाई",
"AREA" : "क्षेत्रफल",
"VOLUME" : "आयतन",
"MASS" : "द्रव्यमान",
"SPEED" : "गति",
"TIME" : "समय",
"TEMPERATURE" : "तापमान",
"DENSITY" : "घनत्व",
"FORCE" : "बल ",
"ENERGY" : "ऊर्जा",
"POWER" : "शक्ति ",
"PRESSURE" : "दाब",
"Do you know this?" : "आपको यह पता है?",
"What is special about these calculations?" : "इन गणनाओं के बारे में खास क्या है?",
"Can you find the interesting trait of these calculations?" : "क्या आप इन गणनाओं के दिलचस्प लक्षण सकते है?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. अन्य फंगक्सन का ग्राफ़ ",
"Constant" : "स्थिर",
"Absolute" : "निरपेक्ष मान",
"Square Root" : "वर्गमूल ",
"Parabolic" : "परवलयिक ",
"Cubic" : "घन ",
"Reciprocal" : "पारस्परिक ",
"Sec" : "सेक ",
"Cosec" : "कोसेक",
"6. FUNCTION TRANSFORMATIONS" : "6. फंगक्सन रूपांतर",
"Horizontal Shifting" : "क्षैतिज स्थानांतरण ",
"Vertical Shifting" : "ऊर्ध्वाधर स्थानांतरण ",
"Reflection" : "प्रतिबिंब ",
"Stretching" : "फैलता हुआ",
"F, F1: Focus points (foci)" : "F, F1: फोकस अंक (foci) ",
"AF=p: Parameter of Parabola" : "AF=p: परवलय का परिमाण",

"Probability & Statistics" : "सांख्यिकी",
"1. SETS" : "1. समुच्चय",
"SET" : "समुच्चय",
"SUBSET" : "उपसमुच्चय",
"INTERSECTION" : "समुच्चय सर्वनिष्ठ",
"UNION" : "समुच्चय संघ",
"SYMMETRIC DIFFERENCE" : "सममित अंतर",
"RELATIVE COMPLEMENT OF A IN B" : "आपेक्षिक पूरक B-A",
"ABSOLUTE COMPLEMENT" : "पूर्ण पूरक",
"OPERATIONS ON SETS" : "सेट आपरेशन",
"2. COMBINATIONS AND PERMUTATIONS" : "2. संचय और क्रमचय",
"COMBINATIONS" : "संचय",
"PERMUTATIONS" : "क्रमचय",
"3. PROBABILITY" : "3. प्रायिकता",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(१) A और B परस्पर अनन्य हैं, (२) A और B स्वतंत्र हैं",
"4. STATISTICS" : "4. सांख्यिकी",
"MEAN" : "माध्य",
"MEDIAN" : "मंझला",
"MODE" : "मोड",
"Numerical value that occurs the most number of times" : "सबसे आम मूल्य",
"EXAMPLE" : "उदाहरण ",
"GEOMETRIC MEAN" : "ज्यामितीय माध्य",
"HARMONIC MEAN" : "हरात्मक माध्य ",
"VARIANCE" : "विचरण",
"STANDARD DEVIATION" : "मानक विचलन",
"MEAN DEVIATION" : "माध्य विचलन ",
"ROOT MEAN SQUARE" : "द्विघाती माध्य या 'वर्ग-माध्य-मूल' (RMS)",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "सामान्य वितरण (गाऊसी वितरण)",
"f: density function, F: Distribution function, μ: mean" : "f: घनत्व फंगक्सन, F: वितरण फंगक्सन μ: माध्य",
"EXPONENTIAL DISTRIBUTION" : "घातीय वितरण ",
"POISSON DISTRIBUTION" : "पायसन वितरण",
"UNIFORM DISTRIBUTION" : "समान वितरण",
"Transforms" : "रूपान्तर",
"1. FOURIER SERIES AND TRANSFORMS" : "2. फूर्ये श्रेणी और फूर्ये रूपान्तर",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): आवधिक फंगक्सन, अवधि 2L; a_n, b_n: फूरियर गुणांक; c_n: काँमपलेक्स फूरियर गुणांक ",
"REAL FORM OF FOURIER SERIES" : "फूर्ये श्रेणी का असली रूप ",
"COMPLEX FORM" : "काँमपलेक्स फार्म",
"PARSEVAL’S THEOREM" : "पार्सेवल का प्रमेय ",
"FOURIER TRANSFORM" : "फूर्ये रूपान्तर",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): x का फंगक्सन, F(s): f(x) का फूर्ये रूपान्तर",
"CONVOLUTION" : "सवलन ",
"CORRELATION" : "सह - संबंध ",
"f*: complex conjugate of f" : "f*: f का काँमपलेक्स कन्जूगेट",
"FOURIER SYMMETRY RELATIONSHIPS" : "फूरियर समरूपता रिस्ते",
"FOURIER TRANSFORM PAIRS" : "फूर्ये रूपान्तर जोड़े",
"2. LAPLACE TRANSFORMS" : "2. लाप्लास रूपांतरण",
"DEFINITION" : "परिभाषा",
"CONVOLUTION" : "सवलन ",
"INVERSE" : "उलटा ",
"a: constant" : "a: स्थायी",
"DERIVATIVE" : "व्युत्पन्न",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "आवृत्ति स्थानांतरण",
"TRANSLATION (TIME SHIFTING)" : "समय स्थानांतरण",
"LAPLACE TRANSFORM PAIRS" : "लाप्लास रूपांतरण जोड़े",

"Math Tricks" : "मनगणित, गणित के गुर",
"1. ADDITION" : "1. जोड़",
"2. SUBTRACTION" : "2. घटाना",
"3. MULTIPLICATION" : "3. गुणा",
"4. DIVISION" : "4. भाग",
"5. SQUARING" : "5. वर्ग निकालना",
"6. EXPONENTIATION" : "6. घातांक",
"7. ROOTS" : "7. जड़ें",
"8. SUMMATIONS" : "8. जोड़ ",
"6. SUMMATIONS" : "6. जोड़ ",

"UNITS CONVERTER" : "इकाई रूपान्तरक",
"ANGLE" : "कोण",
"COMPUTER DATA" : "कम्प्यूटर डाटा ",
"FUEL CONSUMPTION" : "ईंधन की खपत ",
"Click here to download Physics Formulas app" : "भौतिकी सूत्र एप्लिकेशन डाउनलोड करने के लिए यहां क्लिक करें ",
"All Maths formulas for your work and study" : "अपने काम और अध्ययन के लिए सभी गणित सूत्र ",
"EDIT" : "संपादित करें ",
"ADD" : "जोड़ें ",
"TITLE OF THE PICTURE" : "तस्वीर का शीर्षक ",
"GAME" : "खेल ",
"START" : "प्रारंभ ",
"LEVEL" : "स्तर ",
"RETRY" : "पुन: प्रयास करें ",
"SCORE" : "स्कोर ",
"TIME" : "समय",
"BEST" : "सर्वश्रेष्ठ ",
"GAME OVER" : "खेल खत्म ",
"YOUR SCORE IS: " : "आपका स्कोर है ",


"Metre (m)" : "मीटर  (m)",
"Kilometre (km)" : "किलोमीटर  (km)",
"Centimetre (cm)" : "सेंटीमीटर (cm)",
"Millimetre (mm)" : "मिलीमीटर (mm)",
"Micrometre | Mircon (μm)" : "माइक्रोमीटर|  माइक्रोन (μm)",
"Nanometre (nm)" : "नैनोमीटर (nm)",
"Inch (in)" : "इंच  (in)",
"Foot (ft)" : "फूट (ft)",
"Yard (yd)" : "यार्ड  (yd)",
"Mile (mi)" : "माइल  (mi)",
"Light Year" : "प्रकाश वर्ष ",
"Nautical Mile (NM)" : "समुद्री मील  (NM)",
"Angstrom (Å)" : "एनग्सटोर्म (Å)",

"Square Mile (mi²)" : "वर्ग मील  (mi²)",
"Square Yard (yd²)" : "वर्ग यार्ड  (yd²)",
"Square Foot (ft²)" : "वर्ग फुट  (ft²)",
"Square Inch (in²)" : "वर्ग इंच  (in²)",
"Square Kilometre (km²)" : "वर्ग किलोमीटर  (km²)",
"Hectare (ha)" : "हेक्टेयर  (ha)",
"Acre" : "एकर ",
"Square Metre (m²)" : "वर्ग मीटर  (m²)",
"Square Centimetre (cm²)" : "वर्ग सेंटीमीटर (cm²)",
"Square Millimetre (mm²)" : "वर्ग मिलीमीटर (mm²)",

"Litre (l)" : "लीटर  (l)",
"Cubic Metre (m³)" : "घन मीटर  (m³)",
"Cubic Inch (in³)" : "घन इंच  (in³)",
"Cubic Foot (ft³)" : "घन फुट  (ft³)",
"Cubic Yard (yd³)" : "घन यार्ड  (yd³)",
"Gallon (US) (gal)" : "गैलन (यू एस)  (gal)",
"Gallon (UK) (gal)" : "गैलन (यू के)  (gal)",

"Gram (g)" : "ग्राम  (g)",
"Kilogram (kg)" : "किलोग्राम  (kg)",
"Tonne (t)" : "टन  (t)",
"Milligram (mg)" : "मिलीग्राम (mg)",
"Microgram (μg)" : "माइक्रोग्राम  (μg)",
"Ounce (oz)" : "औंस  (oz)",
"Pound (lb)" : "पाउंड  (lb)",
"Carat" : "कैरेट ",
"Slug" : "स्लग ",
"Ounce (Troy) (ozt)" : "औंस (ट्रॉय)  (ozt)",

"Year" : "वर्ष ",
"Month" : "महीना ",
"Week" : "सप्ताह ",
"Day" : "दिन",
"Hour (h)" : "घंटा (h)",
"Minute (min)" : "मिनट  (min)",
"Second (s)" : "सेकंड  (s)",
"Millisecond (ms)" : "मिलीसेकंड (ms)",
"Microsecond (μs)" : "माइक्रोसैकेण्ड (μs)",
"Decade" : "डिकेड ",
"Century" : "सेंचुरी ",
"Millennium" : "मिलेनियम ",

"Kilometre per Hour (km/h)" : "किलोमीटर प्रति घंटा   (km/h)",
"Miles per Hour (mi/h)" : "मील प्रति घंटा  (mi/h)",
"Metre per Second (m/s)" : "मीटर प्रति सेकंड (m/s)",
"Foot per Second (ft/s)" : "फुट प्रति सेकंड (ft/s)",
"Knot" : "ना॔ट /गाँठ",
"Speed of Light (c)" : "प्रकाश की गति  (c)",
"Minutes per Mile (min/mi)" : "मिनट प्रति माइल  (min/mi)",
"Minutes per Kilometre (min/km)" : "मिनट प्रति किलोमीटर (min/km)",

"Celsius (°C)" : "सेल्सियस  (°C)",
"Fahrenheit (°F)" : "फेरनहाइट  (°F)",
"Kelvin (K)" : "केल्विन  (K)",
"Rankine (°R)" : "श्रेणी (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "किलोग्राम /  घन मीटर  (kg/m³)",
"Kilogram/Litre (kg/l)" : "किलोग्राम / लीटर  (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "स्लग / घन फुट  (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "पाउंड / घन फुट  (lbm/ft³)",

"Newton (N)" : "न्यूटन  (N)",
"Kilonewton (kN)" : "किलोन्यूटन  (kN)",
"Atomic unit of force" : "बल की परमाणु इकाई",
"Gram Force (gf)" : "ग्राम बल  (gf)",
"Pound Force (lbf)" : "पाउंड বল (lbf)",
"Poundal (pdl)" : "पाउन्डल (pdl)",
"Dyne (dyn)" : "डाएन  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "किलोग्राम- बल |किलोपाउंड (kgf)",

"Watt (W)" : "वाट  (W)",
"Kilowatt (kW)" : "किलोवाट  (kW)",
"Megawatt (MW)" : "मेगावाट  (MW)",
"Milliwatt (mW)" : "मिलीवाट (mW)",
"Microwatt (µW)" : "माइक्रोवाट (µW)",
"Horsepower (hp)" : "हार्सपावर  (hp)",
"Calorie per Second (cal/s)" : "कैलोरी प्रति सेकंड (cal/s)",

"Pascal (Pa)" : "पास्कल  (Pa)",
"Kilopascal (kPa)" : "किलोपास्कल (kPa)",
"Megapascal (MPa)" : "मेगापास्कल (MPa)",
"Bar (bar)" : "बार  (bar)",
"Millibar (mbar)" : "मिलीबार (mbar)",
"Atmosphere (atm)" : "वायुमंडल (एटीएम) ",
"Pound Force per Square Inch (psi)" : "पाउंड बल प्रति  वर्ग इंच  (psi)",
"Dyne per Square Centimetre" : "वर्ग सेंटीमीटर प्रति डाएन ",

"Degree (°)" : "डिग्री  (°)",
"Radian (rad)" : "रेडिएन (rad)",
"π Radian (π rad)" : "π रेडिएन (π rad)",
"Second" : "सेकंड ",
"Minute (')" : "मिनट  (')",

"Byte (B)" : "बाइट  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "किलोबाइट | किबिबाइट (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "मेगाबाइट | मेबिबाइट (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "गीगाबाइट | जिबिबाइट (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "टेराबाइट | टेबिबाइट  (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "पेटाबाइट | पेबिबाइट  (PB | Pi)",
"Bit (b)" : "बिट  (b)",
"Zebibyte (Zi)" : "जेबिबाइट (Zi)",

"Litres per 100 Kilometres (l/100km)" : "१०० किलोमीटर प्रति लीटर  (l/100km)",
"Kilometres per Litre (km/l)" : "लीटर प्रति किलोमीटर  (km/l)",
"Miles per Gallon (US) (mpg)" : "गैलन प्रति मील (यू एस)  (mpg)",
"Miles per Gallon (UK) (mpg)" : "गैलन प्रति मील (यू के) (mpg)",

"Title of new tool" : "नए उपकरण का शीर्षक",
"Name of the variable" : "चर का नाम",
"Add more variable" : "चर जोड़ें",
"Formula of the tool" : "उपकरण का फॉर्मूला",
"Name of the result " : "परिणाम का नाम",
"Add more result" : "परिणाम जोड़ें",

"EULER'S FORMULA" : "ऑयलर का सूत्र",

};