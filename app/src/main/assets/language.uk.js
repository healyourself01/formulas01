﻿var language={

	"1. 2D SHAPES" : "1. Пласкі фігури ",
"2. 3D SHAPES" : "2. Об'ємні фігури ",
"TRIANGLE" : "Трикутник ",
"RIGHT TRIANGLE" : "Прямокутний трикутник ",
"SQUARE" : "Квадрат",
"RECTANGLE" : "Прямокутник ",
"PARALLELOGRAM" : "Паралелограм ",
"LOZENGE" : "Ромб ",
"TRAPEZOID" : "Трапеція ",
"CONVEX QUADRILATERAL" : "Опуклий  чотирикутник ",
"CIRCLE" : "Круг",
"SEGMENT OF CIRCLE" : "Сегмент Круга ",
"SECTOR OF CIRCLE" : "Сектор круга ",
"REGULAR POLYGON OF N SIDES" : "Правильний багатокутник з N сторонами",
"REGULAR POLYGON" : "Правильний багатокутник ",
"REGULAR POLYGON" : "Правильний багатокутник ",
"HEXAGON" : "Шестикутник",
"SPHERE" : "Сфера ",
"CYLINDER" : "Циліндр ",
"CYLINDER" : "Циліндр ",
"CONE" : "Конус ",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "Усічений прямий круговий конус",
"PYRAMID" : "Піраміда ",
"SQUARE PYRAMID" : "Квадратна піраміда",
"CUBOID" : "Прямокутний паралелепіпед",
"CUBOID" : "Прямокутний паралелепіпед",
"TRIANGULAR PRISM" : "Трикутна призма ",
"A: Area" : "А: Площа ",
"A: Area, P: Perimeter" : "А: Площа, P: Периметр ",
"b: Arc length" : "b: Довжина дуги ",
"V: Volume, A: Surface Area" : "V: Об'єм, А: Площа поверхні ",
"A: Lateral surface area" : "А: Площа бічної поверхні ",
"A: Lateral surface area" : "А: Площа бічної поверхні ",
"A: Base Area" : "А: Площа основи ",
"A: Surface Area" : "А: Площа поверхні ",

"1. OPERATIONS ON EXPRESSIONS" : "1. Операції з виразами ",
"POLYNOMIAL" : "Багаточлен",
"FRACTIONS" : "Дроби",
"IDENTITY" : "Тотожність",
"EXPONENTIATION" : "Піднесення до ступіня ",
"ROOTS" : "Корінь ",
"2. RATE FORMULAS" : "2. Формули відношення",
"6. COMPLEX NUMBERS" : "6. Комплексні числа ",
"COMPLEX PLANE" : "Комплексна площина",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4.  Прогресія \nАрифметична прогресія",
"4. PROGRESSION" : "4.  Прогресія", 
"ARITHMETIC PROGRESSION" : "Арифметична прогресія ", 
"GEOMETRIC PROGRESSION" : "Геометрична прогресія", 
"SUMMATIONS" : "Сумування",
"5. LOGARITHM" : "5. Логарифм ", 
"DECIMAL LOGARITHM" : "Десятковий  логарифм ",
"NATURAL LOGARITHM" : "Натуральний логарифм (ln)",
"d: common difference" : "d: загальна різниця ",
"q: common ratio" : "q: загальний коефіцієнт ",
"3. INEQUALITIES" : "3. Нерівності ",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "Тригонометричні функції для прямокутного трикутника ",
"TRIGONOMETRIC TABLE" : "Тригонометричні таблиці ",
"CO-RATIOS" : "Співвідношення кутів",
"BASIC FORMULAS" : "Основні формули ",
"MULTIPLE ANGLE FORMULAS" : "Формули для декількох кутів",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "Формули пониження ступеня",
"ADDITION FORMULAS" : "Формули складання (тригонометричних функцій)",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "Сума тригонометричних функцій ",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "Добуток тригонометричних функцій ",
"HALF ANGLE FORMULAS" : "Формули половинного кута",
"ANGLES OF A PLANE TRIANGLE" : "Кути плаского трикутника ",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "Співвідношення  між тригонометричними функціями ",
"α, β, γ are 3 angles of a triangle" : "α, β, γ  -  це 3 кути трикутника",


"1. ALGEBRAIC EQUATIONS" : "1. Алгебраїчні рівняння ",
"LINEAR EQUATION" : "Лінійне рівняння ",
"SYSTEM OF TWO LINEAR EQUATIONS" : "Система двох лінійних рівнянь ",
"QUADRATIC EQUATION" : "Квадратне рівняння ",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. Експонента і логарифм \nЕкспоненційні рівняння",
"2. EXPONENT AND LOGARITHM" : "2. Експонента і логарифм ",
"LOGARITHMIC EQUATION" : "Логарифмічне  рівняння ",
"3. TRIGONOMETRIC EQUATION" : "3. Тригонометричне рівняння",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. Нерівності \nЛінійна  нерівність ",
"4. INEQUATIONS" : "4. Нерівності ",
"QUADRATIC INEQUATION" : "Квадратна  нерівність ",
"EXPONENTIAL INEQUATION" : "Експоненційна нерівність ",
"LOGARITHMIC INEQUATION" : "Логарифмічна нерівність ",
"TRIGONOMETRIC INEQUATIONS" : "Тригонометричні нерівності ",
"LINEAR EQUATION" : "Лінійне рівняння ",
"EXPONENTIAL EQUATION" : "Експоненційне рівняння ",
"LINEAR INEQUATION" : "Лінійна нерівність ",
"CUBIC EQUATION" : "Кубічне рівняння ",
"POINTS" : "Точки",
"- Distance between two points A and B" : "- Відстань між двома точками А і В",
"- Distance between point A and origin:" : "- Відстань між точкою А і початком координат",
"TRIANGLE" : "Трикутник ",
"- Area of triangle with vertices at A, B, C" : "- Площа трикутника з вершинами в A, B, C ",
"- Area of a triangle with a vertice at origin" : "- Площа трикутника з вершиною в точці початку координат",
"EQUATION OF LINE" : "Рівняння лінії ",
"- Joining two points A, B" : "- Що з'єднує дві точки А, В",
"- Passing point A and parallel with line y=ax+b" : "- Що проходить через  точку А та паралельна лінії y=ax+b",
"- Passing point A and perpendicular with line y=ax+b" : "- Що проходить через точку А  і перпендикулярна  лінії у=ах+b",
"EQUATION OF CIRCLE" : "Рівняння кола ",
"- Circle with radius r and center at (a, b)" : "- Коло з  радіусом  r  та центром  у точці  (a, b)",
"- Circle with center at origin" : "- Коло з центром у початку координат",
"ELLIPSE" : "Еліпс",
"HYPERBOLA" : "Гіпербола ",
"PARABOLA" : "Парабола ",

"Limit \n" : "Ліміт \n ",
"Derivative \n" : "Похідна \n",
"C: constant" : "C: Постійна ",
"Differentiation \n" : "Диференціювання \n",
"C: constant; u, v, w: functions of x" : "C: постійна; u, v, w: функції х ",
"LIMIT" : "Ліміт",
"DERIVATIVE" : "Похідна ",
"DIFFERENTIATION" : "Диференціювання",

"1. INDEFINITE INTEGRALS" : "1. Невизначені інтеграли ",
"C: arbitrary constant, k: constant" : "C: довільна постійна, k: постійна ",
"INTEGRALS BY PARTIAL FRACTIONS" : "Інтеграли з елементарними дробами",
"INTEGRALS INVOLVING ROOTS" : "Інтеграли, що містять корені",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "Інтеграли, що містять тригонометричні функції ",
"2. DEFINITE INTEGRALS" : "2. Визначені  інтеграли ",
"APPLICATIONS" : "Застосування",
"- Surface area created by y=f(x)" : "- Площа поверхні, утвореної у = f(x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- Об'єм тіла, утвореного обертанням у=f(x) навколо осі: ",

"Median" : "Медіана ",
"Angle bisector" : "Бісектриса ",
"SPHERICAL CAP" : "Півсфера",
"SPHERICAL SEGMENT" : "Кульовий сегмент",
"SPHERICAL SECTOR" : "Кульовий сектор",
"TORUS" : "Тор",
"FORMULAS WITH t=tan(x/2)" : "Формули з t = tan(х/2) ",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "Сторони і кути плаского трикутника ",
"Law of sines, cosines and tangents" : "Теорема синусів, косинусів і тангенсів ",

"1. PLANE ANALYTIC GEOMETRY" : "1 Аналітична геометрія ",
"2. SOLID ANALYTIC GEOMETRY" : "2 Аналітична геометрія ",
"LINE" : "Лінія ",
"- Direction Cosines of Line Joining Points A and B" : "Напрямні косинуси лінії, що з'єднують точки А і В ",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "Рівняння лінії, що з'єднує дві точки A, B ",
"- In standard form" : "- у стандартній формі",
"- In parametric form" : "- в параметричнії формі",
"PLANE" : "Площина",
"- General equation of a plane" : "- Загальне рівняння площини",
"- Equation of plane passing through point A, B, C" : "- Рівняння площини, що проходить через точки A, B, C ",
"- Equation of plane in intercept form" : "- Рівняння площини у формах, що перетинаються",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a , b, с - осі, що перетинають осі х, y, z відповідно ",
"- Normal Form for Equation of Plane" : "- Нормальна форма для рівняння площині",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "р: перпендикуляна відстань від точки О до площини у Р; α, β, γ: кути між Ор і позитивною частиною восей x,y,z",
"- Distance from point M to a plane" : "- Відстань від точки М до площини",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Рівняння сфери з центром у точці М і радіусом R в прямокутних координатах ",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "Рівняння еліпсоїда з центром М і півосями а, Ь, с ",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Еліптичний циліндр з віссю, яка співпадає з віссю z ",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "Еліптичний конус з віссю,  яка співпадає з віссю z ",
"HYPERBOLOID OF ONE SHEET" : "Однопорожнинний гіперболоїд",
"HYPERBOLOID OF TWO SHEETS" : "Двопорожнинний гіперболоїд",
"ELLIPTIC PARABOLOID" : "Еліптичний параболоїд ",
"HYPERBOLIC PARABOLOID" : "Гіперболічний параболоїд ",

"TRANSFORMATIONS" : "Перетворення",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. Спеціальні невизначені інтеграли ",
"Integrals involving ax+b" : "Інтеграли, що містять ax + b",
"Integrals involving ax+b and px+q" : "Інтеграли, що містять aх + b і px + Q ",
"Integrals involving x²+a²" : "Інтеграли, що містять х²+а² ",
"Integrals involving x²-a², x²&gt;a²" : "Інтеграли, що містять х²-а², х²&gt;а² ",
"Integrals involving x²-a², x²&lt;a²" : "Інтеграли, що містять х²-а², х²&lt;а² ",
"Integrals involving ax²+bx+c" : "Інтеграли, що містять ax²+bх+с ",
"Integrals involving xⁿ+aⁿ" : "Інтеграли, що містять хⁿ+аⁿ ",
"Integrals involving sin(ax)" : "Інтеграли що містять sin(ax)",
"Integrals involving e^(ax)" : "Інтеграли, що містять е^(ax) ",
"Integrals involving ln(x)" : "Інтеграли, що містять ln(х) ",

"TRANSPOSE OF A MATRIX" : "Транспонування матриці ",
"ADDITION AND SUBTRACTION OF MATRICES" : "Додавання і віднімання матриць ",
"MULTIPLICATION OF MATRICES" : "Множення матриць ",
"DETERMINANT OF MATRIX" : "Визначник матриці ",
"INVERSE OF MATRIX" : "Зворотні матриці ",
"EQUATION IN MATRIX FORM" : "Рівняння у матричній формі",
"PROPERTIES OF MATRIX CALCULATIONS" : "Властивості матричних обчислень ",
"LENGTH" : "Довжина ",
"AREA" : "Площа ",
"VOLUME" : "Об'єм",
"MASS" : "Маса ",
"SPEED" : "Швидкість ",
"TIME" : "Час ",
"TEMPERATURE" : "Температура ",
"DENSITY" : "Щільність ",
"FORCE" : "Сила ",
"ENERGY" : "Енергія",
"POWER" : "Потужність ",
"PRESSURE" : "Тиск ",
"Do you know this?" : "Чи знаєте ви? ",
"What is special about these calculations?" : "Що особливого в цих розрахунках? ",
"Can you find the interesting trait of these calculations?" : "Чи можете ви знайти цікаву особливість цих розрахунків? ",
"5. GRAPH OF OTHER FUNCTIONS" : "5. ГРАФІК ІНШИХ ФУНКЦІЙ",
"Constant" : "Постійна ",
"Absolute" : "Абсолютна величина",
"Square Root" : "Квадратний корінь ",
"Parabolic" : "Параболічний",
"Cubic" : "Кубічний",
"Reciprocal" : "Зворотне число",
"Sec" : "Секанс",
"Cosec" : "Косеканс",
"6. FUNCTION TRANSFORMATIONS" : "6. ФУНКЦІОНАЛЬНІ ПЕРЕТВОРЕННЯ",
"Horizontal Shifting" : "Горизонтальне переміщення ",
"Vertical Shifting" : "Вертикальне переміщення ",
"Reflection" : "Віддзеркалення ",
"Stretching" : "Розтягування",
"F, F1: Focus points (foci)" : "F, F1: Точка фокусу (фокуси) ",
"AF=p: Parameter of Parabola" : "AF=p: Параметр параболи ",

"Probability & Statistics" : "Імовірність і статистика ",
"1. SETS" : "1. Множини",
"SET" : "Множина",
"SUBSET" : "Підмножина",
"INTERSECTION" : "Перетин",
"UNION" : "Об'єднання",
"SYMMETRIC DIFFERENCE" : "Симетрична різниця ",
"RELATIVE COMPLEMENT OF A IN B" : "Відносне доповнення А у В",
"ABSOLUTE COMPLEMENT" : "Абсолютне доповнення",
"OPERATIONS ON SETS" : "Операції над множинами ",
"2. COMBINATIONS AND PERMUTATIONS" : "2. Сполучення і перестановки ",
"COMBINATIONS" : "Сполучення",
"PERMUTATIONS" : "Перестановка",
"3. PROBABILITY" : "3. Імовірність ",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) Якщо А і В є взаємовиключними, (2) Якщо А і В незалежні ",
"4. STATISTICS" : "4. Статистика ",
"MEAN" : "Середнє значення",
"MEDIAN" : "Медіана ",
"MODE" : "Мода",
"Numerical value that occurs the most number of times" : "Чисельне значення, яке зустрічається найчастіше ",
"EXAMPLE" : "Приклад ",
"GEOMETRIC MEAN" : "Середнє геометричне ",
"HARMONIC MEAN" : "Середнє гармонічне",
"VARIANCE" : "Дисперсія випадкової величини",
"STANDARD DEVIATION" : "Стандартне відхилення ",
"MEAN DEVIATION" : "Середнє відхилення ",
"ROOT MEAN SQUARE" : "Середньоквадратичне ",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "Нормальний розподіл (розподіл Гауса) ",
"f: density function, F: Distribution function, μ: mean" : "f: функція щільності, F: Функція розподілу, μ: середне",
"EXPONENTIAL DISTRIBUTION" : "Експоненційний розподіл ",
"POISSON DISTRIBUTION" : "Розподіл Пуассона ",
"UNIFORM DISTRIBUTION" : "Рівномірний розподіл ",
"Transforms" : "Перетворення ",
"1. FOURIER SERIES AND TRANSFORMS" : "1. Ряди Фур'є і перетворення ",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): періодична функція, період 2L; a_n, b_n: коефіцієнти Фур'є; c_n: комплексний коефіцієнт Фур'є ",
"REAL FORM OF FOURIER SERIES" : "Справжня форма ряду Фур'є ",
"COMPLEX FORM" : "Комплексна форма ",
"PARSEVAL’S THEOREM" : "Теорема Парсеваля ",
"FOURIER TRANSFORM" : "Перетворення Фур'є ",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): функція х, F(s): перетворення Фур'є f(х) ",
"CONVOLUTION" : "Згортання ",
"CORRELATION" : "Кореляція ",
"f*: complex conjugate of f" : "f*: комплексно спряжене f",
"FOURIER SYMMETRY RELATIONSHIPS" : "Співвідношення симетрії Фур'є",
"FOURIER TRANSFORM PAIRS" : "Перетворення Фур'є деяких функцій",
"2. LAPLACE TRANSFORMS" : "2. Перетворення Лапласа ",
"DEFINITION" : "Визначення ",
"CONVOLUTION" : "Згортання ",
"INVERSE" : "Зворотне перетворення Лапласа",
"a: constant" : "а: постійна ",
"DERIVATIVE" : "Похідна ",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "Заміна (Зсув частоти) ",
"TRANSLATION (TIME SHIFTING)" : "Трансляція (зсув у часі) ",
"LAPLACE TRANSFORM PAIRS" : "Перетворення Лапласа деяких функцій",

"Math Tricks" : "Математичні прийоми",
"1. ADDITION" : "1. Додавання ",
"2. SUBTRACTION" : "2. Віднімання ",
"3. MULTIPLICATION" : "3. Множення ",
"4. DIVISION" : "4. Ділення",
"5. SQUARING" : "5. Піднесення до квадрату",
"6. EXPONENTIATION" : "6. Піднесення до ступіня ",
"7. ROOTS" : "7. Корінь ",
"8. SUMMATIONS" : "8. Сумування",
"6. SUMMATIONS" : "6. Сумування",

"UNITS CONVERTER" : "Конвертер одиниць",
"ANGLE" : "Кут ",
"COMPUTER DATA" : "Комп'ютерні дані ",
"FUEL CONSUMPTION" : "Витрата палива ",
"Click here to download Physics Formulas app" : "Натисніть тут, щоб завантажити додаток Physics Formulas ",
"All Maths formulas for your work and study" : "Всі математичні формули для вашої роботи і навчання",
"EDIT" : "Редагувати",
"ADD" : "Додати",
"TITLE OF THE PICTURE" : "Підпис до зображення",
"GAME" : "Ігра",
"START" : "Початок, запуск",
"LEVEL" : "Рівень ",
"RETRY" : "Інтервал",
"SCORE" : "Рахунок",
"TIME" : "Час ",
"BEST" : "Найкращий  показник",
"GAME OVER" : "Гру завершено",
"YOUR SCORE IS: " : "Ваша оцінка",


"Metre (m)" : "Метр  (m)",
"Kilometre (km)" : "Кілометр  (km)",
"Centimetre (cm)" : "Сантиметр  (cm)",
"Millimetre (mm)" : "Міліметр  (mm)",
"Micrometre | Mircon (μm)" : "Мікрометр | Мікрон (μm)",
"Nanometre (nm)" : "Нанометр (nm)",
"Inch (in)" : "Дюйм (in)",
"Foot (ft)" : "Фут (ft)",
"Yard (yd)" : "Ярд (yd)",
"Mile (mi)" : "Миля  (mi)",
"Light Year" : "Світловий рік",
"Nautical Mile (NM)" : "Морська миля  (NM)",
"Angstrom (Å)" : "Ангстрем  (Å)",

"Square Mile (mi²)" : "Квадратна миля  (mi²)",
"Square Yard (yd²)" : "Квадратгий ярд (yd²)",
"Square Foot (ft²)" : "Квадратний фут  (ft²)",
"Square Inch (in²)" : "Квадратний дюйм  (in²)",
"Square Kilometre (km²)" : "Квадратний кілометр  (km²)",
"Hectare (ha)" : "Гектар  (ha)",
"Acre" : "Акр ",
"Square Metre (m²)" : "Квадратний метр  (m²)",
"Square Centimetre (cm²)" : "Квадратний сантиметр (cm²)",
"Square Millimetre (mm²)" : "Квадратний міліметр (mm²)",

"Litre (l)" : "Літр (l)",
"Cubic Metre (m³)" : "Кубічний метр  (m³)",
"Cubic Inch (in³)" : "Кубічний дюйм (in³)",
"Cubic Foot (ft³)" : "Кубічний фут  (ft³)",
"Cubic Yard (yd³)" : "Кубічний ярд (yd³)",
"Gallon (US) (gal)" : "Галон (США)  (gal)",
"Gallon (UK) (gal)" : "Галон (Великобританія)  (gal)",

"Gram (g)" : "Грам  (g)",
"Kilogram (kg)" : "Кілограм  (kg)",
"Tonne (t)" : "Тонна  (t)",
"Milligram (mg)" : "Міліграм (mg)",
"Microgram (μg)" : "Мікрограм (μg)",
"Ounce (oz)" : "Унція  (oz)",
"Pound (lb)" : "Фунт  (lb)",
"Carat" : "Карат",
"Slug" : "Слаг",
"Ounce (Troy) (ozt)" : "Унція (тройська вага)  (ozt)",

"Year" : "Рік ",
"Month" : "Місяць ",
"Week" : "Тиждень ",
"Day" : "День ",
"Hour (h)" : "Година  (h)",
"Minute (min)" : "Хвилина  (min)",
"Second (s)" : "Секунди (s)",
"Millisecond (ms)" : "Мілісекунда  (ms)",
"Microsecond (μs)" : "Мікросекунда (μs)",
"Decade" : "Десятиліття ",
"Century" : "Століття ",
"Millennium" : "Тисячоліття",

"Kilometre per Hour (km/h)" : "Кілометрів на годину (km/h)",
"Miles per Hour (mi/h)" : "Миль на годину  (mi/h)",
"Metre per Second (m/s)" : "Метр на секунду  (m/s)",
"Foot per Second (ft/s)" : "Фут на секунду  (ft/s)",
"Knot" : "Вузол ",
"Speed of Light (c)" : "Швидкість світла (c)",
"Minutes per Mile (min/mi)" : "Хвилин на милю (min/mi)",
"Minutes per Kilometre (min/km)" : "Хвилин на кілометр  (min/km)",

"Celsius (°C)" : "Цельсій  (°C)",
"Fahrenheit (°F)" : "Фаренгейт (°F)",
"Kelvin (K)" : "Кельвін  (K)",
"Rankine (°R)" : "Градус Ранкіна (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "Кілограм / Кубічний метр  (kg/m³)",
"Kilogram/Litre (kg/l)" : "Кілограм / Літр (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "Слаг / Кубічний фут  (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "Фунт / Кубічний фут  (lbm/ft³)",

"Newton (N)" : "Ньютон  (N)",
"Kilonewton (kN)" : "Кілоньютон  (kN)",
"Atomic unit of force" : "Атомна одиниця сили",
"Gram Force (gf)" : "Грам - сила (gf)",
"Pound Force (lbf)" : "Фунт - сила  (lbf)",
"Poundal (pdl)" : "Паундаль  (pdl)",
"Dyne (dyn)" : "Діна  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "Кілограм-сила | Кілопонд (kgf)",

"Watt (W)" : "Ват (W)",
"Kilowatt (kW)" : "Кіловат  (kW)",
"Megawatt (MW)" : "Мегават (MW)",
"Milliwatt (mW)" : "Мілліват (mW)",
"Microwatt (µW)" : "Мікроват (µW)",
"Horsepower (hp)" : "Кінська сила (hp)",
"Calorie per Second (cal/s)" : "Калорій на секунду  (cal/s)",

"Pascal (Pa)" : "Паскаль  (Pa)",
"Kilopascal (kPa)" : "Кілопаскаль (kPa)",
"Megapascal (MPa)" : "Мегапаскаль (MPa)",
"Bar (bar)" : "Бар  (bar)",
"Millibar (mbar)" : "Міллібар (mbar)",
"Atmosphere (atm)" : "Атмосфера (атм) ",
"Pound Force per Square Inch (psi)" : "Фунт - сила на квадратний дюйм  (psi)",
"Dyne per Square Centimetre" : "Діна за квадратний сантиметр ",

"Degree (°)" : "Градус (°)",
"Radian (rad)" : "Радіан  (rad)",
"π Radian (π rad)" : "π Радіан  (π rad)",
"Second" : "Секунди",
"Minute (')" : "Хвилина  (')",

"Byte (B)" : "Байт  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "Кілобайт | Кібібайт (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "Мегабайт | Мебібайт  (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Гігабайт | Гібібайт  (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "Терабайт | Тебібайт (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "Петабайт | Пебібайт (PB | Pi)",
"Bit (b)" : "Біт  (b)",
"Zebibyte (Zi)" : "Зебібайт (Zi)",

"Litres per 100 Kilometres (l/100km)" : "Літрів на 100 кілометрів (l/100km)",
"Kilometres per Litre (km/l)" : "Кілометрів на літр  (km/l)",
"Miles per Gallon (US) (mpg)" : "Миль на галон (США)  (mpg)",
"Miles per Gallon (UK) (mpg)" : "Миль на галон (Великобританія) (mpg)",

"Title of new tool" : "Назва нового інструменту",
"Name of the variable" : "Ім'я змінної",
"Add more variable" : "Додати змінну",
"Formula of the tool" : "Формула інструменту",
"Name of the result " : "Назва результаті",
"Add more result" : "Додати результат",
"EULER'S FORMULA" : "Формула Ейлера",

};