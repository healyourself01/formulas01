﻿var language={

"1. 2D SHAPES" : "1. Плоские фигуры",
"2. 3D SHAPES" : "2. Объёмные фигуры",
"TRIANGLE" : "Треугольник",
"RIGHT TRIANGLE" : "Прямоугольный треугольник",
"SQUARE" : "Квадрат",
"RECTANGLE" : "Прямоугольник",
"PARALLELOGRAM" : "Параллелограмм",
"LOZENGE" : "Ромб",
"TRAPEZOID" : "Трапеция",
"CONVEX QUADRILATERAL" : "Выпуклый четырёхугольник",
"CIRCLE" : "Круг",
"SEGMENT OF CIRCLE" : "Сегмент Круга",
"SECTOR OF CIRCLE" : "Сектор круга",
"REGULAR POLYGON OF N SIDES" : "Правильный многоугольник с N сторонами",
"REGULAR POLYGON" : "Правильный многоугольник",
"REGULAR POLYGON" : "Правильный многоугольник",
"HEXAGON" : "Шестиугольник",
"SPHERE" : "Сфера",
"CYLINDER" : "Цилиндр",
"CYLINDER" : "Цилиндр",
"CONE" : "Конус",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "Усеченный прямой круговой конус",
"PYRAMID" : "Пирамида",
"SQUARE PYRAMID" : "Квадратная пирамида",
"CUBOID" : "Прямоугольный параллелепипед",
"CUBOID" : "Прямоугольный параллелепипед",
"TRIANGULAR PRISM" : "Треугольная призма",
"A: Area" : "A: Площадь",
"A: Area, P: Perimeter" : "A: Площадь, P: Периметр",
"b: Arc length" : "b: Длина дуги",
"V: Volume, A: Surface Area" : "V: Объём, А: Площадь поверхности",
"A: Lateral surface area" : "A: Площадь боковой поверхности ",
"A: Lateral surface area" : "A: Площадь боковой поверхности ",
"A: Base Area" : "A: Площадь основания",
"A: Surface Area" : "А: Площадь поверхности",

"1. OPERATIONS ON EXPRESSIONS" : "1. Операции с выражениями",
"POLYNOMIAL" : "Многочлен",
"FRACTIONS" : "Дроби",
"IDENTITY" : "Тождество",
"EXPONENTIATION" : "Возведение в степень",
"ROOTS" : "Корень",
"2. RATE FORMULAS" : "2. Формулы отношения",
"6. COMPLEX NUMBERS" : "6. Комплексные числа",
"COMPLEX PLANE" : "Комплексная плоскость",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. Прогрессия \nАрифметическая прогрессия",
"4. PROGRESSION" : "4. Прогрессия", 
"ARITHMETIC PROGRESSION" : "Арифметическая прогрессия", 
"GEOMETRIC PROGRESSION" : "Геометрическая прогрессия", 
"SUMMATIONS" : "Суммирование",
"5. LOGARITHM" : "5. Логарифм", 
"DECIMAL LOGARITHM" : "Десятичный логарифм",
"NATURAL LOGARITHM" : "Натуральный логарифм (ln)",
"d: common difference" : "d: общая разница",
"q: common ratio" : "q: общий коэффициент",
"3. INEQUALITIES" : "3. Неравенства ",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "Тригонометрические функции для прямоугольного треугольника",
"TRIGONOMETRIC TABLE" : "Тригонометрические таблицы",
"CO-RATIOS" : "Соотношения углов",
"BASIC FORMULAS" : "Основные  формулы",
"MULTIPLE ANGLE FORMULAS" : "Формулы для нескольких углов",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "Формулы понижения степени",
"ADDITION FORMULAS" : "Формулы сложения (тригонометрических функций)",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "Сумма тригонометрических функций",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "Произведение тригонометрических функций",
"HALF ANGLE FORMULAS" : "Формулы половинного угла",
"ANGLES OF A PLANE TRIANGLE" : "Углы плоского треугольника",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "Соотношения между тригонометрическими функциями",
"α, β, γ are 3 angles of a triangle" : "α, β, γ - это 3 угла треугольника",


"1. ALGEBRAIC EQUATIONS" : "1. Алгебраическое уравнение",
"LINEAR EQUATION" : "Линейное уравнение",
"SYSTEM OF TWO LINEAR EQUATIONS" : "Система двух линейных уравнений",
"QUADRATIC EQUATION" : "Квадратное уравнение",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. Экспонента и логарифм \nПоказательное уравнение",
"2. EXPONENT AND LOGARITHM" : "2. Экспонента и логарифм",
"LOGARITHMIC EQUATION" : "Логарифмические уравнения",
"3. TRIGONOMETRIC EQUATION" : "3. Тригонометрические уравнения",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. Неравенства \nЛинейное неравенство",
"4. INEQUATIONS" : "4. Неравенства ",
"QUADRATIC INEQUATION" : "Квадратное неравенство",
"EXPONENTIAL INEQUATION" : "Экспоненциальная неравенство",
"LOGARITHMIC INEQUATION" : "Логарифмическое неравенство",
"TRIGONOMETRIC INEQUATIONS" : "Тригонометрические неравенства",
"LINEAR EQUATION" : "Линейное уравнение",
"EXPONENTIAL EQUATION" : "Показательное уравнение",
"LINEAR INEQUATION" : "Линейное неравенство",
"CUBIC EQUATION" : "Кубическое уравнение",
"POINTS" : "Точки",
"- Distance between two points A and B" : " - Расстояние между двумя точками А и В",
"- Distance between point A and origin:" : " - Расстояние между точкой А и началом координат",
"TRIANGLE" : "Треугольник",
"- Area of triangle with vertices at A, B, C" : "- Площадь треугольника с вершинами в A, B, C",
"- Area of a triangle with a vertice at origin" : "- Площадь треугольника с вершиной в точке начала координат",
"EQUATION OF LINE" : "Уравнение линии",
"- Joining two points A, B" : "- Соединяющей две точки A, B",
"- Passing point A and parallel with line y=ax+b" : "- Проходящей через  точку А и параллельно прямой у=ах+b",
"- Passing point A and perpendicular with line y=ax+b" : "- Проходящей через точку А и перпендикулярно  линии у=ах+b",
"EQUATION OF CIRCLE" : "Уравнение окружности ",
"- Circle with radius r and center at (a, b)" : "- Окружность радиусом r с центром в точке (а, b)",
"- Circle with center at origin" : "- Окружность с центром в начале координат",
"ELLIPSE" : "Эллипс",
"HYPERBOLA" : "Гипербола",
"PARABOLA" : "Парабола",

"Limit \n" : "Предел \n",
"Derivative \n" : "Производная \n",
"C: constant" : "C: Постоянная",
"Differentiation \n" : "Дифференцирование \n",
"C: constant; u, v, w: functions of x" : "C: постоянная, u, v, w: функции x",
"LIMIT" : "Предел",
"DERIVATIVE" : "Производная",
"DIFFERENTIATION" : "Дифференцирование",

"1. INDEFINITE INTEGRALS" : "1. Неопределенные интегралы",
"C: arbitrary constant, k: constant" : "C: произвольная постоянная, k: постоянная",
"INTEGRALS BY PARTIAL FRACTIONS" : "Интегралы с простейшими дробями",
"INTEGRALS INVOLVING ROOTS" : "Интегралы, включающие корни",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "Интегралы, включающие тригонометрические корни",
"2. DEFINITE INTEGRALS" : "2. Определенные интегралы",
"APPLICATIONS" : "Применение",
"- Surface area created by y=f(x)" : "- Площадь поверхности, образованной у= f(x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- Объем тела, образованного вращением у= f(x) вокруг оси:",

"Median" : "Медиана",
"Angle bisector" : "Биссектриса",
"SPHERICAL CAP" : "Полусфера",
"SPHERICAL SEGMENT" : "Шаровой сегмент",
"SPHERICAL SECTOR" : "Шаровой сектор",
"TORUS" : "Тор",
"FORMULAS WITH t=tan(x/2)" : "Формулы с t=tan(x/2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "Стороны и углы плоского треугольника",
"Law of sines, cosines and tangents" : "Теорема синусов, косинусов и тангенсов",

"1. PLANE ANALYTIC GEOMETRY" : "1. Аналитическая планиметрия",
"2. SOLID ANALYTIC GEOMETRY" : "2. Аналитическая стереометрия",
"LINE" : "Линия",
"- Direction Cosines of Line Joining Points A and B" : "- Направляющий косинус линии, соединяющей точки А и В",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "Уравнение линии, соединяющей две точки A, B",
"- In standard form" : "- в стандартной форме",
"- In parametric form" : "- в параметрической форме",
"PLANE" : "Плоскость ",
"- General equation of a plane" : "- Общее Уравнение плоскости",
"- Equation of plane passing through point A, B, C" : "- Уравнение плоскости, проходящей через точки A, B, C",
"- Equation of plane in intercept form" : "- Уравнение плоскостей в пересекающихся формах",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a, b, c, пересекающие оси х, у, z соответственно",
"- Normal Form for Equation of Plane" : "- Нормальная форма для уравнения плоскости",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "р: перпендикулярное расстояние от О до плоскости в P; α, β, γ: углы между OP и положительной частью осей x,y,z",
"- Distance from point M to a plane" : "- Расстояние от точки М до плоскости",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Уравнение сферы с центром М и радиусом R в прямоугольных координатах",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "Уравнение эллипсоида с центром М и полуосями a, b, c",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Эллиптический цилиндр с осью, совпадающей с осью z",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "Эллиптический конус с осью, совпадающей с осью z",
"HYPERBOLOID OF ONE SHEET" : "Однополостный гиперболоид",
"HYPERBOLOID OF TWO SHEETS" : "Двуполостный гиперболоид",
"ELLIPTIC PARABOLOID" : "Эллиптический параболоид",
"HYPERBOLIC PARABOLOID" : "Гиперболический параболоид",

"TRANSFORMATIONS" : "Преобразования",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. Специальные неопределенные интегралы",
"Integrals involving ax+b" : "Интегралы, содержащие ах+b",
"Integrals involving ax+b and px+q" : "Интегралы, содержащие ах+b и px+q",
"Integrals involving x²+a²" : "Интегралы, содержащие x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "Интегралы, содержащие x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a²" : "Интегралы, содержащие x²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "Интегралы, содержащие ax²+bx+c",
"Integrals involving xⁿ+aⁿ" : "Интегралы, содержащие xⁿ+aⁿ",
"Integrals involving sin(ax)" : "Интегралы, содержащие sin(ax)",
"Integrals involving e^(ax)" : "Интегралы, содержащие е^(ax)",
"Integrals involving ln(x)" : "Интегралы, содержащие ln(x)",

"TRANSPOSE OF A MATRIX" : "Транспонирование матрицы",
"ADDITION AND SUBTRACTION OF MATRICES" : "Сложение и вычитание матриц",
"MULTIPLICATION OF MATRICES" : "Умножение матриц",
"DETERMINANT OF MATRIX" : "Определитель матрицы",
"INVERSE OF MATRIX" : "Обратные матрицы",
"EQUATION IN MATRIX FORM" : "Уравнения в матричной форме",
"PROPERTIES OF MATRIX CALCULATIONS" : "Свойства матричных вычислений",
"LENGTH" : "Длина",
"AREA" : "Площадь",
"VOLUME" : "Объём",
"MASS" : "Масса",
"SPEED" : "Скорость",
"TIME" : "Время",
"TEMPERATURE" : "Температура",
"DENSITY" : "Плотность",
"FORCE" : "Силы",
"ENERGY" : "Энергия",
"POWER" : "Мощность",
"PRESSURE" : "Давление",
"Do you know this?" : "Знаете ли вы это?",
"What is special about these calculations?" : "Что особенного в этих расчетах?",
"Can you find the interesting trait of these calculations?" : "Можете ли вы найти интересную черту этих расчетов?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. График других функций ",
"Constant" : "Постоянная",
"Absolute" : "Абсолютная величина",
"Square Root" : "Квадратный корень ",
"Parabolic" : "Параболический",
"Cubic" : "Кубический ",
"Reciprocal" : "Обратное число",
"Sec" : "Sec (секанс)",
"Cosec" : "Cosec (косеканс)",
"6. FUNCTION TRANSFORMATIONS" : "6. Функциональные преобразования ",
"Horizontal Shifting" : "Горизонтальный сдвиг ",
"Vertical Shifting" : "Вертикальное перемещение ",
"Reflection" : "Отражение ",
"Stretching" : "Pастягивание",
"F, F1: Focus points (foci)" : "F, F1: Точки фокусирования (фокусы)",
"AF=p: Parameter of Parabola" : "AF=p: Параметр параболы",

"Probability & Statistics" : "Вероятность & Cтатистика",
"1. SETS" : "1. Множества",
"SET" : "Множество",
"SUBSET" : "Подмножество",
"INTERSECTION" : "Пересечение",
"UNION" : "Объединение",
"SYMMETRIC DIFFERENCE" : "Симметрическая разность",
"RELATIVE COMPLEMENT OF A IN B" : "Относительное дополнение А у В",
"ABSOLUTE COMPLEMENT" : "Абсолютное дополнение",
"OPERATIONS ON SETS" : "Операции над множествами",
"2. COMBINATIONS AND PERMUTATIONS" : "2. Сочетание и перестановка",
"COMBINATIONS" : "Сочетание",
"PERMUTATIONS" : "Перестановка",
"3. PROBABILITY" : "3. Вероятность",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) Если А и В являются взаимоисключающими, (2) Если А и В независимы ",
"4. STATISTICS" : "4. Cтатистика",
"MEAN" : "Среднее значение",
"MEDIAN" : "Медиана",
"MODE" : "Мода",
"Numerical value that occurs the most number of times" : "Числовое значение, которое встречается наиболее часто",
"EXAMPLE" : "Пример ",
"GEOMETRIC MEAN" : "Среднее геометрическое",
"HARMONIC MEAN" : "Среднее гармоническое ",
"VARIANCE" : "Дисперсия случайной величины",
"STANDARD DEVIATION" : "Среднеквадратическое отклонение",
"MEAN DEVIATION" : "Cреднее отклонение ",
"ROOT MEAN SQUARE" : "Среднее квадратическое",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "Нормальное распределение (распределение Гаусса) ",
"f: density function, F: Distribution function, μ: mean" : "f: функция плотности, F: Функция распределения, μ: среднее",
"EXPONENTIAL DISTRIBUTION" : "Экспоненциальное распределение",
"POISSON DISTRIBUTION" : "Распределение Пуассона",
"UNIFORM DISTRIBUTION" : "Равномерное распределение ",
"Transforms" : "Преобразования ",
"1. FOURIER SERIES AND TRANSFORMS" : "1. Ряды Фурье и преобразования ",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): периодическая функция, период 2L; a_n, b_n: коэффициенты Фурье; c_n: комплексный коэффициент Фурье ",
"REAL FORM OF FOURIER SERIES" : "Настоящее форма ряда Фурье ",
"COMPLEX FORM" : "Комплексная форма ",
"PARSEVAL’S THEOREM" : "Теорема Парсеваля ",
"FOURIER TRANSFORM" : "Преобразование Фурье ",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): функция х, F(s): преобразование Фурье f(x)",
"CONVOLUTION" : "Свёртка",
"CORRELATION" : "Корреляция ",
"f*: complex conjugate of f" : "f*: комплексно сопряженное f",
"FOURIER SYMMETRY RELATIONSHIPS" : "Отношения симметрии Фурье ",
"FOURIER TRANSFORM PAIRS" : "Преобразование Фурье некоторых функций",
"2. LAPLACE TRANSFORMS" : "2. Преобразование Лапласа",
"DEFINITION" : "Определение ",
"CONVOLUTION" : "Свёртка",
"INVERSE" : "Обратное преобразование Лапласа",
"a: constant" : "a: постоянная ",
"DERIVATIVE" : "Производная",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "Замена (сдвиг частоты)",
"TRANSLATION (TIME SHIFTING)" : "Трансляция (Сдвиг во времени)",
"LAPLACE TRANSFORM PAIRS" : "Преобразование Лапласа некоторых функций",

"Math Tricks" : "Математические приемы",
"1. ADDITION" : "1. Дополнение ",
"2. SUBTRACTION" : "2. Вычитание ",
"3. MULTIPLICATION" : "3. Умножение ",
"4. DIVISION" : "4. Деление",
"5. SQUARING" : "5. Возведение в квадрат",
"6. EXPONENTIATION" : "6. Возведение в степень",
"7. ROOTS" : "7. Корень",
"8. SUMMATIONS" : "8. Суммирование",
"6. SUMMATIONS" : "6. Суммирование",

"UNITS CONVERTER" : "Единицы конвертер ",
"ANGLE" : "Угол",
"COMPUTER DATA" : "Компьютер данных ",
"FUEL CONSUMPTION" : "Расход топлива ",
"Click here to download Physics Formulas app" : "Нажмите здесь, чтобы загрузить приложение Physics Formulas ",
"All Maths formulas for your work and study" : "Все математические формулы для вашей работы и учебы",
"EDIT" : "Редактировать ",
"ADD" : "Добавить ",
"TITLE OF THE PICTURE" : "Название картины ",
"GAME" : "Игра",
"START" : "Начать",
"LEVEL" : "Уровень",
"RETRY" : "Повторить",
"SCORE" : "Cчет",
"TIME" : "Время",
"BEST" : "Лучший ",
"GAME OVER" : "Game Over",
"YOUR SCORE IS: " : "Ваша оценка является ",



"Metre (m)" : "Mетр (m)",
"Kilometre (km)" : "Kилометр (km)",
"Centimetre (cm)" : "Сантиметр (cm)",
"Millimetre (mm)" : "Миллиметр (mm)",
"Micrometre | Mircon (μm)" : "Микрометр | Микрон (μm)",
"Nanometre (nm)" : "Нанометр (nm)",
"Inch (in)" : "Дюйм (in)",
"Foot (ft)" : "Foot Фут (ft)",
"Yard (yd)" : "Двор Ярд (yd)",
"Mile (mi)" : "Миля  (mi)",
"Light Year" : "Световой год",
"Nautical Mile (NM)" : "Морская миля (NM)",
"Angstrom (Å)" : "Ангстрем (Å)",

"Square Mile (mi²)" : "Квадратная миля (mi²)",
"Square Yard (yd²)" : "Площадь участка (yd²)",
"Square Foot (ft²)" : "Квадратный фут (ft²)",
"Square Inch (in²)" : "Квадратный дюйм (in²)",
"Square Kilometre (km²)" : "Квадратный километр (km²)",
"Hectare (ha)" : "Гектар  (ha)",
"Acre" : "Акко Акр",
"Square Metre (m²)" : "Квадратный метр  (m²)",
"Square Centimetre (cm²)" : "Квадратный  Сантиметр  (cm²)",
"Square Millimetre (mm²)" : "Квадратный  миллиметра  (mm²)",

"Litre (l)" : "л  (l)",
"Cubic Metre (m³)" : "Кубический метр (m³)",
"Cubic Inch (in³)" : "Кубический дюйм (in³)",
"Cubic Foot (ft³)" : "Кубический фут (ft³)",
"Cubic Yard (yd³)" : "Кубический ярд (yd³)",
"Gallon (US) (gal)" : "Галлон (США)  (gal)",
"Gallon (UK) (gal)" : "Галлон (Великобритания)  (gal)",

"Gram (g)" : "Грамм  (g)",
"Kilogram (kg)" : "Килограмм  (kg)",
"Tonne (t)" : "Тонна  (t)",
"Milligram (mg)" : "Миллиграмм (mg)",
"Microgram (μg)" : "микрограмм  (μg)",
"Ounce (oz)" : "Унция  (oz)",
"Pound (lb)" : "фунт  (lb)",
"Carat" : "Карат",
"Slug" : "Slug ",
"Ounce (Troy) (ozt)" : "Унция (Troy)  (ozt)",

"Year" : "Год ",
"Month" : "Mесяц ",
"Week" : "Неделя ",
"Day" : "День ",
"Hour (h)" : "Час  (h)",
"Minute (min)" : "Mинуту  (min)",
"Second (s)" : "Секунды  (s)",
"Millisecond (ms)" : "Миллисекунда  (ms)",
"Microsecond (μs)" : "Микросекунда (μs)",
"Decade" : "Десятилетие Декада",
"Century" : "Bек ",
"Millennium" : "Тысячелетие",

"Kilometre per Hour (km/h)" : "Километр в час  (km/h)",
"Miles per Hour (mi/h)" : "Миль в час  (mi/h)",
"Metre per Second (m/s)" : "Метр в секунду  (m/s)",
"Foot per Second (ft/s)" : "Фут в секунду  (ft/s)",
"Knot" : "Узел ",
"Speed of Light (c)" : "Скорость Света  (c)",
"Minutes per Mile (min/mi)" : "Минут на Миле  (min/mi)",
"Minutes per Kilometre (min/km)" : "Минут на километр  (min/km)",

"Celsius (°C)" : "Цельсий (°C)",
"Fahrenheit (°F)" : "Фаренгейт (°F)",
"Kelvin (K)" : "Кельвин  (K)",
"Rankine (°R)" : "Ранкин (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "Килограмм / Кубический метр  (kg/m³)",
"Kilogram/Litre (kg/l)" : "Килограмм / литр (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "Slug / кубический фут  (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "Фунт / кубический фут  (lbm/ft³)",

"Newton (N)" : "Ньютон  (N)",
"Kilonewton (kN)" : "Kилоньютон  (kN)",
"Atomic unit of force" : "Атомная единица силы",
"Gram Force (gf)" : "Грамм Force  (gf)",
"Pound Force (lbf)" : "фунт Force  (lbf)",
"Poundal (pdl)" : "Паундаль  (pdl)",
"Dyne (dyn)" : "Дина  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "Килограмм-сила | Килопонд (kgf)",

"Watt (W)" : "Вт  (W)",
"Kilowatt (kW)" : "Kиловатт  (kW)",
"Megawatt (MW)" : "Мегаватт  (MW)",
"Milliwatt (mW)" : "Милливатт (mW)",
"Microwatt (µW)" : "Mикроватт  (µW)",
"Horsepower (hp)" : "Мощность  (hp)",
"Calorie per Second (cal/s)" : "Калорийность в секунду  (cal/s)",

"Pascal (Pa)" : "Паскаль  (Pa)",
"Kilopascal (kPa)" : "Килопаскаль  (kPa)",
"Megapascal (MPa)" : "MПа  (MPa)",
"Bar (bar)" : "Бара (bar)",
"Millibar (mbar)" : "Mиллибара  (mbar)",
"Atmosphere (atm)" : "Атмосфера (атм) ",
"Pound Force per Square Inch (psi)" : "Понд сила на квадратный дюйм  (psi)",
"Dyne per Square Centimetre" : "Дина за квадратный Сантиметр ",

"Degree (°)" : "Степень  (°)",
"Radian (rad)" : "Радиан  (rad)",
"π Radian (π rad)" : "π Радиан  (π rad)",
"Second" : "Секунды ",
"Minute (')" : "Mинуту  (')",

"Byte (B)" : "байт  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "Килобайт | Kibibyte  (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "Мегабайт | Mебибайт  (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Гигабайт | Гибибайт  (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "Терабайт | Тебибайт (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "Петабайт | Пебибайт (PB | Pi)",
"Bit (b)" : "Бит  (b)",
"Zebibyte (Zi)" : "Зебибайт (Zi)",

"Litres per 100 Kilometres (l/100km)" : "Литров на 100 километрах  (l/100km)",
"Kilometres per Litre (km/l)" : "Километры в литр  (km/l)",
"Miles per Gallon (US) (mpg)" : "Миль на галлон (США)  (mpg)",
"Miles per Gallon (UK) (mpg)" : "Миль на галлон (Великобритания) (mpg)",


"Title of new tool" : "Название нового инструмента",
"Name of the variable" : "Имя переменной",
"Add more variable" : "Добавить переменную",
"Formula of the tool" : "Формула инструмента",
"Name of the result " : "Название результате",
"Add more result" : "Добавить результат",
"EULER'S FORMULA" : "Формула Эйлера",

		
};