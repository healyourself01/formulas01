﻿var language={

"1. 2D SHAPES" : "1. 2D Tvary",
"2. 3D SHAPES" : "2. 3D Tvary ",
"TRIANGLE" : "Trojúhelník",
"RIGHT TRIANGLE" : "Pravoúhlý trojúhelník",
"SQUARE" : "Čtverec",
"RECTANGLE" : "Obdélník",
"PARALLELOGRAM" : "Rovnoběžník",
"LOZENGE" : "Kosočtverec",
"TRAPEZOID" : "Lichoběžník",
"CONVEX QUADRILATERAL" : "Konvexní čtyřúhelník",
"CIRCLE" : "Kruh",
"SEGMENT OF CIRCLE" : "Segment kruhu",
"SECTOR OF CIRCLE" : "Sektor kruhu",
"REGULAR POLYGON OF N SIDES" : "Pravidelný polygon s N stranami",
"REGULAR POLYGON" : "Pravidelný polygon",
"REGULAR POLYGON" : "Pravidelný polygon",
"HEXAGON" : "Hexagon",
"SPHERE" : "Sféra",
"CYLINDER" : "Válec",
"CYLINDER" : "Válec",
"CONE" : "Kužel",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "Komolý rotační kužel",
"PYRAMID" : "Pyramida",
"SQUARE PYRAMID" : "Čtvercová pyramida",
"CUBOID" : "Kvádr",
"CUBOID" : "Kvádr",
"TRIANGULAR PRISM" : "Trojboký hranol",
"A: Area" : "A: Plocha",
"A: Area, P: Perimeter" : "A: Plocha, P: Obvod",
"b: Arc length" : "b: délka oblouku",
"V: Volume, A: Surface Area" : "V: Objem, A: Plocha povrchu",
"A: Lateral surface area" : "A: Boční plocha",
"A: Lateral surface area" : "A: Boční plocha",
"A: Base Area" : "A: plocha základny",
"A: Surface Area" : "A: Surface Area",

"1. OPERATIONS ON EXPRESSIONS" : "1. Operace s výrazy ",
"POLYNOMIAL" : "Mnohočleny",
"FRACTIONS" : "Zlomky",
"IDENTITY" : "Totožnost",
"EXPONENTIATION" : "Umocňování",
"ROOTS" : "Kořeny",
"2. RATE FORMULAS" : "2. Sazba Vzorce",
"6. COMPLEX NUMBERS" : "6. Komplexní čísla",
"COMPLEX PLANE" : "Komplexní plocha",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. Postup \n Aritmetický postup",
"4. PROGRESSION" : "4. Postup", 
"ARITHMETIC PROGRESSION" : "Aritmetický postup", 
"GEOMETRIC PROGRESSION" : "Geometrický postup", 
"SUMMATIONS" : "Součet ",
"5. LOGARITHM" : "5. logaritmus", 
"DECIMAL LOGARITHM" : "Desetinný logaritmus",
"NATURAL LOGARITHM" : "Přirozený logaritmus",
"d: common difference" : "d: společná rozdíl",
"q: common ratio" : "q: společná poměr",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "Goniometrické funkce pro pravoúhlý trojúhelník",
"TRIGONOMETRIC TABLE" : "Trigonometrická tabulka",
"CO-RATIOS" : "Spolupořadatelé poměry",
"BASIC FORMULAS" : "Základní vzorce",
"MULTIPLE ANGLE FORMULAS" : "Víceúhlové vzorce",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "Mocniny goniometrických funkcí",
"ADDITION FORMULAS" : "Doplněkové vzorce",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "Sumář goniometrických funkcí",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "Výsledek goniometrických funkcí",
"HALF ANGLE FORMULAS" : "Vzorce polovičních úhlů",
"ANGLES OF A PLANE TRIANGLE" : "Úhly roviných trojúhelníkú",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "Vztahy mezi goniometrickými funkcemi",
"α, β, γ are 3 angles of a triangle" : "α, β, γ jsou 3 úhly trojúhelníku",
"3. Inequalities" : "3. Nerovnosti",

"1. ALGEBRAIC EQUATIONS" : "1. Algebraické rovnice",
"LINEAR EQUATION" : "Lineární rovnice",
"SYSTEM OF TWO LINEAR EQUATIONS" : "Systém dvou lineárních rovnic",
"QUADRATIC EQUATION" : "Kvadratická rovnice",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. Exponent a logaritmus \n Exponenciální rovnice",
"2. EXPONENT AND LOGARITHM" : "2. Exponent a logaritmus",
"LOGARITHMIC EQUATION" : "Logaritmické rovnice",
"3. TRIGONOMETRIC EQUATION" : "3. Trigonometrické rovnice",

"4. INEQUATIONS" : "4. Nerovnosti",
"QUADRATIC INEQUATION" : "Kvadratická nerovnost",
"EXPONENTIAL INEQUATION" : "Exponenciální nerovnost",
"LOGARITHMIC INEQUATION" : "Logaritmická nerovnost",
"TRIGONOMETRIC INEQUATIONS" : "Goniometrické nerovnice",
"LINEAR EQUATION" : "Lineární rovnice",
"EXPONENTIAL EQUATION" : "Exponenciální rovnice",
"LINEAR INEQUATION" : "Lineární nerovnost",
"CUBIC EQUATION" : "Kubické rovnice",
"POINTS" : "Body",
"- Distance between two points A and B" : "- Vzdálenost mezi dvěma body  A a B",
"- Distance between point A and origin:" : "- Vzdálenost mezi bodem A a počátkem:",
"TRIANGLE" : "Trojúhelník",
"- Area of triangle with vertices at A, B, C" : "- Povrch trojúhelníku s vrcholi A, B, C",
"- Area of a triangle with a vertice at origin" : "- Povrch trojúhelníku s vrcholem v počátku",
"EQUATION OF LINE" : "Rovnice pro přímku",
"- Joining two points A, B" : "- Spojení dvou bodů A, B",
"- Passing point A and parallel with line y=ax+b" : "- Průsečík přímky A a paralelní přímky y=ax+b",
"- Passing point A and perpendicular with line y=ax+b" : "- Průsečík přímky A a kolmice y=ax+b",
"EQUATION OF CIRCLE" : "Rovnice kružnice",
"- Circle with radius r and center at (a, b)" : "- Kruh s poloměrem r a středem v (a, b)",
"- Circle with center at origin" : "- Kruh se středem v počátku",
"ELLIPSE" : "Elipsa",
"HYPERBOLA" : "Hyperbola",
"PARABOLA" : "Parabola",

"Limit \n" : "Limit \n",
"Derivative \n" : "Derivát \n",
"C: constant" : "C: konstanta",
"Differentiation \n" : "Diferenciace \n",
"C: constant; u, v, w: functions of x" : "C: konstanta; u, v, w: funkce x",
"LIMIT" : "Limit",
"DERIVATIVE" : "Derivát",
"DIFFERENTIATION" : "Diferenciace ",

"1. INDEFINITE INTEGRALS" : "1. Neurčité integrály",
"C: arbitrary constant, k: constant" : "C: libovolná konstanta, k: konstanta",
"INTEGRALS BY PARTIAL FRACTIONS" : "Integrály s parciálním zlomky",
"INTEGRALS INVOLVING ROOTS" : "Integrály včetně kořenů",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "Integrály včetně goniometrických funkcí",
"2. DEFINITE INTEGRALS" : "2. Určité integrály",
"APPLICATIONS" : "Aplikace",
"- Surface area created by y=f(x)" : "- Plocha vytvořená y=f(x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- Objem pevné látky vytvořený y=f(x) otáčené kolem osy: ",

"Median" : "Medián",
"Angle bisector" : "Osa úhlu",
"SPHERICAL CAP" : "Polokoule",
"SPHERICAL SEGMENT" : "Sférický segmentu",
"SPHERICAL SECTOR" : "Sférický průmysl",
"TORUS" : "Plodnic",
"FORMULAS WITH t=tan(x/2)" : "Vzorce s t=tan(x/2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "Strany a úhly rovin trojúhelníku",
"Law of sines, cosines and tangents" : "Sinová, cosinová a tangentová věta",

"1. PLANE ANALYTIC GEOMETRY" : "1. Rovinná analytická geometrie",
"2. SOLID ANALYTIC GEOMETRY" : "2. Analytický geometrie těles",
"LINE" : "Řádek",
"- Direction Cosines of Line Joining Points A and B" : "- Směr cosinu na čáře spojující body  A, B",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "Rovnice přímky spojující dva body A, B",
"- In standard form" : "- Ve standardním formuláři",
"- In parametric form" : "- V parametrické podobě",
"PLANE" : "PLOCHA",
"- General equation of a plane" : "- Obecná rovnice roviny,",
"- Equation of plane passing through point A, B, C" : "- Rovnice roviny procházející bodem A, B, C",
"- Equation of plane in intercept form" : "- Rovnice roviny ve formě zachycovací",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a, b, c se zachytí na x, y, z os, resp",
"- Normal Form for Equation of Plane" : "- Normální forma pro rovnici roviny",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p: kolmé vzdálenosti od O do roviny v bodě P; α, β, γ: úhly mezi OP a kladnými úhly x, y, z osy",
"- Distance from point M to a plane" : "- Vzdálenost od bodu M k rovině",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "Rovnice středu koule na M a poloměru R v pravoúhlých souřadnicích",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "Rovnice elipsoidu se středem M a polo-osami a, b, c",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Eliptický válec s osami jako osy z",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : " Elliptic kužel s osami jako osa z",
"HYPERBOLOID OF ONE SHEET" : "Hyperboloid z jedné vrstvy",
"HYPERBOLOID OF TWO SHEETS" : "Hyperboloid ze dvou vrstev",
"ELLIPTIC PARABOLOID" : "Elliptický paraboloid",
"HYPERBOLIC PARABOLOID" : "Hyperbolický paraboloid",

"TRANSFORMATIONS" : "Transformace",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. Zvláštní neurčitá integrály",
"Integrals involving ax+b" : "Integrály včetně ax+b",
"Integrals involving ax+b and px+q" : "Integrály včetně ax+b a px+q",
"Integrals involving x²+a²" : "Integrály včetně x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "Integrály včetně x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a²" : "Integrály včetně x²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "Integrály včetně ax²+bx+c",
"Integrals involving xⁿ+aⁿ" : "Integrály včetně xⁿ+aⁿ",
"Integrals involving sin(ax)" : "Integrály včetně sin(x)",
"Integrals involving e^(ax)" : "Integrály včetně e^(ax)",
"Integrals involving ln(x)" : "Integrály včetně ln(x)",

"TRANSPOSE OF A MATRIX" : "Transponovat matice",
"ADDITION AND SUBTRACTION OF MATRICES" : "Sčítání a odčítání matic",
"MULTIPLICATION OF MATRICES" : "Násobení matic",
"DETERMINANT OF MATRIX" : "Determinant matice",
"INVERSE OF MATRIX" : "Inverzní matice",
"EQUATION IN MATRIX FORM" : "Rovnice ve formě matice",
"PROPERTIES OF MATRIX CALCULATIONS" : "Vlastnosti maticového výpočtu",
"LENGTH" : "Délka",
"AREA" : "Plocha",
"VOLUME" : "Objem",
"MASS" : "Hmota",
"SPEED" : "Rychlost",
"TIME" : "Čas",
"TEMPERATURE" : "Teplota",
"DENSITY" : "Hustota",
"FORCE" : "Síla",
"ENERGY" : "Energie",
"POWER" : "Energie",
"PRESSURE" : "Tlak",
"Do you know this?" : "Víte to?",
"What is special about these calculations?" : "Co je zvláštního na těchto výpočtech?",
"Can you find the interesting trait of these calculations?" : "Můžete najít zajímavý rys těchto výpočtů?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. Graf s dalšími funkcemi",
"Constant" : "Konstantní",
"Absolute" : "Absolutní",
"Square Root" : "Druhá odmocnina",
"Parabolic" : "Parabolický",
"Cubic" : "Krychlový",
"Reciprocal" : "Reciproční",
"Sec" : "Sekunda",
"Cosec" : "Cosec",
"6. FUNCTION TRANSFORMATIONS" : "6. PROMĚNY FUNKCÍ",
"Horizontal Shifting" : "Horizontální posun",
"Vertical Shifting" : "Vertikální posun",
"Reflection" : "Odraz",
"Stretching" : "Protahování",
"F, F1: Focus points (foci)" : "F, F1: Zaostřovacími body (ohniska)",
"AF=p: Parameter of Parabola" : "AF=p: Parametr paraboly",

"Probability & Statistics" : "Pravděpodobnost a statistika",
"1. SETS" : "1. Množiny",
"SET" : "Množiny",
"SUBSET" : "Podmnožiny",
"INTERSECTION" : "Průsečík ",
"UNION" : "Spojení",
"SYMMETRIC DIFFERENCE" : "Symetrická diference",
"RELATIVE COMPLEMENT OF A IN B" : "Relativní doplněk A v B",
"ABSOLUTE COMPLEMENT" : "Absolutní doplňkem",
"OPERATIONS ON SETS" : "Operace s množinami",
"2. COMBINATIONS AND PERMUTATIONS" : "2. Kombinace a permutace",
"COMBINATIONS" : "Kombinace",
"PERMUTATIONS" : "Permutace",
"3. PROBABILITY" : "3. Pravděpodobnost",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) Pokud se A a B  vzájemně vylučují, (2) Jsou-li A a B nezávislé",
"4. STATISTICS" : "4. Statistika",
"MEAN" : "Znamenat",
"MEDIAN" : "Medián",
"MODE" : "Režim",
"Numerical value that occurs the most number of times" : "Číselnou hodnotu, u které nastává největší počet opakování",
"EXAMPLE" : "Příklad",
"GEOMETRIC MEAN" : "Geometrický průměr",
"HARMONIC MEAN" : "Harmonický průměr",
"VARIANCE" : "Odchylka",
"STANDARD DEVIATION" : "Směrodatná odchylka",
"MEAN DEVIATION" : "Střední odchylka ",
"ROOT MEAN SQUARE" : "Střední kvadratická odchylka",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "Normální rozdělení ",
"f: density function, F: Distribution function, μ: mean" : "f: Funkce hustoty, F: Distribuční funkce, μ: průměr",
"EXPONENTIAL DISTRIBUTION" : "Exponenciální rozdělení",
"POISSON DISTRIBUTION" : "Poissonovo rozdělení",
"UNIFORM DISTRIBUTION" : "Rovnoměrné rozložení",
"Transforms" : "Transformace",
"1. FOURIER SERIES AND TRANSFORMS" : "1. Fourierova řada a transformace",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): periodická funkce, doba 2L; a_n, b_n: Fourier koeficienty; c_n: komplexní Fourierova koeficientu",
"REAL FORM OF FOURIER SERIES" : "Skutečná podoba Fourierovy série",
"COMPLEX FORM" : "Komplexní forma",
"PARSEVAL’S THEOREM" : "Parsevalova věta",
"FOURIER TRANSFORM" : "Fourierova transformace",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): funkce x, F(s): Fourierova transformace f(x)",
"CONVOLUTION" : "Konvoluce",
"CORRELATION" : "Korelace",
"f*: complex conjugate of f" : "f*: komplexně sdružená hodnota f",
"FOURIER SYMMETRY RELATIONSHIPS" : "Vztahy Fourierovy symetrie ",
"FOURIER TRANSFORM PAIRS" : "Fourierova transformace párů",
"2. LAPLACE TRANSFORMS" : "2. Laplaceova transformace",
"DEFINITION" : "Definice",
"CONVOLUTION" : "Konvoluce",
"INVERSE" : "Obrácený",
"a: constant" : "a: konstanta",
"DERIVATIVE" : "Derivát",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "Střídání (Frekvence posouvání)",
"TRANSLATION (TIME SHIFTING)" : "Překlad (časový posun)",
"LAPLACE TRANSFORM PAIRS" : "Laplaceova transformace párů",

"Math Tricks" : "Matematiceké triky",
"1. ADDITION" : "1. Sčítání",
"2. SUBTRACTION" : "2. Odčítání",
"3. MULTIPLICATION" : "3. Násobení",
"4. DIVISION" : "4. Dělení",
"5. SQUARING" : "5. Srovnat",
"6. EXPONENTIATION" : "6. Umocňování",
"7. ROOTS" : "7. Kořeny",
"8. SUMMATIONS" : "8. Součet ",
"6. SUMMATIONS" : "6. Součet ",

"UNITS CONVERTER" : "Konvertor jednotek",
"ANGLE" : "Úhel",
"COMPUTER DATA" : "Počítač dat",
"FUEL CONSUMPTION" : "Spotřeba paliva",
"Click here to download Physics Formulas app" : "Zde si můžete stáhnout aplikaci fyzikální Vzorce",
"All Maths formulas for your work and study" : "Všechny matematické vzorce pro vaši práci a studiu",
"EDIT" : "Editovat",
"ADD" : "Přidat",
"TITLE OF THE PICTURE" : "Název obrázku",
"GAME" : "Hra",
"START" : "Start",
"LEVEL" : "Hladina",
"RETRY" : "Opakovat",
"SCORE" : "Skóre",
"TIME" : "Čas",
"BEST" : "Nejlepší",
"GAME OVER" : "Konec hry",
"YOUR SCORE IS: " : "Vaše skóre je:",
"Title of new tool" : "Název nového nástroje",
"Name of the variable" : "Název proměnné",
"Add more variable" : "Přidat další proměnné",
"Formula of the tool" : "Vzorec nástroje",
"Name of the result " : "Jméno výsledku",
"Add more result" : "Přidat další výsledek",

"Save favorite successful" : "Uložení do oblíbených proběhlo úspěšně",
"Save favorite failed" : "Uložení do oblíbených selhalo",
"Save tool successful" : "Uložení nástroje porběhlo úspěšně",
"Save tool failed" : "Uložení nástroje selhalo",
"Free version is limited with only 3 variables and 3 results. For more, please use Full version" : "Volná verze obsahuje jen tři proměnné. Pro více možností, prosím použijte plnou verzi",
"Are you sure deleting this?" : "Jste si jistý, že to chcete smazat?",
"Go to Full version" : "Přejít na plnou verzi",
"Delete invalid" : "Odstranění neplatné",
"EULER'S FORMULA" : "Eulerův vzorec",

"Metre (m)" : "Metr (m)",
"Kilometre (km)" : "Kilometr (km)",
"Centimetre (cm)" : "Centimetr (cm)",
"Millimetre (mm)" : "Milimetr (mm)",
"Micrometre | Mircon (μm)" : "Mikrometr | mikron (μm)",
"Nanometre (nm)" : "Nanometr (nm)",
"Inch (in)" : "Palec (in)",
"Foot (ft)" : "Stopa (ft)",
"Yard (yd)" : "Yard (yd)",
"Mile (mi)" : "Míle (mi)",
"Light Year" : "Světelný rok",
"Nautical Mile (NM)" : "Námořní míle (NM)",
"Angstrom (Å)" : "Angstrom (Å)",

"Square Mile (mi²)" : "Čtvereční míle (mi²)",
"Square Yard (yd²)" : "Čtvereční yard (yd²)",
"Square Foot (ft²)" : "Čtverečních stopa (ft²)",
"Square Inch (in²)" : "Čtvereční palec (in²)",
"Square Kilometre (km²)" : "Kilometr čtvereční (km²)",
"Hectare (ha)" : "Hektar (ha)",
"Acre" : "Akr",
"Square Metre (m²)" : "Metr čtvereční (m²)",
"Square Centimetre (cm²)" : "Centimetr čtvereční (cm²)",
"Square Millimetre (mm²)" : "Čtvercový milimetr (mm²)",

"Litre (l)" : "Litr (l)",
"Cubic Metre (m³)" : "Metr krychlový (m³)",
"Cubic Inch (in³)" : "Palec krychlový (in³)",
"Cubic Foot (ft³)" : "Stopa krychlová (ft³)",
"Cubic Yard (yd³)" : "Yard krychlový (yd³)",
"Gallon (US) (gal)" : "Galon (US) (gal)",
"Gallon (UK) (gal)" : "Galon (UK) (gal)",

"Gram (g)" : "Gram (g)",
"Kilogram (kg)" : "Kilogram (kg)",
"Tonne (t)" : "Tuna (t)",
"Milligram (mg)" : "Miligram (mg)",
"Microgram (μg)" : "Mikrogram (μg)",
"Ounce (oz)" : "Unce (oz)",
"Pound (lb)" : "Libra (lb)",
"Carat" : "Karát",
"Slug" : "Brok",
"Ounce (Troy) (ozt)" : "Unce (Troy) (ozt)",

"Year" : "Rok",
"Month" : "Měsíc",
"Week" : "Týden",
"Day" : "Den",
"Hour (h)" : "Hodina (h)",
"Minute (min)" : "Minuta (min)",
"Second (s)" : "Druhý (s)",
"Millisecond (ms)" : "Milisekunda (ms)",
"Microsecond (μs)" : "Mikrosekunda (μs)",
"Decade" : "Desetiletí",
"Century" : "Století",
"Millennium" : "Tisíciletí",

"Kilometre per Hour (km/h)" : "Kilometr za hodinu (km/h)",
"Miles per Hour (mi/h)" : "Míle za hodinu (mi/h)",
"Metre per Second (m/s)" : "Metr za sekundu (m/s)",
"Foot per Second (ft/s)" : "Stopa za sekundu (ft/s)",
"Knot" : "Uzel",
"Speed of Light (c)" : "Rychlost světla (c)",
"Minutes per Mile (min/mi)" : "Minut za míly (min/mi)",
"Minutes per Kilometre (min/km)" : "Minut za kilometru (min/km)",

"Celsius (°C)" : "Celsia (°C)",
"Fahrenheit (°F)" : "Fahrenheita (°F)",
"Kelvin (K)" : "Kelvina (K)",
"Rankine (°R)" : "Rankinův (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "Kilogram / krychlový metr (kg/m³)",
"Kilogram/Litre (kg/l)" : "Kilogram / litr (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "Brok/ krychlová stopa (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "Libra / krychlová stopa (lbm/ft³)",

"Newton (N)" : "Newton (N)",
"Kilonewton (kN)" : "Kilonewton (kN)",
"Atomic unit of force" : "Atomová jednotka síly",
"Gram Force (gf)" : "Gramová síla (gf)",
"Pound Force (lbf)" : "Librová síla (lbf)",
"Poundal (pdl)" : "Librový (pdl)",
"Dyne (dyn)" : "Dyn (dyn)",
"Kilogram-force | Kilopond (kgf)" : "Kilogram-síla | kilolibra (kgf)",

"Watt (W)" : "Watt (W)",
"Kilowatt (kW)" : "Kilowatt (kW)",
"Megawatt (MW)" : "Megawatt (MW)",
"Milliwatt (mW)" : "Miliwattů (mW)",
"Microwatt (µW)" : "Microwatt (µW)",
"Horsepower (hp)" : "Koňská síla (hp)",
"Calorie per Second (cal/s)" : "Kalorií za sekundu (cal/s)",

"Pascal (Pa)" : "Pascal (Pa)",
"Kilopascal (kPa)" : "Kilopascals (kPa)",
"Megapascal (MPa)" : "Megapascal (MPa)",
"Bar (bar)" : "Bar (bar)",
"Millibar (mbar)" : "Millibar (mbar)",
"Atmosphere (atm)" : "Atmosféra (atm)",
"Pound Force per Square Inch (psi)" : "Síla libry na čtvereční palec (psi)",
"Dyne per Square Centimetre" : "Dyn na čtvereční Centimetre",

"Degree (°)" : "Stupeň (°)",
"Radian (rad)" : "Radian (rad)",
"π Radian (π rad)" : "π Radian (π rad)",
"Second" : "Druhý",
"Minute (')" : "Minuta (')",

"Byte (B)" : "Byte (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "Kilobyte | kibibyte (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "Megabyte | mebibyte (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Gigabyte | gibibyte (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "Terabyte | tebibyte (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "Petabyte | pebibyte (PB | Pi)",
"Bit (b)" : "Bit (b)",
"Zebibyte (Zi)" : "Zebibyte (Zi)",

"Litres per 100 Kilometres (l/100km)" : "Litry na 100 kilometry (l/100km)",
"Kilometres per Litre (km/l)" : "Kilometry za litr (km/l)",
"Miles per Gallon (US) (mpg)" : "Míle na galon (US) (mpg)",
"Miles per Gallon (UK) (mpg)" : "Míle na galon (UK) (mpg)",


};