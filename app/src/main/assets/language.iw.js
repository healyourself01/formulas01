﻿var language={

"1. 2D SHAPES" : "1. צורות דו-מימדיות",
"2. 3D SHAPES" : "2.גופים תלת-מימדיים",
"TRIANGLE" : "משולש",
"RIGHT TRIANGLE" : "משולש ישר זווית",
"SQUARE" : "מרובע",
"RECTANGLE" : "מלבן",
"PARALLELOGRAM" : "מַקבִּילִית",
"LOZENGE" : "מעויין",
"TRAPEZOID" : "טרפז",
"CONVEX QUADRILATERAL" : "מרובע קמור",
"CIRCLE" : "מעגל",
"SEGMENT OF CIRCLE" : "קטע של מעגל (קשת)",
"SECTOR OF CIRCLE" : "גזרה של המעגל",
"REGULAR POLYGON OF N SIDES" : "מצולע משוכלל של N צלעות",
"REGULAR POLYGON" : "מצולע משוכלל",
"REGULAR POLYGON" : "מצולע משוכלל",
"HEXAGON" : "מְשׁוּשֶׁה",
"SPHERE" : "כדור",
"CYLINDER" : "גליל",
"CYLINDER" : "גליל",
"CONE" : "גביע",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "קטיעה של גביע",
"PYRAMID" : "פירמידה",
"SQUARE PYRAMID" : "פירמידה מרובעת",
"CUBOID" : "קוביה",
"CUBOID" : "קוביה",
"TRIANGULAR PRISM" : "מנסרה משולשת",
"A: Area" : "A: שטח",
"A: Area, P: Perimeter" : "A: שטח, P: היקף",
"b: Arc length" : "b: אורך קשת",
"V: Volume, A: Surface Area" : "V: כרך,:A שטח פנים",
"A: Lateral surface area" : "A: שטח פנים רוחבי",
"A: Lateral surface area" : "A: שטח פנים רוחבי",
"A: Base Area" : "A: שטח בסיס",
"A: Surface Area" : "A: שטח פנים",

"1. OPERATIONS ON EXPRESSIONS" : "1. תפקוד של ביטויים",
"POLYNOMIAL" : "פולינום",
"FRACTIONS" : "שברים",
"IDENTITY" : "זהות",
"EXPONENTIATION" : "אקספוננטה",
"ROOTS" : "שורשים",
"2. RATE FORMULAS" : "2. נוסחות גדילה ודעיכה",
"6. COMPLEX NUMBERS" : "6. מספרים מרוכבים",
"COMPLEX PLANE" : "מישור מרוכב",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. סדרות \n סדרות חשבוניות",
"4. PROGRESSION" : "4. סדרות", 
"ARITHMETIC PROGRESSION" : "סדרת חשבונית", 
"GEOMETRIC PROGRESSION" : "טור גיאומטרי", 
"SUMMATIONS" : "סכום",
"5. LOGARITHM" : "5. לוגריתם", 
"DECIMAL LOGARITHM" : "לוגריתם דצימלי",
"NATURAL LOGARITHM" : "לוגריתם טבעי",
"d: common difference" : "d: הפרש",
"q: common ratio" : "q: יחס ",
"3. INEQUALITIES" : "3. אי שוויון",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "פונקציות טריגונומטריות למשולש ישר זווית",
"TRIGONOMETRIC TABLE" : "טבלה טריגונומטרית",
"CO-RATIOS" : "יחסים הדדיים",
"BASIC FORMULAS" : "נוסחות בסיסיות",
"MULTIPLE ANGLE FORMULAS" : "נוסחות מרובות זוויות",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "כוחות של פונקציות טריגונומטריות",
"ADDITION FORMULAS" : "נוסחאות נוספות",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "סכום של פונקציות טריגונומטריות",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "תוצר של פונקציות טריגונומטריות",
"HALF ANGLE FORMULAS" : "נוסחאות של חצי זווית",
"ANGLES OF A PLANE TRIANGLE" : "זוויות של משולש שטוח",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "יחסים בין פונקציות טריגונומטריות",
"α, β, γ are 3 angles of a triangle" : "α, β, γ הן 3 זוויות של משולש",


"1. ALGEBRAIC EQUATIONS" : "1. משוואות אלגברית",
"LINEAR EQUATION" : "משוואה ליניארית",
"SYSTEM OF TWO LINEAR EQUATIONS" : "מערכת של שתי משוואות ליניארית",
"QUADRATIC EQUATION" : "משוואה ריבועית",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "משוואת מעריכית. מעריך ולוגריתם ",
"2. EXPONENT AND LOGARITHM" : "2. מעריך ולוגריתם",
"LOGARITHMIC EQUATION" : "משוואה לוגריתמית",
"3. TRIGONOMETRIC EQUATION" : "3. משוואה טריגונומטריות",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. אי שוויון \n אי-שוויון ליניארי",
"4. INEQUATIONS" : "4. אי שוויון",
"QUADRATIC INEQUATION" : "אי שוויוניים ריבועיים",
"EXPONENTIAL INEQUATION" : "אי שוויוניים מעריכיים",
"LOGARITHMIC INEQUATION" : "אי שוויוניים לוגריתמיים",
"TRIGONOMETRIC INEQUATIONS" : "אי-שוויוניים טריגונומטריים",
"LINEAR EQUATION" : "משוואה ליניארית",
"EXPONENTIAL EQUATION" : "משוואה מעריכית",
"LINEAR INEQUATION" : "אי שוויוניים ליניארים",
"CUBIC EQUATION" : "משוואה ממעלה שלישית",
"POINTS" : "נקודות",
"- Distance between two points A and B" : "-מרחק בין שתי נקודות A ו- B ",
"- Distance between point A and origin:" : "-מרחק בין נקודה ומקור",
"TRIANGLE" : "משולש",
"- Area of triangle with vertices at A, B, C" : "-שטח של משולש עם נקדות מפגש A, B, C",
"- Area of a triangle with a vertice at origin" : "- שטח משולש עם נקודת מפגש בראשית הצירים",
"EQUATION OF LINE" : "משוואה של קו",
"- Joining two points A, B" : "- חיבור של שתי נקודות A, B",
"- Passing point A and parallel with line y=ax+b" : "-עובר דרך נקודה A ומקביל לפונקציה y=ax+b",
"- Passing point A and perpendicular with line y=ax+b" : "-עובר דרך נקודה A ומאונך לפונקציה y=ax+b",
"EQUATION OF CIRCLE" : "משוואה של מעגל",
"- Circle with radius r and center at (a, b)" : "-מעגל עם רדיוס r  ומרכז ב (a,b)",
"- Circle with center at origin" : "- מעגל עם מרכז בראשית הצירים",
"ELLIPSE" : "אֶלִיפְּסָה",
"HYPERBOLA" : "הִיפֵּרבּוֹלָה",
"PARABOLA" : "פָּרַבּוֹלָה",

"Limit \n" : "\n גבול",
"Derivative \n" : "\n תנגזר",
"C: constant" : "C: קבוע",
"Differentiation \n" : "\n נגזרת",
"C: constant; u, v, w: functions of x" : "C: קבוע; u, v, w: פונקציות של x",
"LIMIT" : "גבול",
"DERIVATIVE" : "נגזרת",
"DIFFERENTIATION" : "נגזרת",

"1. INDEFINITE INTEGRALS" : "1. אינטגרל לא מסוים",
"C: arbitrary constant, k: constant" : "C: שרירותי קבוע, k: קבוע",
"INTEGRALS BY PARTIAL FRACTIONS" : "אינטגרלים עם פונקציות חלקיות",
"INTEGRALS INVOLVING ROOTS" : "אינטגרלים עם שורשים",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "אינטגרלים עם פונקציות טריגונומטריות",
"2. DEFINITE INTEGRALS" : "2. אינטגרל מסוים",
"APPLICATIONS" : "יישומים",
"- Surface area created by y=f(x)" : "-פני שטח שנוצרים על ידי y = f (x)",
"- Volume of a solid created by y=f(x) rotated around axis:" : "-נפח גוף סיבוב הנוצר על-ידי פונקציה y=f(x) שמסתובבת מסביב לציר",

"Median" : "חציון",
"Angle bisector" : "חוצת זווית",
"SPHERICAL CAP" : "כיפה",
"SPHERICAL SEGMENT" : "קטע כדורי",
"SPHERICAL SECTOR" : "חלק חרוטי",
"TORUS" : "טורוס",
"FORMULAS WITH t=tan(x/2)" : "נוסחות עם t = tan (x / 2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "צלעות וזוויות של משולש במישור",
"Law of sines, cosines and tangents" : "משפטי סינוסים, קוסינוסים וטנגנסים",

"1. PLANE ANALYTIC GEOMETRY" : "גיאומטריה אנליטית במישור.1 ",
"2. SOLID ANALYTIC GEOMETRY" : "2. גיאומטריה אנליטית בגופים",
"LINE" : "קו",
"- Direction Cosines of Line Joining Points A and B" : "-כיוון של קו קוסינוס המחבר בין הנקודות A ו- B",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "משוואה של קו המחבר שתי נקודות A, B",
"- In standard form" : "- בצורה סטנדרטית",
"- In parametric form" : "- בצורה פרמטרית",
"PLANE" : "מישור",
"- General equation of a plane" : "-משוואה כללית של מישור",
"- Equation of plane passing through point A, B, C" : "- משוואה של מישור העובר דרך הנקודות A, B, C",
"- Equation of plane in intercept form" : "- משוואה של מישור בצורה אלגברית",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a,b,c נמצאים על הצירים x,y,z בהתאמה",
"- Normal Form for Equation of Plane" : "-צורה רגילה של משוואת המישור",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p:מרחק מאונך מנקודה O לנקודה P שנמצאת על המישור;α, β, γ, זוויות בין OP וצירי x,y,z חיוביים",
"- Distance from point M to a plane" : "- מרחק מהנקודה M למישור",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "משוואה של מרכז כדור בנקודה M ו- R רדיוס בקואורדינטות מלבניות",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "משוואה של אליפסה עם המרכז M וצירים חוצים A,B,C",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "גליל אליפטי עם ציר Z בתור ציר",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "קונוס אליפטי עם ציר Z בתור ציר",
"HYPERBOLOID OF ONE SHEET" : "היפרבולואיד המורכב מחלק אחד",
"HYPERBOLOID OF TWO SHEETS" : "היפרבולואיד המורכב משני חלקים",
"ELLIPTIC PARABOLOID" : "פרדבלואיד אליפטי",
"HYPERBOLIC PARABOLOID" : "פרבלואיד היפרבולואי",

"TRANSFORMATIONS" : "סימטריה",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. אינטגרלים בלתי מוגבלים מיוחדים",
"Integrals involving ax+b" : "אינטגרלים הכוללים ax+b",
"Integrals involving ax+b and px+q" : "אינטגרלים הכוללים ax+b ו pz+q",
"Integrals involving x²+a²" : "אינטגרלים הכוללים x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "אינטגרלים הכוללים x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a²" : "אינטגרלים הכוללים x²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "אינטגרלים הכוללים ax²+bx+c",
"Integrals involving xⁿ+aⁿ" : "אינטגרלים הכוללים xⁿ+aⁿ",
"Integrals involving sin(ax)" : "אינטגרלים הכוללים סינוס sin(ax)",
"Integrals involving e^(ax)" : "אינטגרלים הכוללים e^(ax)",
"Integrals involving ln(x)" : "אינטגרלים הכוללים לאן ln(x)",

"TRANSPOSE OF A MATRIX" : "שירבוב של מטריצה",
"ADDITION AND SUBTRACTION OF MATRICES" : "חיבור וחיסור של מטריצות",
"MULTIPLICATION OF MATRICES" : "כפל של מטריצות",
"DETERMINANT OF MATRIX" : "חילוק של מטריצות",
"INVERSE OF MATRIX" : "ההופכי של מטריצה",
"EQUATION IN MATRIX FORM" : "משוואה בצורת מטריצה",
"PROPERTIES OF MATRIX CALCULATIONS" : "מאפיינים של חישובי מטריצות",
"LENGTH" : "אורך",
"AREA" : "אזור",
"VOLUME" : "נפח",
"MASS" : "מסה",
"SPEED" : "מהירות",
"TIME" : "זמן",
"TEMPERATURE" : "טמפרטורה",
"DENSITY" : "צפיפות",
"FORCE" : "כוח",
"ENERGY" : "אנרגיה",
"POWER" : "כוח",
"PRESSURE" : "לחץ",
"Do you know this?" : "האם אתה יודע את זה?",
"What is special about these calculations?" : "מה מיוחד בחישובים אלה?",
"Can you find the interesting trait of these calculations?" : "האם אתה יכול למצוא את התכונה המעניינת של החישובים האלה?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. גרף של פונקציות אחרות",
"Constant" : "קבוע",
"Absolute" : "מוחלט",
"Square Root" : "שורש ריבועי",
"Parabolic" : "פרבולואידי",
"Cubic" : "מעוקב",
"Reciprocal" : "גומלין",
"Sec" : "סקאנס",
"Cosec" : "קוסקאנס",
"6. FUNCTION TRANSFORMATIONS" : "6. שינויי פונקציה",
"Horizontal Shifting" : "הסטה אופקית",
"Vertical Shifting" : "הסטה אנכית",
"Reflection" : "השתקפות",
"Stretching" : "מתיחה",
"F, F1: Focus points (foci)" : "F, F1: נקודות פוקוס (מוקדים)",
"AF=p: Parameter of Parabola" : "AF=p: פרמטר של פרבולה",

"Probability & Statistics" : "הסתברות וסטטיסטיקה",
"1. SETS" : "1. קבוצות",
"SET" : "קבוצה",
"SUBSET" : "תת-קבוצה",
"INTERSECTION" : "חיתוך",
"UNION" : "איחוד",
"SYMMETRIC DIFFERENCE" : "הפרש סימטרי",
"RELATIVE COMPLEMENT OF A IN B" : "השלמה יחסית של A ב-B",
"ABSOLUTE COMPLEMENT" : "השלמה מוחלטת",
"OPERATIONS ON SETS" : "פעולות על קבוצות",
"2. COMBINATIONS AND PERMUTATIONS" : "2. שילובים וצירופים",
"COMBINATIONS" : "שילובים",
"PERMUTATIONS" : "צירופים",
"3. PROBABILITY" : "3. הסתברות",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) אם A ו-B מתקיימים רק ביחד (2) אם A ו-B מתקיימים בנפרד",
"4. STATISTICS" : "4. סטטיסטיקה",
"MEAN" : "ממוצע",
"MEDIAN" : "חציון",
"MODE" : "מצב",
"Numerical value that occurs the most number of times" : "ערך מספרי המתרחש מספר רב ביותר של פעמים",
"EXAMPLE" : "דוגמא",
"GEOMETRIC MEAN" : "ממוצע גיאומטרי",
"HARMONIC MEAN" : "ממוצע הרמוני",
"VARIANCE" : "משתנים",
"STANDARD DEVIATION" : "סטיית תקן",
"MEAN DEVIATION" : "אומר סטייה",
"ROOT MEAN SQUARE" : "שורש מתכוון מרובע",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "התפלגות נורמלית (הפצת גאוס)",
"f: density function, F: Distribution function, μ: mean" : "f: פונקצית צפיפות, F: פונקצית הפצה, μ:  ממוצע",
"EXPONENTIAL DISTRIBUTION" : "התפלגות מעריכית",
"POISSON DISTRIBUTION" : "התפלגות פואסון",
"UNIFORM DISTRIBUTION" : "התפלגות אחידה",
"Transforms" : "התמרות",
"1. FOURIER SERIES AND TRANSFORMS" : "1. סדרות פורייה והתפלגות",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f (x): 2מחזור, פונקציה מחזוריתL; a_n, b_n: מקדמי פורייה; c_n: מקדם פורייה מורכב",
"REAL FORM OF FOURIER SERIES" : "צורה אמיתית של סדרות פורייה",
"COMPLEX FORM" : "צורה מורכבת",
"PARSEVAL’S THEOREM" : "משפט פרסוול",
"FOURIER TRANSFORM" : "התמרת פורייה",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f (x): פונקציה של x, F (s): התמרת פורייה של f (x)",
"CONVOLUTION" : "פיתול",
"CORRELATION" : "מתאם",
"f*: complex conjugate of f" : "f*: המוצמד המרוכב של f",
"FOURIER SYMMETRY RELATIONSHIPS" : "יחסי סימטריה לפי פורייה",
"FOURIER TRANSFORM PAIRS" : "התמרת פורייה לזוגות",
"2. LAPLACE TRANSFORMS" : "2. התמרות לפלס",
"DEFINITION" : "הגדרה",
"CONVOLUTION" : "פיתול",
"INVERSE" : "הופכי",
"a: constant" : "a: קבוע",
"DERIVATIVE" : "נגזרת",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "חילוף (שיוני תדר)",
"TRANSLATION (TIME SHIFTING)" : "תרגום (שינוי זמן)",
"LAPLACE TRANSFORM PAIRS" : "התמרה לפלס לזוגות",

"Math Tricks" : "טריקים במתמטיקה",
"1. ADDITION" : "1. חיבור",
"2. SUBTRACTION" : "2. חיסור",
"3. MULTIPLICATION" : "3. כפל",
"4. DIVISION" : "4. חילוק",
"5. SQUARING" : "5. כפל בריבוע",
"6. EXPONENTIATION" : "6. אקספוננטה",
"7. ROOTS" : "7. שורשים",
"8. SUMMATIONS" : "8. סכום",
"6. SUMMATIONS" : "6. סכום",

"UNITS CONVERTER" : "ממיר יחידות",
"ANGLE" : "זווית",
"COMPUTER DATA" : "נתוני מחשב",
"FUEL CONSUMPTION" : "צריכת דלק",
"Click here to download Physics Formulas app" : "לחץ כאן על מנת להוריד אפליקציה עם נוסחאות בפיזיקה",
"All Maths formulas for your work and study" : "כל נוסחאות המתמטיקה לעבודה ולמחקר.",
"EDIT" : "עריכה",
"ADD" : "הוסף",
"TITLE OF THE PICTURE" : "כותרת התמונה",
"GAME" : "משחק",
"START" : "התחל",
"LEVEL" : "רמה",
"RETRY" : "נסה שוב",
"SCORE" : "ציון",
"TIME" : "זמן",
"BEST" : "התוצאה הטובה ביותר",
"GAME OVER" : "המשחק הסתיים",
"YOUR SCORE IS: " : "הציון שלך הוא:",
"Title of new tool" : "כותרת של כלי חדש",
"Name of the variable" : "שם המשתנה",
"Add more variable" : "הוסף עוד משתנה",
"Formula of the tool" : "נוסחת הכלי",
"Name of the result " : "שמה של התוצאה",
"Add more result" : "הוסף תוצאה",

"Save favorite successful" : "להציל 'מועדף' עברה בהצלחה",
"Save favorite failed" : "שמירת 'מועדף' נכשלה",
"Save tool successful" : "שמירת כלי עברה בהצלחה",
"Save tool failed" : "שמירת כלי נכשלה",
"Free version is limited with only 3 variables. For more, please use Full version" : "הגרסה החינמית מוגבלת לשלושה משתנים. על מנת לקבל עוד, אנא השתמשו בגרסה המלאה.",
"Are you sure deleting this?" : "האם אתה בטוח שאתה רוצה למחוק את זה?",
"Go to Full version" : "עבור לגרסה המלאה",
"Delete invalid" : "מחיקה לא חוקית",
"EULER'S FORMULA" : "נוסחת אוילר",



"Metre (m)" : "מטר (m)",
"Kilometre (km)" : "קילומטר (km)",
"Centimetre (cm)" : "סנטימטר (cm)",
"Millimetre (mm)" : "מילימטר (mm)",
"Micrometre | Mircon (μm)" : "מיקרומטר | מיקרון (μm)",
"Nanometre (nm)" : "ננומטר (nm)",
"Inch (in)" : "אינץ' (in)",
"Foot (ft)" : "רגל (ft)",
"Yard (yd)" : "יארד (yd)",
"Mile (mi)" : "מייל (mi)",
"Light Year" : "שנת אור",
"Nautical Mile (NM)" : "מייל ימי (NM)",
"Angstrom (Å)" : "אנגסטרום (Å)",

"Square Mile (mi²)" : "מייל רבוע (mi²)",
"Square Yard (yd²)" : "יארד רבוע (yd²)",
"Square Foot (ft²)" : "מטר רבוע (ft²)",
"Square Inch (in²)" : "אינץ' רבוע (in²)",
"Square Kilometre (km²)" : "קילומטר רבוע (km²)",
"Hectare (ha)" : "הקטאר (ha)",
"Acre" : "אקר",
"Square Metre (m²)" : "מטר רבוע (m²)",
"Square Centimetre (cm²)" : "סנטימטר רבוע (cm²)",
"Square Millimetre (mm²)" : "מילימטר רבוע (mm²)",

"Litre (l)" : "ליטר (l)",
"Cubic Metre (m³)" : "מטר מעוקב (m³)",
"Cubic Inch (in³)" : "אינץ' מעוקב (in³)",
"Cubic Foot (ft³)" : "רגל מעוקב (ft³)",
"Cubic Yard (yd³)" : "יארד מעוקב (yd³)",
"Gallon (US) (gal)" : "גלון (ארה'ב) (gal)",
"Gallon (UK) (gal)" : "גלון (בריטניה) (gal)",

"Gram (g)" : "גרם (g)",
"Kilogram (kg)" : "קילוגרם (kg)",
"Tonne (t)" : "טון (t)",
"Milligram (mg)" : "מִילִיגרָם (mg)",
"Microgram (μg)" : "מיקרוגרם (μg)",
"Ounce (oz)" : "אונקיה (oz)",
"Pound (lb)" : "פאונד (lb)",
"Carat" : "קרט",
"Slug" : "סלאג",
"Ounce (Troy) (ozt)" : "אונקיה (טרוינית) (ozt)",

"Year" : "שנה",
"Month" : "חודש",
"Week" : "שבוע",
"Day" : "יום",
"Hour (h)" : "שעה (h)",
"Minute (min)" : "דקה (min)",
"Second (s)" : "שנייה (s)",
"Millisecond (ms)" : "אלפית שנייה (ms)",
"Microsecond (μs)" : "מיקרו-שנייה (μs)",
"Decade" : "עשור",
"Century" : "מאה",
"Millennium" : "מילניום",

"Kilometre per Hour (km/h)" : "קילומטר לשעה (km/h)",
"Miles per Hour (mi/h)" : "מייל לשעה (mi/h)",
"Metre per Second (m/s)" : "מטר לשנייה (m/s)",
"Foot per Second (ft/s)" : "רגל לשנייה (ft/s)",
"Knot" : "קשר",
"Speed of Light (c)" : "מהירות האור (c)",
"Minutes per Mile (min/mi)" : "דקות למייל (min/mi)",
"Minutes per Kilometre (min/km)" : "דקות לקילומטר (min/km)",

"Celsius (°C)" : "צלזיוס (°C)",
"Fahrenheit (°F)" : "פרנהייט (°F)",
"Kelvin (K)" : "קלווין (K)",
"Rankine (°R)" : "רנקין (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "קילוגרם / מטר מעוקב (kg/m³)",
"Kilogram/Litre (kg/l)" : "קילוגרם / ליטר (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "סלאג / רגל מעוקב (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "פאונד / רגל מעוקב (lbm/ft³)",

"Newton (N)" : "ניוטון (N)",
"Kilonewton (kN)" : "קילו-ניוטון (kN)",
"Atomic unit of force" : "יחידה אטומית של כוח",
"Gram Force (gf)" : "גרם (כוח) (gf)",
"Pound Force (lbf)" : "פאונד (כוח) (lbf)",
"Poundal (pdl)" : "פאונדל (pdl)",
"Dyne (dyn)" : "דיין (מידה) (dyn)",
"Kilogram-force | Kilopond (kgf)" : "קילוגרם (כוח) | קילופונד (kgf)",

"Watt (W)" : "וואט (W)",
"Kilowatt (kW)" : "קִילוֹוָאט (kW)",
"Megawatt (MW)" : "מגה וואט (MW)",
"Milliwatt (mW)" : "מיליוואט (mW)",
"Microwatt (µW)" : "מיקרו-וואט (µW)",
"Horsepower (hp)" : "כוחות סוס (hp)",
"Calorie per Second (cal/s)" : "קלוריות לשנייה (cal/s)",

"Pascal (Pa)" : "פסקל (Pa)",
"Kilopascal (kPa)" : "קילו-פסקל (kPa)",
"Megapascal (MPa)" : "מגה פסקל (MPa)",
"Bar (bar)" : "בר (מידה) (bar)",
"Millibar (mbar)" : "מיליבר (mbar)",
"Atmosphere (atm)" : "לחץ אטמוספירי ",
"Pound Force per Square Inch (psi)" : "פאונד (כוח) לאינץ' רבוע (psi)",
"Dyne per Square Centimetre" : "דיין לסנטימטר רבוע",

"Degree (°)" : "זווית (°)",
"Radian (rad)" : "רדיאן (rad)",
"π Radian (π rad)" : "π רדיאן (π rad)",
"Second" : "שנייה",
"Minute (')" : "דקה (')",

"Byte (B)" : "בייט (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "קילובייט | קיביבייט (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "מגה | מביבייט (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "ג'יגה בייט | גיביבייט (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "טרה-בייט | טביבייט (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "פטה-בייט | פביבייט (PB | Pi)",
"Bit (b)" : "ביט (b)",
"Zebibyte (Zi)" : "זביבייט (Zi)",

"Litres per 100 Kilometres (l/100km)" : "ליטרים לכל 100 קילומטרים (l/100km)",
"Kilometres per Litre (km/l)" : "קילומטרים לליטר (km/l)",
"Miles per Gallon (US) (mpg)" : "מייל לגלון (ארה'ב) (mpg)",
"Miles per Gallon (UK) (mpg)" : "מייל לגלון (בריטניה) (mpg)",


};