﻿var language={

"1. 2D SHAPES" : "1 . الأشكال ثنائية الأبعاد",
"2. 3D SHAPES" : "2 . الأشكال ثلاثية الأبعاد",
"TRIANGLE" : "المثلث",
"RIGHT TRIANGLE" : "المثلث الحقيقي ",
"SQUARE" : "المربع",
"RECTANGLE" : "المستطيل",
"PARALLELOGRAM" : "متوازي الاضلاع",
"LOZENGE" : "المعين",
"TRAPEZOID" : "شبه منحرف",
"CONVEX QUADRILATERAL" : "الرباعي المحدب",
"CIRCLE" : "الدائرة",
"SEGMENT OF CIRCLE" : "جزء من الدائرة",
"SECTOR OF CIRCLE" : "قطر دائرة",
"REGULAR POLYGON OF N SIDES" : "المضلع المنتظم من الجانبين ",
"REGULAR POLYGON" : "المضلع منتظم",
"REGULAR POLYGON" : "المضلع منتظم",
"HEXAGON" : "المسدس",
"SPHERE" : "الكرة",
"CYLINDER" : "الأسطوانة",
"CYLINDER" : "الأسطوانة",
"CONE" : "المخروط",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "المخروط الناقص ",
"PYRAMID" : "الهرم",
"SQUARE PYRAMID" : "هرم الرباعي",
"CUBOID" : "المكعب",
"CUBOID" : "المكعب",
"TRIANGULAR PRISM" : "المنشور الثلاثي",
"A: Area" : "A : المنطقة",
"A: Area, P: Perimeter" : "A : المساحة ، P : المحيط",
"b: Arc length" : "b : طول القوس",
"V: Volume, A: Surface Area" : "V : المجلد، A: المساحة بالمتر المربع",
"A: Lateral surface area" : "A: مساحة السطح الجانبي",
"A: Lateral surface area" : "A: مساحة السطح الجانبي",
"A: Base Area" : "A : قاعدة المنطقة",
"A: Surface Area" : "A : المساحة بالمتر المربع",

"1. OPERATIONS ON EXPRESSIONS" : "1 . العمليات على التعابير",
"POLYNOMIAL" : "متعدد الحدود",
"FRACTIONS" : "الكسور",
"IDENTITY" : "مطابقة رياضية",
"EXPONENTIATION" : "المعادلات الأسية",
"ROOTS" : "الجذور",
"2. RATE FORMULAS" : "2 . العمليات المعدلية",
"6. COMPLEX NUMBERS" : "6 . الأعداد المركبة",
"COMPLEX PLANE" : "المخططات المعقدة",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4. التقدم \n الحسابي التقدمي",
"4. PROGRESSION" : "4 . التقدم", 
"ARITHMETIC PROGRESSION" : "المتتاليات العددية", 
"GEOMETRIC PROGRESSION" : "المتتاليات الهندسية", 
"SUMMATIONS" : "المجموع",
"5. LOGARITHM" : "5 . اللوغارثم في الرياضيات", 
"DECIMAL LOGARITHM" : "اللوغاريتم العشري",
"NATURAL LOGARITHM" : "اللوغاريتم الطبيعي",
"d: common difference" : "d : الفرق المشتركة",
"q: common ratio" : "q : النسبة مشتركة",
"3. INEQUALITIES" : "3. عدم المساواة",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "الدوال المثلثية للمثلث القائم ",
"TRIGONOMETRIC TABLE" : "جدول المثلثات",
"CO-RATIOS" : " نسبة المشترك",
"BASIC FORMULAS" : "العمليات الأساسية",
"MULTIPLE ANGLE FORMULAS" : "العملبات الخاصة بالزوايا",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "صلاحيات الدوال المثلثية",
"ADDITION FORMULAS" : "بالإضافة إلى العمليات",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "مجموع الدوال المثلثية",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "النتيجة من الدوال المثلثية",
"HALF ANGLE FORMULAS" : "العمليات المنصفة للزواية",
"ANGLES OF A PLANE TRIANGLE" : "زوايا مثلث المخطط",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "العلاقات بين الدوال المثلثية",
"α, β, γ are 3 angles of a triangle" : "α ، β ، γ هي 3 زوايا مثلث",


"1. ALGEBRAIC EQUATIONS" : "1 . المعادلات الجبرية",
"LINEAR EQUATION" : "المعادلة خطية",
"SYSTEM OF TWO LINEAR EQUATIONS" : "نظام المعادلات الخطية من الدرجة الثانية",
"QUADRATIC EQUATION" : "المعادلة من الدرجة الثانية",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. الأس واللوغاريتم / المعادلة الأسية ",
"2. EXPONENT AND LOGARITHM" : "2. الأس واللوغاريتم ",
"LOGARITHMIC EQUATION" : "المعادلة اللوغاريتمية ",
"3. TRIGONOMETRIC EQUATION" : "3. المعادلة المثلثية ",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. التفاوت \ الخطي و لامعادلة",
"4. INEQUATIONS" : "4. لامعادلة",
"QUADRATIC INEQUATION" : "لامعادلة التربيعية",
"EXPONENTIAL INEQUATION" : "لامعادلة الأسي",
"LOGARITHMIC INEQUATION" : "لامعادلة لوغاريتمي",
"TRIGONOMETRIC INEQUATIONS" : "لامعادلة المثلثية",
"LINEAR EQUATION" : "المعادلة خطية",
"EXPONENTIAL EQUATION" : "المعادلة الأسية",
"LINEAR INEQUATION" : "لامعادلة الخطية",
"CUBIC EQUATION" : "المعادلة التكعيبية",
"POINTS" : "النقاط",
"- Distance between two points A and B" : "- المسافة بين النقطتين A و B",
"- Distance between point A and origin:" : "- المسافة بين النقطة A والأصل :",
"TRIANGLE" : "المثلث",
"- Area of triangle with vertices at A, B, C" : "- مساحة مثلث مع القمم  A ، B ، C",
"- Area of a triangle with a vertice at origin" : "- مساحة مثلث من الإرتفاع إلى القاعدة",
"EQUATION OF LINE" : "المعادلة الخطية",
"- Joining two points A, B" : "-الجمع بين نقطتين A, B",
"- Passing point A and parallel with line y=ax+b" : "- اجتياز النقطة (أ) و بالتوازي مع خط y=ax+b ",
"- Passing point A and perpendicular with line y=ax+b" : "- عبور نقطة بالتعامد مع y=ax+b",
"EQUATION OF CIRCLE" : "معادلة الدائرة",
"- Circle with radius r and center at (a, b)" : "- دائرة نصف قطرها r و مركز في (a،b)",
"- Circle with center at origin" : "- القطر مع المركز في الأصل",
"ELLIPSE" : "القطع الناقص",
"HYPERBOLA" : "القطع الزائد",
"PARABOLA" : "القطع الناقص هندسة",

"Limit \n" : "النهاية ",
"Derivative \n" : "المشتقة",
"C: constant" : "C: ثابت",
"Differentiation \n" : "التمايز",
"C: constant; u, v, w: functions of x" : "C: ثابت ؛ u, v, w : مجاهيل x",
"LIMIT" : "االنهاية",
"DERIVATIVE" : "المشتقة",
"DIFFERENTIATION" : "التفاضل",

"1. INDEFINITE INTEGRALS" : "1 . التكاملات إلى أجل غير مسمى",
"C: arbitrary constant, k: constant" : "C: الثابت التعسفي ، k : ثابت",
"INTEGRALS BY PARTIAL FRACTIONS" : "التكاملات بواسطة الكسور الجزئية",
"INTEGRALS INVOLVING ROOTS" : "التكاملات التي تنطوي على جذور",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "التكاملات التي تنطوي على الدوال المثلثية",
"2. DEFINITE INTEGRALS" : "2 . تكاملات المحددة",
"APPLICATIONS" : "التطبيقات",
"- Surface area created by y=f(x)" : "- المساحة التي أنشأتها y=f(x) ",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- استدارة الحجم من المواد الصلبة التي أنشأتها y=f(x) حول محور :",

"Median" : "متوسط",
"Angle bisector" : "زاوية المنصف",
"SPHERICAL CAP" : "كروية كاب",
"SPHERICAL SEGMENT" : "الجزء الكروية",
"SPHERICAL SECTOR" : "القطاع الكروية",
"TORUS" : "مستدير",
"FORMULAS WITH t=tan(x/2)" : "العمليات على t=tan(x/2)",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "الجانبين و زوايا مثلث ",
"Law of sines, cosines and tangents" : "قانون الجيوب ، الجب التمام و التظل",

"1. PLANE ANALYTIC GEOMETRY" : "1 . الهندسة التحليلية المسطحة",
"2. SOLID ANALYTIC GEOMETRY" : "2 . الهندسة التحليلية الثلاثية الأبعاد",
"LINE" : "خط",
"- Direction Cosines of Line Joining Points A and B" : "- جيب التمام اتجاه الخط والتقاطع مع النقاط A و B",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "معادلة الخط الواصل بين النقاط A، B",
"- In standard form" : "- في النموذج القياسي",
"- In parametric form" : "- في شكل حدودي",
"PLANE" : "مسطح",
"- General equation of a plane" : "- المعادلة العامة للمسطحات",
"- Equation of plane passing through point A, B, C" : "- معادلة المستوي الدي يمر عبر النقاط A ، B ، C",
"- Equation of plane in intercept form" : "- معادلة المستويات المتقاطعة",
"a,b,c are the intercept on the x,y,z axes, respectively" : "a b c هي متقاطعة مع x y z في المحاور ، على التوالي",
"- Normal Form for Equation of Plane" : "- نموذج عادي لمعادلة المستوي",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "ع : مسافة عمودية من المستوي O إلى المستوي P ؛ α ، β ، γ : الزوايا بين OP و x  y z هم لمحاور ",
"- Distance from point M to a plane" : "- المسافة من نقطة M إلى المستوي",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "معادلة الكرة من مركز دائرة نصف قطرها R و M في الإحداثيات المستطيلة",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "معادلة الإهليلجي مع مركز M وشبه محاور a b c",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "اسطوانة بيضاوي الشكل مع المحور  z",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "مخروط بيضاوي الشكل مع المحور  z",
"HYPERBOLOID OF ONE SHEET" : "سطح زائد من ورقة واحدة",
"HYPERBOLOID OF TWO SHEETS" : "سطح زائد من ورقتين",
"ELLIPTIC PARABOLOID" : "قطع إهليلجي",
"HYPERBOLIC PARABOLOID" : "قطع القطعي",

"TRANSFORMATIONS" : "التحولات",
"3. SPECIAL INDEFINITE INTEGRALS" : "3 . تكاملات إلى أجل غير مسمى ",
"Integrals involving ax+b" : "التكاملات التي تنطوي على ax+b",
"Integrals involving ax+b and px+q" : "التكاملات الت تنطوي على ax+b و px+q",
"Integrals involving x²+a²" : "التكاملات التي تنطوي على x²+a²",
"Integrals involving x²-a², x²&gt;a²" : "التكاملات التي تنطوي على x²-a², x²&gt;a²",
"Integrals involving x²-a², x²&lt;a²" : "التكاملات التي تنطوي على x²-a², x²&lt;a²",
"Integrals involving ax²+bx+c" : "التكاملات التي تنطوي على ax²+bx+c",
"Integrals involving xⁿ+aⁿ" : "التكاملات التي تنطوي على xⁿ+aⁿ",
"Integrals involving sin(ax)" : "التكاملات التي تنطوي على sin(ax)",
"Integrals involving e^(ax)" : "التكاملات تنطوي e^(ax)",
"Integrals involving ln(x)" : "التكاملات التي تنطوي على ln(x)",

"TRANSPOSE OF A MATRIX" : "تبديل للمصفوفة",
"ADDITION AND SUBTRACTION OF MATRICES" : "الجمع والطرح في المصفوفات",
"MULTIPLICATION OF MATRICES" : "ضرب المصفوفات",
"DETERMINANT OF MATRIX" : "العوامل الحيادية في المصفوفة",
"INVERSE OF MATRIX" : "معكوس مصفوفة",
"EQUATION IN MATRIX FORM" : "المعادلة في شكل مصفوفة",
"PROPERTIES OF MATRIX CALCULATIONS" : "خصائص حسابات مصفوفة",
"LENGTH" : "طول",
"AREA" : "منطقة",
"VOLUME" : "حجم",
"MASS" : "كتلة",
"SPEED" : "السرعة",
"TIME" : "وقت",
"TEMPERATURE" : "درجة الحرارة",
"DENSITY" : "كثافة",
"FORCE" : "قوة",
"ENERGY" : "طاقة",
"POWER" : "طاقة",
"PRESSURE" : "الضغط",
"Do you know this?" : "هل تعرف هذا؟",
"What is special about these calculations?" : "ما هو الشيء المميز في هاته الحسابات ؟",
"Can you find the interesting trait of these calculations?" : "هل بمكن العثور على شيء مثير للاهتمام من هذه الحسابات ؟",
"5. GRAPH OF OTHER FUNCTIONS" : "5 .الرسم الياني للدوال الأخرى",
"Constant" : "ثابت",
"Absolute" : "قيمة مطلقة",
"Square Root" : "جذر تربيعي ",
"Parabolic" : "قطع مكافئ",
"Cubic" : "مكعب ",
"Reciprocal" : "المقلوب العددي",
"Sec" : "ظتا (Sec)",
"Cosec" : "قا (Cosec)",
"6. FUNCTION TRANSFORMATIONS" : "6 . تحويل الدوال",
"Horizontal Shifting" : "التحول الأفقي ",
"Vertical Shifting" : "التحول العمودي ",
"Reflection" : "انعكاس",
"Stretching" : "تمديد",
"F, F1: Focus points (foci)" : "F, F1: نقطة التركيز ( بؤر )",
"AF=p: Parameter of Parabola" : "AF=p: خصائص القطع المكافئ",

"Probability & Statistics" : "الاحتمالات و الإحصاء",
"1. SETS" : "1. المجموعات",
"SET" : "مجموعة",
"SUBSET" : "مجموعة جزئية",
"INTERSECTION" : "التقاطع",
"UNION" : "الاتحاد",
"SYMMETRIC DIFFERENCE" : "الفرق المتماثل",
"RELATIVE COMPLEMENT OF A IN B" : "الفرق",
"ABSOLUTE COMPLEMENT" : "مجموعة مكملة",
"OPERATIONS ON SETS" : "عمليات المجموعات",
"2. COMBINATIONS AND PERMUTATIONS" : "2. التوافيق و التباديل",
"COMBINATIONS" : "توفيق",
"PERMUTATIONS" : "تبديل",
"3. PROBABILITY" : "3. احتمال",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) إذا كان A و B لا يجتمعان، (2) إذا كان A و B مستقلان ",
"4. STATISTICS" : "4. إحصاء",
"MEAN" : "المتوسط",
"MEDIAN" : "متوسط",
"MODE" : "منوال",
"Numerical value that occurs the most number of times" : "المنوال  هو القيمة الأكثر تكراراً في مجموعة من البيانات.",
"EXAMPLE" : "مثال ",
"GEOMETRIC MEAN" : "متوسط هندسي",
"HARMONIC MEAN" : "متوسط توافقي",
"VARIANCE" : "تباين",
"STANDARD DEVIATION" : "انحراف معياري",
"MEAN DEVIATION" : "متوسط الانحراف",
"ROOT MEAN SQUARE" : "جذر متوسط ​​مربع ",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "التوزيع الطبيعي (توزيع جاوس) ",
"f: density function, F: Distribution function, μ: mean" : "f: دالة الكثافة، F: دالة التوزيع، μ: متوسط ",
"EXPONENTIAL DISTRIBUTION" : "التوزيع الأسي ",
"POISSON DISTRIBUTION" : "توزيع بواسون ",
"UNIFORM DISTRIBUTION" : "توزيع موحد ",
"Transforms" : "التحويلات ",
"1. FOURIER SERIES AND TRANSFORMS" : "1.متسلسلة و تحويل فورييه",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x) : دالة دورية، 2L الفترة؛ a_n، b_n: معاملات فورييه؛ c_n: معامل فورييه التخيلي",
"REAL FORM OF FOURIER SERIES" : "الصيغة الحقيقية لمتسلسلة فورييه",
"COMPLEX FORM" : "الصيغة المركبة لمتسلسلة فورييه",
"PARSEVAL’S THEOREM" : "نظرية بيرسفال",
"FOURIER TRANSFORM" : "تحويل فورييه",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x):دالةx، F(s): تحويل فورييه f(x)",
"CONVOLUTION" : "التفاف ",
"CORRELATION" : "ارتباط ",
"f*: complex conjugate of f" : "f* :المرافق المركب f",
"FOURIER SYMMETRY RELATIONSHIPS" : "علاقات التناظر في تحويل فورييه ",
"FOURIER TRANSFORM PAIRS" : "بعض الدوال ومقابلها في تحويل فورييه",
"2. LAPLACE TRANSFORMS" : "2.تحويل لابلاس",
"DEFINITION" : "تعريف ",
"CONVOLUTION" : "التفاف ",
"INVERSE" : " العكسي",
"a: constant" : "a: ثابت ",
"DERIVATIVE" : "المشتقة",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "إزاحة التردد",
"TRANSLATION (TIME SHIFTING)" : "إزاحة الزمن",
"LAPLACE TRANSFORM PAIRS" : "بعض الدوال ومقابلها في تحويل لابلاس",

"Math Tricks" : "الحساب الذهني",
"1. ADDITION" : "1. الجمع",
"2. SUBTRACTION" : "2. الطرح ",
"3. MULTIPLICATION" : "3. الضرب",
"4. DIVISION" : "4. قسمة",
"5. SQUARING" : "5. مربع عدد",
"6. EXPONENTIATION" : "6. المعادلات الأسية",
"7. ROOTS" : "7. الجذور",
"8. SUMMATIONS" : "8. المجموع",
"6. SUMMATIONS" : "6. المجموع",

"UNITS CONVERTER" : "تحويل الوحدات ",
"ANGLE" : "زاوية",
"COMPUTER DATA" : "بيانات الكمبيوتر ",
"FUEL CONSUMPTION" : "استهلاك الوقود ",
"Click here to download Physics Formulas app" : "انقر هنا لتحميل تطبيق الصيغ الفيزيائية",
"All Maths formulas for your work and study" : "جميع الصيغ الرياضية لعملك والدراسة ",
"EDIT" : "تحرير ",
"ADD" : "إضافة ",
"TITLE OF THE PICTURE" : "عنوان الصورة ",
"GAME" : "اللعبة ",
"START" : "بداية ",
"LEVEL" : "مستوى ",
"RETRY" : "أعد المحاولة ",
"SCORE" : "النتيجة ",
"TIME" : "وقت",
"BEST" : "أفضل نتيجة",
"GAME OVER" : "انتهت اللعبة",
"YOUR SCORE IS: " : "النتيجة هي ",


"Metre (m)" : "متر  (m)",
"Kilometre (km)" : "الكيلومتر  (km)",
"Centimetre (cm)" : "سنتيمتر  (cm)",
"Millimetre (mm)" : "الملليمتر  (mm)",
"Micrometre | Mircon (μm)" : "ميكرومتر   (μm)",
"Nanometre (nm)" : "نانومتر (nm)",
"Inch (in)" : "بوصة  (in)",
"Foot (ft)" : "قدم  (ft)",
"Yard (yd)" : "ياردة (yd)",
"Mile (mi)" : "ميل  (mi)",
"Light Year" : "ضوء السنة ",
"Nautical Mile (NM)" : "ميل بحري  (NM)",
"Angstrom (Å)" : "انجستروم  (Å)",

"Square Mile (mi²)" : "ميل مربع (mi²)",
"Square Yard (yd²)" : "ياردة مربعة (yd²)",
"Square Foot (ft²)" : "قدم مربع  (ft²)",
"Square Inch (in²)" : "بوصة مربعة (in²)",
"Square Kilometre (km²)" : "كيلومتر مربع (km²)",
"Hectare (ha)" : "هكتار  (ha)",
"Acre" : "فدان ",
"Square Metre (m²)" : " متر مربع (m²)",
"Square Centimetre (cm²)" : "سنتيمتر مربع (cm²)",
"Square Millimetre (mm²)" : "ميليمتر مربع (mm²)",

"Litre (l)" : "لتر  (l)",
"Cubic Metre (m³)" : "متر مكعب  (m³)",
"Cubic Inch (in³)" : "بوصة مكعبة  (in³)",
"Cubic Foot (ft³)" : "قدم مكعب  (ft³)",
"Cubic Yard (yd³)" : "يارد مكعب  (yd³)",
"Gallon (US) (gal)" : "غالون (US)  (gal)",
"Gallon (UK) (gal)" : "غالون (المملكة المتحدة)  (gal)",

"Gram (g)" : "غرام  (g)",
"Kilogram (kg)" : "كيلوغرام  (kg)",
"Tonne (t)" : "طن  (t)",
"Milligram (mg)" : "مليغرام  (mg)",
"Microgram (μg)" : "ميكروجرام  (μg)",
"Ounce (oz)" : "أونصة  (oz)",
"Pound (lb)" : "الجنيه  (lb)",
"Carat" : "قيراط ",
"Slug" : "سبيكة ",
"Ounce (Troy) (ozt)" : "أونصة (تروي)  (ozt)",

"Year" : "العام ",
"Month" : "الشهر ",
"Week" : "الأسبوع ",
"Day" : "اليوم ",
"Hour (h)" : "ساعة  (h)",
"Minute (min)" : "دقيقة  (min)",
"Second (s)" : "ثانية  (s)",
"Millisecond (ms)" : "ميلي ثانية   (ms)",
"Microsecond (μs)" : "ميكروثانية  (μs)",
"Decade" : "العقد ",
"Century" : "القرن ",
"Millennium" : "الألفية ",

"Kilometre per Hour (km/h)" : "كيلومتر في ساعة  (km/h)",
"Miles per Hour (mi/h)" : "ميل في الساعة  (mi/h)",
"Metre per Second (m/s)" : "متر في الثانية  (m/s)",
"Foot per Second (ft/s)" : "قدم في الثانية  (ft/s)",
"Knot" : "عقدة ",
"Speed of Light (c)" : "سرعة الضوء  (c)",
"Minutes per Mile (min/mi)" : "دقيقة لكل ميل  (min/mi)",
"Minutes per Kilometre (min/km)" : "دقيقة لكل كيلومتر  (min/km)",

"Celsius (°C)" : "مئوية  (°C)",
"Fahrenheit (°F)" : "فهرنهايت  (°F)",
"Kelvin (K)" : "كلفن  (K)",
"Rankine (°R)" : "رانكين  (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "كيلوغرام / متر مكعب  (kg/m³)",
"Kilogram/Litre (kg/l)" : "كيلوغرام / لتر  (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "سبيكة (وحدة ) / قدم مكعب  (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "باوند / قدم مكعب  (lbm/ft³)",

"Newton (N)" : "نيوتن  (N)",
"Kilonewton (kN)" : "كيلو نيوتن  (kN)",
"Atomic unit of force" : "وحدة القوة الذرية",
"Gram Force (gf)" : "قوة غرام  (gf)",
"Pound Force (lbf)" : "باوند (قوة ) (lbf)",
"Poundal (pdl)" : "باوندال  (pdl)",
"Dyne (dyn)" : "داين  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "كيلوجرام-قوة | كيلوباوند (kgf)",

"Watt (W)" : "واط  (W)",
"Kilowatt (kW)" : "كيلووات  (kW)",
"Megawatt (MW)" : "ميجاوات  (MW)",
"Milliwatt (mW)" : "ميلي واط  (mW)",
"Microwatt (µW)" : "ميكروواط  (µW)",
"Horsepower (hp)" : "حصان (hp)",
"Calorie per Second (cal/s)" : "السعرات الحرارية في الثانية  (cal/s)",

"Pascal (Pa)" : "باسكال  (Pa)",
"Kilopascal (kPa)" : "كيلو باسكال (kPa)",
"Megapascal (MPa)" : "ميجا بسكال (MPa)",
"Bar (bar)" : "شريط  (bar)",
"Millibar (mbar)" : "ميلي بار  (mbar)",
"Atmosphere (atm)" : "الغلاف الجوي ",
"Pound Force per Square Inch (psi)" : " باوند (القوة ) في البوصة المربعة (psi)",
"Dyne per Square Centimetre" : "داين لكل سنتيمتر مربع ",

"Degree (°)" : "درجة  (°)",
"Radian (rad)" : "راديان  (rad)",
"π Radian (π rad)" : "(ط) راديان  (π rad)",
"Second" : "ثانية ",
"Minute (')" : "دقيقة  (')",

"Byte (B)" : "بايت  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "كيلوبايت | Kibibyte  (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "ميغا بايت | Mebibyte  (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "Gibibyte | جيجابايت  (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "تيرابايت | Tebibyte  (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "بيتابايت | Pebibyte  (PB | Pi)",
"Bit (b)" : "بت | Bit (b)",
"Zebibyte (Zi)" : "زيبي بايت |Zebibyte (Zi)",

"Litres per 100 Kilometres (l/100km)" : "لتر لكل 100 كيلومتر  (l/100km)",
"Kilometres per Litre (km/l)" : "كيلومترات لكل لتر  (km/l)",
"Miles per Gallon (US) (mpg)" : "ميل لكل جالون (US)  (mpg)",
"Miles per Gallon (UK) (mpg)" : "ميل لكل جالون (المملكة المتحدة) (mpg)",


"Title of new tool" : "عنوان أداة جديدة",
"Name of the variable" : "اسم المتغير",
"Add more variable" : "إضافة متغير",
"Formula of the tool" : "صيغة الأداة",
"Name of the result " : "اسم نتيجة",
"Add more result" : "إضافة نتيجة",
"EULER'S FORMULA" : "صيغة أويلر",


};