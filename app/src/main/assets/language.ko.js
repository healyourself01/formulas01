﻿var language={

"1. 2D SHAPES" : "1. 2D 도형",
"2. 3D SHAPES" : "2. 3D 도형",
"TRIANGLE" : "삼각형",
"RIGHT TRIANGLE" : "직각삼각형",
"SQUARE" : "정사각형",
"RECTANGLE" : "직사각형",
"PARALLELOGRAM" : "평행사변형",
"LOZENGE" : "마름모",
"TRAPEZOID" : "사다리꼴",
"CONVEX QUADRILATERAL" : "볼록사각형",
"CIRCLE" : "원",
"SEGMENT OF CIRCLE" : "활꼴",
"SECTOR OF CIRCLE" : "부채꼴",
"REGULAR POLYGON OF N SIDES" : "정다각형의N변",
"REGULAR POLYGON" : "정다각형",
"REGULAR POLYGON" : "정다각형",
"HEXAGON" : "육각형",
"SPHERE" : "구",
"CYLINDER" : "원기둥",
"CYLINDER" : "원기둥",
"CONE" : "원뿔",
"FRUSTUM OF RIGHT CIRCULAR CONE" : "직원뿔의 절두체",
"PYRAMID" : "피라미드",
"SQUARE PYRAMID" : "사각뿔",
"CUBOID" : "직육면체",
"CUBOID" : "직육면체",
"TRIANGULAR PRISM" : "삼각기둥",
"A: Area" : "A: 넓이",
"A: Area, P: Perimeter" : "A: 넓이, P: 둘레",
"b: Arc length" : "b: 호의 길이",
"V: Volume, A: Surface Area" : "V: 부피, A: 겉넓이",
"A: Lateral surface area" : "A: 옆면 넓이",
"A: Lateral surface area" : "A: 옆면 넓이",
"A: Base Area" : "A: 밑면넓이",
"A: Surface Area" : "A: 표면적",

"1. OPERATIONS ON EXPRESSIONS" : "1.식의 연산",
"POLYNOMIAL" : "다항식",
"FRACTIONS" : "분수",
"IDENTITY" : "항등식",
"EXPONENTIATION" : "지수",
"ROOTS" : "뿌리",
"2. RATE FORMULAS" : "2. 비율공식",
"6. COMPLEX NUMBERS" : "6. 복소수",
"COMPLEX PLANE" : "복소평면",
"4. PROGRESSION \NARITHMETIC  PROGRESSION" : "4.수열 \n 등차수열",
"4. PROGRESSION" : "4.수열", 
"ARITHMETIC PROGRESSION" : "등차수열", 
"GEOMETRIC PROGRESSION" : "등비수열", 
"SUMMATIONS" : "합계함",
"5. LOGARITHM" : "5.로그", 
"DECIMAL LOGARITHM" : "상용로그",
"NATURAL LOGARITHM" : "자연로그",
"d: common difference" : "d: 공차",
"q: common ratio" : "q: 공비",
"3. INEQUALITIES" : "3. 부등식",

"TRIGONOMETRIC FUNCTIONS FOR A RIGHT TRIANGLE" : "정삼각형의 삼각함수",
"TRIGONOMETRIC TABLE" : "삼각법 테이블",
"CO-RATIOS" : "공동 비율",
"BASIC FORMULAS" : "기본 공식",
"MULTIPLE ANGLE FORMULAS" : "여러 각도 공식",
"POWERS OF TRIGONOMETRIC FUNCTIONS" : "삼각 함수의 제곱",
"ADDITION FORMULAS" : "덧셈 공식",
"SUM OF TRIGONOMETRIC FUNCTIONS" : "삼각 함수의 합",
"PRODUCT OF TRIGONOMETRIC FUNCTIONS" : "삼각 함수의 제품",
"HALF ANGLE FORMULAS" : "반각 공식",
"ANGLES OF A PLANE TRIANGLE" : "평면 삼각형의 각도",
"RELATIONSHIPS AMONG TRIGONOMETRIC FUNCTIONS" : "삼각 함수 간의 관계",
"α, β, γ are 3 angles of a triangle" : "α, β는 γ는 삼각형의 세 각이다",


"1. ALGEBRAIC EQUATIONS" : "1. 대수 방정식",
"LINEAR EQUATION" : "일차 방정식 ",
"SYSTEM OF TWO LINEAR EQUATIONS" : "두 개의 선형 방정식 의 시스템",
"QUADRATIC EQUATION" : "이차 방정식",
"2. EXPONENT AND LOGARITHM \NEXPONENTIAL EQUATION" : "2. 지수 와 로그 \n 지수 방정식",
"2. EXPONENT AND LOGARITHM" : "2. 지수 와 로그",
"LOGARITHMIC EQUATION" : "대수 방정식",
"3. TRIGONOMETRIC EQUATION" : "3. 삼각 방정식",
"4. INEQUATIONS \NLINEAR INEQUATION" : "4. 부등식 \n 선형 부등식",
"4. INEQUATIONS" : "4. 부등식",
"QUADRATIC INEQUATION" : "이차 부등식",
"EXPONENTIAL INEQUATION" : "지수 부등식",
"LOGARITHMIC INEQUATION" : "로그 부등식",
"TRIGONOMETRIC INEQUATIONS" : "삼각 부등식",
"LINEAR EQUATION" : "일차 방정식 ",
"EXPONENTIAL EQUATION" : "지수 방정식",
"LINEAR INEQUATION" : "일차 부등식",
"CUBIC EQUATION" : "삼차 방정식",
"POINTS" : "점",
"- Distance between two points A and B" : "- 두점 A 와 B 사이의 거리",
"- Distance between point A and origin:" : "- 점 A 와 원점 사이의 거리:",
"TRIANGLE" : "삼각형",
"- Area of triangle with vertices at A, B, C" : "- A, B, C 정점 삼각형의 면적",
"- Area of a triangle with a vertice at origin" : "- 원점꼭지점 을 가진삼각형의 면적",
"EQUATION OF LINE" : "선의 방정식",
"- Joining two points A, B" : "- A, B 두 지점을 연결",
"- Passing point A and parallel with line y=ax+b" : "- y=ax+b 선과 병렬이며 점 A 을 통과",
"- Passing point A and perpendicular with line y=ax+b" : "- y=ax+b 선과 수직이며 점 A 를 통과",
"EQUATION OF CIRCLE" : "원의 방정식",
"- Circle with radius r and center at (a, b)" : "- 반지름 r 과 센터 서클 (a, b)",
"- Circle with center at origin" : "- 원점센터의 원",
"ELLIPSE" : "타원",
"HYPERBOLA" : "쌍곡선",
"PARABOLA" : "포물선",

"Limit \n" : "극한\n",
"Derivative \n" : "도함수 \n",
"C: constant" : "C: 상수",
"Differentiation \n" : "미분 \n",
"C: constant; u, v, w: functions of x" : "C: 상수 ; U, V, W: x 의 함수",
"LIMIT" : "극한",
"DERIVATIVE" : "미분",
"DIFFERENTIATION" : "미분 ",

"1. INDEFINITE INTEGRALS" : "1.부정 적분 ",
"C: arbitrary constant, k: constant" : "  C:임의의 상수, k: 상수",
"INTEGRALS BY PARTIAL FRACTIONS" : "부분 분수에 의한 적분",
"INTEGRALS INVOLVING ROOTS" : "뿌리를 포함한 적분",
"INTEGRALS INVOLVING TRIGONOMETRIC FUNCTIONS" : "삼각 함수의 적분",
"2. DEFINITE INTEGRALS" : "2. 정적분",
"APPLICATIONS" : "애플리케이션",
"- Surface area created by y=f(x)" : "- y=f(x)에의해 만들어진 표면적",
"- Volume of a solid created by y=f(x) rotated around axis:" : "- y=f(x)의회전 축을 중심으로하여 만든 고체의 부피:",

"Median" : "중앙값",
"Angle bisector" : "각의 이등분선",
"SPHERICAL CAP" : "구모자",
"SPHERICAL SEGMENT" : "구면대",
"SPHERICAL SECTOR" : "구형 분야",
"TORUS" : "원환면",
"FORMULAS WITH t=tan(x/2)" : "t=tan(x/2) 의 공식",
"SIDES AND ANGLES OF A PLANE TRIANGLE" : "삼각형의 변과 각도",
"Law of sines, cosines and tangents" : "sine, cosine, tangent 의 법칙",

"1. PLANE ANALYTIC GEOMETRY" : "1. 평면 분석 기하학",
"2. SOLID ANALYTIC GEOMETRY" : "2. 솔리드 해석 기하학",
"LINE" : "직선",
"- Direction Cosines of Line Joining Points A and B" : "- 점 A 과 B 가 결합되는 선의 consine 방향",
"EQUATION OF LINE JOINING TWO POINTS A, B" : "점 A 와 B 가 결합되는 선의 방정식",
"- In standard form" : "- 표준 형태로",
"- In parametric form" : "- 변수 형태로",
"PLANE" : "평면",
"- General equation of a plane" : "- 평면의 방정식",
"- Equation of plane passing through point A, B, C" : "- 점 A, B, C 를 지나는 평면의 방정식",
"- Equation of plane in intercept form" : "- 절편 형태의 평면의 방정식",
"a,b,c are the intercept on the x,y,z axes, respectively" : "A, B, C 는 각각x, Y, Z 축 에 대한절편 이다",
"- Normal Form for Equation of Plane" : "- 평면 의 방정식 정규형",
"p: perpendicular distance from O to plane at P; α, β, γ: angles between OP and positive x,y,z axes" : "p: O 와 평면 P 간의 수직거리; α, β, γ: OP 와 축 x,y,z 의 양의 각도",
"- Distance from point M to a plane" : "- 평면과 점 M 과의 거리",
"EQUATION OF SPHERE CENTER AT M AND RADIUS R IN RECTANGULAR COORDINATES" : "지름 r 의 직교좌표와 구점 M 의 구 방정식",
"EQUATION OF ELLIPSOID WITH CENTER M AND SEMI-AXES a, b, c" : "중점 M 과 반축 a,b,c 의 타원체 방정식",
"ELLIPTIC CYLINDER WITH AXIS AS z AXIS" : "Z 축 타원주",
"ELLIPTIC CONE WITH AXIS AS z AXIS" : "z 축 타원 콘",
"HYPERBOLOID OF ONE SHEET" : "일엽쌍곡면",
"HYPERBOLOID OF TWO SHEETS" : "이엽쌍곡면",
"ELLIPTIC PARABOLOID" : "타원 포물면",
"HYPERBOLIC PARABOLOID" : "쌍곡선 포물면",

"TRANSFORMATIONS" : "변환",
"3. SPECIAL INDEFINITE INTEGRALS" : "3. 특별 부정적분",
"Integrals involving ax+b" : "ax+b 적분",
"Integrals involving ax+b and px+q" : "ax+b 와 px+q 적분",
"Integrals involving x²+a²" : "x²+a² 적분",
"Integrals involving x²-a², x²&gt;a²" : "x²-a², x²&gt;a² 적분",
"Integrals involving x²-a², x²&lt;a²" : "x²-a², x²&lt;a² 적분",
"Integrals involving ax²+bx+c" : "ax²+bx+c 적분",
"Integrals involving xⁿ+aⁿ" : "xⁿ+aⁿ 적분",
"Integrals involving sin(ax)" : "sin(ax) 적분",
"Integrals involving e^(ax)" : "e^(ax) 적분",
"Integrals involving ln(x)" : "ln(x) 적분",

"TRANSPOSE OF A MATRIX" : "행렬의 전치",
"ADDITION AND SUBTRACTION OF MATRICES" : "행렬 의 덧셈과 뺄셈",
"MULTIPLICATION OF MATRICES" : "행렬 의 곱셈",
"DETERMINANT OF MATRIX" : "행렬의 행렬식",
"INVERSE OF MATRIX" : "가역행렬",
"EQUATION IN MATRIX FORM" : "매트릭스 형태의 방정식",
"PROPERTIES OF MATRIX CALCULATIONS" : "행렬 계산 의 속성",
"LENGTH" : "길이",
"AREA" : "넓이",
"VOLUME" : "부피",
"MASS" : "무게",
"SPEED" : "속력",
"TIME" : "시간",
"TEMPERATURE" : "온도",
"DENSITY" : "밀도",
"FORCE" : "힘 (물리)",
"ENERGY" : "에너지",
"POWER" : "일률",
"PRESSURE" : "압력",
"Do you know this?" : "이것을 알고 계십니까?",
"What is special about these calculations?" : "이 계산에 특별한 점은 무엇입니까?",
"Can you find the interesting trait of these calculations?" : "이러한 계산의 흥미로운 특성 을 찾을 수 있습니까?",
"5. GRAPH OF OTHER FUNCTIONS" : "5. 다른 함수의 그래프 ",
"Constant" : "상수 ",
"Absolute" : "절대값",
"Square Root" : "제곱근 ",
"Parabolic" : " 이차 함수",
"Cubic" : "세제곱수",
"Reciprocal" : "역수 ",
"Sec" : "시컨트(할선)",
"Cosec" : "코시컨트(여할)",
"6. FUNCTION TRANSFORMATIONS" : "6. 함수 변환 ",
"Horizontal Shifting" : "수평 변속 ",
"Vertical Shifting" : "수직 변속 ",
"Reflection" : "반사 ",
"Stretching" : "스트레칭",
"F, F1: Focus points (foci)" : "F, F1: 병소",
"AF=p: Parameter of Parabola" : "AF=p: 포물선 의 매개 변수",

"Probability & Statistics" : "확률 및 통계",
"1. SETS" : "1. 집합",
"SET" : "집합",
"SUBSET" : "부분집합",
"INTERSECTION" : "교집합",
"UNION" : "합집합",
"SYMMETRIC DIFFERENCE" : "대칭차",
"RELATIVE COMPLEMENT OF A IN B" : "차집합",
"ABSOLUTE COMPLEMENT" : "여집합",
"OPERATIONS ON SETS" : "성질",
"2. COMBINATIONS AND PERMUTATIONS" : "2. 조합 과 순열",
"COMBINATIONS" : "조합",
"PERMUTATIONS" : "순열",
"3. PROBABILITY" : "3. 확률",
"(1) If A and B are mutually exclusive, (2) If A and B are independent" : "(1) A 와 B가 배반사건인 경우, (2) A 와 B가 독립사건인 경우 ",
"4. STATISTICS" : "4. 통계",
"MEAN" : "평균 ",
"MEDIAN" : "중앙값",
"MODE" : "최빈값",
"Numerical value that occurs the most number of times" : "가장 일반적인 값 ",
"EXAMPLE" : "예 ",
"GEOMETRIC MEAN" : "기하 평균",
"HARMONIC MEAN" : "조화 평균 ",
"VARIANCE" : "분산",
"STANDARD DEVIATION" : "표준편차",
"MEAN DEVIATION" : "평균 편차",
"ROOT MEAN SQUARE" : "RMS값 (제곱 평균값)",
"NORMAL DISTRIBUTION (GAUSSIAN DISTRIBUTION)" : "정규 분포 (가우스 분포) ",
"f: density function, F: Distribution function, μ: mean" : "f : 밀도함수, F : 분포함수, μ : 평균",
"EXPONENTIAL DISTRIBUTION" : "지수 분포 ",
"POISSON DISTRIBUTION" : "포아송 분포 ",
"UNIFORM DISTRIBUTION" : "균등 분포 ",
"Transforms" : "변환 ",
"1. FOURIER SERIES AND TRANSFORMS" : "1. 푸리에 급수와 변환 ",
"f(x): periodic function, period 2L; a_n, b_n: Fourier coefficients; c_n: complex Fourier coefficient" : "f(x): 주기 함수, 주기 2L; a_n, b_n : 푸리에 계수; c_n : 복소 푸리에 계수 ",
"REAL FORM OF FOURIER SERIES" : "푸리에 급수의 기본 형태 ",
"COMPLEX FORM" : "복소 형태 ",
"PARSEVAL’S THEOREM" : "파시발의 정리",
"FOURIER TRANSFORM" : "푸리에 변환 ",
"f(x): function of x, F(s): Fourier transform of f(x)" : "f(x): x의 함수; F(s): 푸리에 변환의 f(x)",
"CONVOLUTION" : "합성곱",
"CORRELATION" : "상관 관계 ",
"f*: complex conjugate of f" : "f*: f의 켤레복소수",
"FOURIER SYMMETRY RELATIONSHIPS" : "푸리에 대칭 관계 ",
"FOURIER TRANSFORM PAIRS" : "푸리에 변환쌍 ",
"2. LAPLACE TRANSFORMS" : "2. 라플라스 변환 ",
"DEFINITION" : "정의 ",
"CONVOLUTION" : "합성곱",
"INVERSE" : "역변환",
"a: constant" : "a: 상수 ",
"DERIVATIVE" : "미분",
"SUBSTITUTION (FREQUENCY SHIFTING)" : "주파수 도메인 변속",
"TRANSLATION (TIME SHIFTING)" : "시간 도메인 변환 ",
"LAPLACE TRANSFORM PAIRS" : "라플라스  변환쌍",

"Math Tricks" : "수학 트릭",
"1. ADDITION" : "1. 덧셈",
"2. SUBTRACTION" : "2. 뺄셈",
"3. MULTIPLICATION" : "3. 곱셈",
"4. DIVISION" : "4. 나눗셈",
"5. SQUARING" : "5. 제곱",
"6. EXPONENTIATION" : "6. 지수",
"7. ROOTS" : "7. 뿌리",
"8. SUMMATIONS" : "8. 합계함",
"6. SUMMATIONS" : "6. 합계함",

"UNITS CONVERTER" : "단위 변환기",
"ANGLE" : "각도",
"COMPUTER DATA" : "컴퓨터 데이터양",
"FUEL CONSUMPTION" : "연료 소비량",
"Click here to download Physics Formulas app" : "물리학 공식 앱을 다운로드하려면 여기를 클릭하십시오 ",
"All Maths formulas for your work and study" : "작업과 연구를위한 모든 수학 공식 ",
"EDIT" : "편집 ",
"ADD" : "추가 ",
"TITLE OF THE PICTURE" : "사진 제목 ",
"GAME" : "게임 ",
"START" : "시작 ",
"LEVEL" : "레벨 ",
"RETRY" : "재시도 ",
"SCORE" : "점수 ",
"TIME" : "시간",
"BEST" : "베스트 ",
"GAME OVER" : "게임 오버 ",
"YOUR SCORE IS: " : "당신의 점수는 ",


"Metre (m)" : "미터  (m)",
"Kilometre (km)" : "킬로미터  (km)",
"Centimetre (cm)" : "센티미터  (cm)",
"Millimetre (mm)" : "밀리미터 (mm)",
"Micrometre | Mircon (μm)" : "마이크로 미터 | Micron  (μm)",
"Nanometre (nm)" : "나노미터 (nm)",
"Inch (in)" : "인치  (in)",
"Foot (ft)" : "피트 (ft)",
"Yard (yd)" : "야드 (yd)",
"Mile (mi)" : "마일  (mi)",
"Light Year" : "광년 ",
"Nautical Mile (NM)" : "해리 (NM)",
"Angstrom (Å)" : "옹스트롬  (Å)",

"Square Mile (mi²)" : "평방 마일  (mi²)",
"Square Yard (yd²)" : "제곱야드 (yd²)",
"Square Foot (ft²)" : "제곱피트  (ft²)",
"Square Inch (in²)" : "제곱인치 (in²)",
"Square Kilometre (km²)" : "제곱킬로미터  (km²)",
"Hectare (ha)" : "헥타르  (ha)",
"Acre" : "에이커 ",
"Square Metre (m²)" : "제곱미터 (m²)",
"Square Centimetre (cm²)" : "제곱센티미터 (cm²)",
"Square Millimetre (mm²)" : "제곱밀리미터  (mm²)",

"Litre (l)" : "리터  (l)",
"Cubic Metre (m³)" : "세제곱미터 (m³)",
"Cubic Inch (in³)" : "세제곱인치 (in³)",
"Cubic Foot (ft³)" : "세제곱피트  (ft³)",
"Cubic Yard (yd³)" : "세제곱야드  (yd³)",
"Gallon (US) (gal)" : "갤런 (미국)  (gal)",
"Gallon (UK) (gal)" : "갤런 (영국)  (gal)",

"Gram (g)" : "그램  (g)",
"Kilogram (kg)" : "킬로그램  (kg)",
"Tonne (t)" : "톤  (t)",
"Milligram (mg)" : "밀리그램  (mg)",
"Microgram (μg)" : "마이크로 그램  (μg)",
"Ounce (oz)" : "온스  (oz)",
"Pound (lb)" : "파운드  (lb)",
"Carat" : "캐럿 ",
"Slug" : "슬러그",
"Ounce (Troy) (ozt)" : "트로이온스 (ozt)",

"Year" : "년 ",
"Month" : "월 ",
"Week" : "주 ",
"Day" : "일 ",
"Hour (h)" : "시 (h)",
"Minute (min)" : "분  (min)",
"Second (s)" : "초  (s)",
"Millisecond (ms)" : "밀리초  (ms)",
"Microsecond (μs)" : "마이크로초 (μs)",
"Decade" : "10년 ",
"Century" : "세기 ",
"Millennium" : "밀레니엄 ",

"Kilometre per Hour (km/h)" : "시간 당 킬로미터  (km/h)",
"Miles per Hour (mi/h)" : "시간 당 마일  (mi/h)",
"Metre per Second (m/s)" : "초 당 미터 (m/s)",
"Foot per Second (ft/s)" : "초 당 피트 (ft/s)",
"Knot" : "노트",
"Speed of Light (c)" : "광속 (c)",
"Minutes per Mile (min/mi)" : "분 당 마일 (min/mi)",
"Minutes per Kilometre (min/km)" : "분 당 킬로미터 (min/km)",

"Celsius (°C)" : "섭씨  (°C)",
"Fahrenheit (°F)" : "화씨  (°F)",
"Kelvin (K)" : "켈빈  (K)",
"Rankine (°R)" : "랭킨  (°R)",

"Kilogram/Cubic Metre (kg/m³)" : "킬로그램 / 세제곱미터 (kg/m³)",
"Kilogram/Litre (kg/l)" : "킬로그램 / 리터  (kg/l)",
"Slug/Cubic Foot (slug/ft³)" : "슬러그 / 세제곱피트 (slug/ft³)",
"Pound/Cubic Foot (lbm/ft³)" : "파운드 /  세제곱피트  (lbm/ft³)",

"Newton (N)" : "뉴턴  (N)",
"Kilonewton (kN)" : "킬로뉴턴  (kN)",
"Atomic unit of force" : "포스 원자 단위",
"Gram Force (gf)" : "그램 포스  (gf)",
"Pound Force (lbf)" : "파운드 포스  (lbf)",
"Poundal (pdl)" : "파운달 (pdl)",
"Dyne (dyn)" : "다인  (dyn)",
"Kilogram-force | Kilopond (kgf)" : "킬로그램 포스 | 킬로폰드  (kgf)",

"Watt (W)" : "와트  (W)",
"Kilowatt (kW)" : "킬로와트  (kW)",
"Megawatt (MW)" : "메가와트  (MW)",
"Milliwatt (mW)" : "밀리와트  (mW)",
"Microwatt (µW)" : "마이크로와트  (µW)",
"Horsepower (hp)" : "마력  (hp)",
"Calorie per Second (cal/s)" : "초 당 칼로리  (cal/s)",

"Pascal (Pa)" : "파스칼  (Pa)",
"Kilopascal (kPa)" : "킬로파스칼  (kPa)",
"Megapascal (MPa)" : "메가파스칼  (MPa)",
"Bar (bar)" : "바  (bar)",
"Millibar (mbar)" : "밀리바  (mbar)",
"Atmosphere (atm)" : "기압 (atm)",
"Pound Force per Square Inch (psi)" : "세제곱인치 당 파운드 포스  (psi)",
"Dyne per Square Centimetre" : "제곱센티미터 당 다인 ",

"Degree (°)" : "도 (°)",
"Radian (rad)" : "라디안  (rad)",
"π Radian (π rad)" : "π 라디안  (π rad)",
"Second" : "초 ",
"Minute (')" : "분  (')",

"Byte (B)" : "바이트  (B)",
"Kilobyte | Kibibyte (kB | Ki)" : "킬로바이트 | Kibibyte  (kB | Ki)",
"Megabyte | Mebibyte (MB | Mi)" : "메가바이트 | Mebibyte  (MB | Mi)",
"Gigabyte | Gibibyte (GB | Gi)" : "기가바이트 | Gibibyte  (GB | Gi)",
"Terabyte | Tebibyte (TB | Ti)" : "테라바이트 | Tebibyte  (TB | Ti)",
"Petabyte | Pebibyte (PB | Pi)" : "페타바이트 | Pebibyte  (PB | Pi)",
"Bit (b)" : "비트  (b)",
"Zebibyte (Zi)" : "Zebibyte  (Zi)",

"Litres per 100 Kilometres (l/100km)" : "100 킬로미터 당 리터  (l/100km)",
"Kilometres per Litre (km/l)" : "리터 당 킬로미터  (km/l)",
"Miles per Gallon (US) (mpg)" : "갤런 당 마일 (US)  (mpg)",
"Miles per Gallon (UK) (mpg)" : "갤런 당 마일 (UK) (mpg)",

"Title of new tool" : "새로운 도구의 제목",
"Name of the variable" : "변수의 이름",
"Add more variable" : "변수를 추가",
"Formula of the tool" : "공구의 화학식",
"Name of the result " : "결과의 이름",
"Add more result" : "결과 추가",
"EULER'S FORMULA" : "오일러의 공식",

};