package jokes.mindgame.com.telugujokes02

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import content_apps.mindgame.com.formulas01.JokesDataset
import content_apps.mindgame.com.formulas01.R
import kotlinx.android.synthetic.main.fragment_joke_detail.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [JokeDetailFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [JokeDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class JokeDetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_joke_detail, container, false)
        val position = JokesDataset.position
        v.tvTitle2.text = "$position ." +JokesDataset.jokes.elementAt(position).title
//        v.wvBody.webViewClient = WebViewClient()
        v.wvBody.settings.javaScriptEnabled = true
        v.wvBody.loadUrl(JokesDataset.jokes.elementAt(position).url)
       //Share button
        v.imgbtnShare.setOnClickListener(View.OnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "Test data")
                type = "text/plain"
            }
            startActivity(sendIntent)
        })

        return v

    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    }
