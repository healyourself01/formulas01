package content_apps.mindgame.com.formulas01

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.*
import kotlinx.android.synthetic.main.joke_view.view.*
import java.io.File
import java.io.InputStream


class JokesAdapter(val ctx:Context, val act:FragmentActivity?):RecyclerView.Adapter<JokesAdapter.ViewHolder>() {


    class ViewHolder(val view:View):RecyclerView.ViewHolder(view){

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(ctx).inflate(R.layout.joke_view,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return JokesDataset.jokes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title =  JokesDataset.jokes.elementAt(position).title
        holder.view.tvTitle.text = "$position ." + title
        val iconPath = "icons/icon_"+ title.toLowerCase().replace(" ","_")+".png"

        val img = BitmapFactory.decodeStream(act!!.assets.open(iconPath ))
//        holder.view.imgIcon.setImageResource(icons[position])
        holder.view.imgIcon.setImageBitmap(img)
//        holder.view.tvLine1.text = JokesDataset.jokes.elementAt(position).line1

        holder.view.setOnClickListener(View.OnClickListener {
            JokesDataset.position = position
            if (act is JokeInterfaces){
               // act.loadJokeDetail()
                AdObject.admob.loadNextScreen { act.loadJokeDetail() }
            }

        })

    }


}