package content_apps.mindgame.com.formulas01

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.gson.Gson
import org.jetbrains.anko.*
import com.google.gson.reflect.TypeToken
import jokes.mindgame.com.telugujokes02.Joke
import jokes.mindgame.com.telugujokes02.JokeDetailFragment
import jokes.mindgame.com.telugujokes02.JokeHeaderFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_start_screnn.*
import kotlinx.android.synthetic.main.fragment_start_screnn.view.*


class MainActivity : AppCompatActivity(), JokeInterfaces {

private var BANNER_LOADED = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        //Check Internet connection before loading the ad.
        loadBannerWithConnectivityCheck()
        //Initialize the Admob Object and save it so that anybody can access it.
        AdObject.INTERSTITIAL_ID = AdObject.INTERSTITIAL_TEST_ID
        AdObject.APPLICATION_ADS_ID = "ca-app-pub-3940256099942544~3347511713"
        AdObject.PACKAGE_NAME = packageName
        AdObject.admob = AdmobUtility(this,AdObject.ADS_MODE_PROD)
        AdObject.connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager


        val jsondata = applicationContext.assets.open("mainmenu.json").bufferedReader().readText()

        JokesDataset.jokes = streamingArray(jsondata)

        // If the app is in test mode load the test screen else load the start screen.
        if (AdObject.showAppOrNot()){
            loadStartScreen()}
        else{
            loadTestModeScreen()
        }

//         findViewById<TextView>(R.id.tvPrivacy).setOnClickListener(View.OnClickListener {
//            startActivity(Intent(this,PrivacyPolicy::class.java))
//        })


    }

    override fun loadTestModeScreen(){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_holder,TestModeFragment())
            commit()
            addToBackStack(null)
        }
        loadBannerWithConnectivityCheck()
    }
    override fun loadStartScreen(){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_holder,StartScreenFragment())
            commit()
            addToBackStack(null)
        }
        loadBannerWithConnectivityCheck()
    }

    override fun loadJokeHeader(){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_holder, JokeHeaderFragment())
            commit()
            addToBackStack(null)
        }
        loadBannerWithConnectivityCheck()
    }
    //Load the Joke detail fragment
    override fun loadJokeDetail() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_holder, JokeDetailFragment())
            commit()
            addToBackStack(null)
        }
        loadBannerWithConnectivityCheck()
    }




    fun parseJSON(array: String):Boolean{

        doAsync {
            val jsonArray = streamingArray(array)

        }
        runOnUiThread {
            alert("JSON parsing done!") {
                yesButton { toast("Yat")
                noButton { toast("haha") }}
            }
        }
        return true
    }


    fun streamingArray(array: String): Collection<Joke>  {


        val gson = Gson()

        // Deserialization
        val collectionType = object : TypeToken<Collection<Joke>>(){}.type
        val result = gson.fromJson<Collection<Joke>>(array, collectionType)


//
//        }
       return result
    }

    fun loadBannerWithConnectivityCheck(){
        if (AdObject.isConnected()) {
            findViewById<AdView>(R.id.adBanner).loadAd(AdRequest.Builder().build())
            BANNER_LOADED = true
        }
    }
}
