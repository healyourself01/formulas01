package content_apps.mindgame.com.formulas01

interface JokeInterfaces {
    fun loadJokeDetail()
    fun loadStartScreen()
    fun loadJokeHeader()
    fun loadTestModeScreen()
}